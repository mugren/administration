package blogito

/**
 * Created by igor on 3/31/14.
 */
class UnderscoreCodec {
    static encode = {target->
        target.replaceAll(" ", "_")
    }

    static decode = {target->
        target.replaceAll("_", " ")
    }
}
