package blogito

import sun.misc.BASE64Encoder

import java.security.MessageDigest

/**
 * Created by igor on 3/31/14.
 */
class HashCodec {
    static encode = { str ->
        MessageDigest md = MessageDigest.getInstance('SHA')
        md.update(str.getBytes('UTF-8'))
        return (new BASE64Encoder()).encode(md.digest())
    }
}