package blogito

class Entry {

    static searchable = true

    static constraints = {
        title()
        summary(maxSize:1000)
        filename(nullable: true,blank:true)
        dateCreated()
        lastUpdated()
    }

    static mapping = {
        sort "lastUpdated":"desc"
    }

    static belongsTo = [author:User]

    String title
    String summary
    String filename
    Date dateCreated
    Date lastUpdated

    String toString(){
        "${title} (${lastUpdated})"
    }
}
