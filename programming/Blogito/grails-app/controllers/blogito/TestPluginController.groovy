package blogito

class TestPluginController {

    def shortenUrlService

    def index = {
        render """This is a test for the ShortenUrl plug-in
        " +
        "Type test/tinyurl?q=http://grails.org to try it out."""
    }

    def tinyurl = {
        String s = "s"
        render shortenUrlService.tinyurl(params.q)
    }
}
