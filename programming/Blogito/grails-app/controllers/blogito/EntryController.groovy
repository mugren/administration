package blogito

class EntryController {

    def beforeInterceptor = [action:this.&auth, except:["index", "list", "show","atom","search"]]

    def auth() {
        if(!session.user) {
            redirect(controller:"user", action:"login")
            return false
        }
    }

    def scaffold = Entry

    def list = {
        if(!params.max) params.max = 10
        flash.id = params.id
        if(!params.id) params.id = "No User Supplied"
        flash.title = params.title
        if(!params.title) params.title = ""

        def entryList
        def entryCount
        def author = User.findByLogin(params.id)
        if(author){
            def query = {
                eq('author', author)
                like("title", params.title.decodeUnderscore() + '%')
            }
            entryList = Entry.createCriteria().list(params, query)
            entryCount = Entry.createCriteria().count(query)
        }else{
            entryList = Entry.list( params )
            entryCount = Entry.count()
        }

        [ entryInstanceList:entryList, entryCount:entryCount  ]
    }

    def edit = {
        def entryInstance = Entry.get( params.id )

        //limit editing to the original author
        if( !(session.user.login == entryInstance.author.login) ){
            flash.message = "Sorry, you can only edit your own entries."
            redirect(action:list)
        }

        if(!entryInstance) {
            flash.message = "Entry not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ entryInstance : entryInstance ]
        }
    }


    def save = {
        def entryInstance = new Entry(params)
        entryInstance.author = User.get(session.user.id)

        //handle uploaded file
        def uploadedFile = request.getFile('payload')
        if(!uploadedFile.empty){
            println "Class: ${uploadedFile.class}"
            println "Name: ${uploadedFile.name}"
            println "OriginalFileName: ${uploadedFile.originalFilename}"
            println "Size: ${uploadedFile.size}"
            println "ContentType: ${uploadedFile.contentType}"

            def webRootDir = servletContext.getRealPath("/")
            def userDir = new File(webRootDir, "/payload/${session.user.login}")
            userDir.mkdirs()
            uploadedFile.transferTo( new File( userDir, uploadedFile.originalFilename))

            entryInstance.filename = uploadedFile.originalFilename
        }

        if(!entryInstance.hasErrors() && entryInstance.save()) {
            flash.message = "Entry ${entryInstance.id} created"
            redirect(action:show,id:entryInstance.id)
        }
        else {
            render(view:'create',model:[entryInstance:entryInstance])
        }
    }

    def atom = {
        if(!params.max) params.max = 10
        def list = Entry.list( params )
        def lastUpdated = list[0].lastUpdated

        [ entryInstanceList:list, lastUpdated:lastUpdated ]
    }

    def search = {
        //render Entry.search(params.q, params)
        def searchResults = Entry.search(params.q, params)
        flash.message = "${searchResults.total} results found for search: ${params.q}"
        flash.q = params.q
        return [searchResults:searchResults.results, resultCount:searchResults.total]
    }


    def show(Long id) {
        println params
        def entryInstance = Entry.get(id)
        println params.title.decodeerscore()
        def entryByTitle = Entry.where {
            title =~ params.title.decodeUnderscore()
        }.find()
        println entryByTitle
        if (!entryInstance || (entryInstance!=entryByTitle) ) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: '${domainClass.propertyName}.label', default: 'Entry'), id])
            redirect(action: "list")
            return
        }

        [entryInstance: entryInstance]
    }
}
