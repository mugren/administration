package blogito

class UserController {

    def scaffold = User

    def beforeInterceptor = [action:this.&auth,
                             except:["login", "authenticate", "logout"]]

    def auth() {
        if( !(session?.user?.role == "admin") ){
            flash.message = "You must be an administrator to perform that task."
            redirect(action:"login")
            return false
        }
    }

    def login = {}

    def authenticate = {
        def user = User.findByLoginAndPassword(params.login, params.password.encodeAsHash())
        if(user){
            session.user = user
            flash.message = "Hello ${user.name}!"
            redirect(controller:"entry", action:"list")
        }else{
            flash.message = "Sorry, ${params.login}. Please try again."
            redirect(action:"login")
        }
    }

    def logout = {
        flash.message = "Goodbye ${session.user.name}"
        session.user = null
        redirect(controller:"entry", action:"list")
    }

    def save() {
        def userInstance = new User()
        params.password = params.password.encodeAsHash()
        userInstance.properties = params
        if (!userInstance.save(flush: true)) {
            render(view: "create", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'user.userInstance.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }

    def update(Long id, Long version) {
        def userInstance = User.get(id)
        if (!userInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.userInstance.label', default: 'User'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (userInstance.version > version) {
                    def lowerCaseName = grails.util.GrailsNameUtils.getPropertyName(User)
                    userInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                            [message(code: 'user.userInstance.label', default: 'User')] as Object[],
                            "Another user has updated this ${User} while you were editing")
                render(view: "edit", model: [userInstance: userInstance])
                return
            }
        }

        params.password = params.password.encodeAsHash()
        userInstance.properties = params

        if (!userInstance.save(flush: true)) {
            render(view: "edit", model: [userInstance: userInstance])
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'user.userInstance.label', default: 'User'), userInstance.id])
        redirect(action: "show", id: userInstance.id)
    }
}
