class UrlMappings {

	static mappings = {
		"/$controller/$action?/$id?"{
			constraints {
				// apply constraints here
			}
		}

		"/"(controller:"entry",action: 'index')
		"500"(view:'/error')

        "/blog/$id/$title?"(controller:"entry", action:"list")
        "/entry/$action?/$id?/$title?"(controller:"entry")

        name blok: "/blok/$id/$title?" {
            controller = "entry"
            action = "show"
        }

	}
}
