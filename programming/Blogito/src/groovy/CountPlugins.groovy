/**
 * Created by igor on 4/2/14.
 */
def addr = "http://plugins.grails.org/.plugin-meta/plugins-list.xml"
def plugins = new XmlSlurper().parse(addr)
def count = 0
def spring_count = 0
plugins.plugin.each{
    println it.@name
    count++
    if(it.@name.toString().contains("spring")){
        spring_count++
    }
}
println "Total number of plugins: ${count}"
println "Total number of Spring plugins: ${spring_count}"