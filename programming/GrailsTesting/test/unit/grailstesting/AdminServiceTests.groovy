package grailstesting

import grails.test.GrailsUnitTestCase
import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(AdminService)
class AdminServiceTests extends GrailsUnitTestCase {

    void testRestartServer() {
        def jdoe = new User(name: "John Doe", login: "jdoe", role: "user", password: "password")
        def suziq = new User(name: "Suzi Q", login: "suziq", role: "admin", password: "password")
        def adminService = new AdminService()

        mockLogging(AdminService)

        assertTrue adminService.restartServer(suziq)
        assertFalse adminService.restartServer(jdoe)
    }
}
