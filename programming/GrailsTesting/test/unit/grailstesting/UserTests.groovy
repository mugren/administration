package grailstesting


import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(User)
class UserTests {

    void testBlank() {
        mockForConstraintsTests(User)
        def user = new User(name:"",
                            login:"admin",
                            password:"wordpass")
        assertFalse user.validate()


        println "=" * 20
        println "Total number of errors:"
        println user.errors.errorCount

        println "=" * 20
        println "Here are all of the errors:"
        println user.errors

        println "=" * 20
        println "Here are the errors individually:"
        user.errors.allErrors.each{
            println it
            println "-" * 20
        }

        assertEquals "blank", user.errors["name"]
        assertEquals 1, user.errors.errorCount
        //assertNull user.errors['name']
        //println user.errors['name']
    }

    void testMockDomain(){
        def jdoe = new User(name:"John Doe", login:"jdoe" ,role:"user",password: "password")
        def suziq = new User(name:"Suzi Q", login:"suziq", role:"admin",password: "password")
        def jsmith = new User(name:"Jane Smith", login:"jsmith" ,role:"user",password: "password")

        mockDomain(User, [jdoe, suziq, jsmith])

        //dynamic finder
        def list = User.findAllByRole("admin")
        assertEquals 1, list.size()

        //NOTE: criteria, Hibernate Query Language (HQL)
        //      and Query By Example (QBE) are not supported
    }

    void testMockGorm(){
        def jdoe = new User(name:"John Doe", login:"jdoe" , role:"user",password: "password")
        def suziq = new User(name:"Suzi Q",login:"suziq", role:"admin",password: "password")
        def jsmith = new User(name:"Jane Smith", login:"jsmith",role:"user",password: "password")

        mockDomain(User, [jdoe, suziq, jsmith])


        def foo = new User(login:"foo")
        foo.name = "Bubba"
        foo.role = "user"
        foo.password = "password"
        foo.save()
        assertEquals 4, foo.id  //NOTE: id gets assigned
        assertEquals 3, User.findAllByRole("user").size()
    }
}
