package grailstesting

class User {

    String name
    String login
    String password
    String role = "user"

    static constraints = {
        name(blank:false)
        login(unique:true, blank:false)
        password(password:true, minSize:5)
        role(inList:["user", "admin"])
    }

    String toString(){
        "${name} (${role})"
    }
}
