package grailstesting

class AdminService {

    boolean transactional = true

    def restartServer(User user) {
        if(user.role == "admin"){
            //restart the server
            return true
        }else{
            log.info "Ha! ${user.name} thinks s/he is an admin..."
            return false
        }
    }
}
