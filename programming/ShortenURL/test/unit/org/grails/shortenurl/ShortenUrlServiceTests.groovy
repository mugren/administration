package org.grails.shortenurl

import grails.test.GrailsUnitTestCase
import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ShortenUrlService)
class ShortenUrlServiceTests extends GrailsUnitTestCase {

    def transactional = false
    def shortenUrlService

    /* setUp and tearDown are called for every test */
    protected void setUp() {
        super.setUp()
        mockLogging(ShortenUrlService)
        shortenUrlService = new ShortenUrlService()
    }

    protected void tearDown() {
        super.tearDown()
    }

    void testTinyUrl() {
        def shortUrl = shortenUrlService.tinyurl("http://grails.org")
        assertEquals "http://tinyurl.com/3xfpkv", shortUrl
    }

    void testIsGd() {
        def shortUrl = shortenUrlService.isgd("http://grails.org")
        assertEquals "http://is.gd/MHSN7W", shortUrl
    }

    void testIsGdWithBadUrl() {
        def shortUrl = shortenUrlService.isgd("IAmNotAValidUrl")
        assertTrue shortUrl.startsWith("An error occurred:")
    }
}
