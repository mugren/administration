package org.grails.shortenurl

/**
 * Created by igor on 4/3/14.
 */
class TinyUrlTests extends GroovyTestCase {
    def transactional = false

    void testShorten(){
        def shortUrl = TinyUrl.shorten("http://grails.org")
        assertEquals "http://tinyurl.com/3xfpkv", shortUrl
    }
}
