package org.grails.shortenurl

/**
 * Created by igor on 4/3/14.
 */
class IsGd {
    static String shorten(String longUrl){
        def addr = "http://is.gd/create.php?format=simple&url=${longUrl}"
        def url = addr.toURL()
        def urlConnection = url.openConnection()
        if(urlConnection.responseCode == 200){
            return urlConnection.content.text
        }else{
            return "An error occurred: ${addr}\n" +
                    "${urlConnection.responseCode} : ${urlConnection.responseMessage}"
        }
    }
}
