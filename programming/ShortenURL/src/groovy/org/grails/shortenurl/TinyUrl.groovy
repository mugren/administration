package org.grails.shortenurl

/**
 * Created by igor on 4/3/14.
 */
class TinyUrl {
    static String shorten(String longUrl){
        def addr = "http://tinyurl.com/api-create.php?url=${longUrl}"
        return addr.toURL().text
    }
}
