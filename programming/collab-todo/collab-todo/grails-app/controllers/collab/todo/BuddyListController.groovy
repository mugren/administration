package collab.todo

class BuddyListController {

    /**
     * Adding a buddy.
     */
    def add = {
        log.info "Add a buddy list of $params.buddyListName"

        // create our list and save it
        def buddyList = new BuddyList()
        buddyList.name = params.buddyListName
        buddyList.owner = session.user
        buddyList.description = ""
        log.info "BuddyList - $buddyList"
        // TODO - error message if not saved
        buddyList.save()

        String cnn = params.cn2
        session.cn = cnn
        // render
        render(template:'/common/buddyList',  var: 'list', collection:BuddyList.findAllByOwner(session.user))
    }

    /**
     * Lists the buddy list.
     */
    def list = {
        log.info "List Buddy List "
        if(!params.max)params.max = 10
        def user = User.get(session.user.id)
        log.info params
        [ buddyListList: BuddyList.findAllByOwner(user, params) ]
    }

    /**
     * Saves the buddy list when adding a new one.
     */
    def save = {
        log.info("Saving BuddyList")
        def bl = new BuddyList(params)
        // grab the user and set him accordingly
        bl.owner = User.get(session.user.id)

        if(bl.save()) {
            redirect(action:list,id:bl.id)
        }
        else {
            render(view:'edit',model:[buddyList:bl])
        }
    }

    /**
     * Called when editing the name.
     * This will save the name change.
     */
    def editName = {

        log.info "Update buddy list name"

        // retrieve member
        def buddyList = BuddyList.get(params.id)
        buddyList.name = params.name

        // render a new page.
        //render(template:'/common/buddyListMember',  var: 'buddy', collection:BuddyListMember.findAllByBuddyList(buddyListMember.buddyList))
        render params.name
    }

    def delete = {
        log.info "Delete Buddy List - $params.id"

        //vo params.id se naogja id na member od buddy lista
        println "***buddyListId:${params.id}***"
        // find and remove the member
        def buddyList = BuddyList.get(params.id)
        def buddyListOwner = buddyList.owner

        // retrieve the buddy list so we can retrieve it later

        println buddyList
        //member.delete()

        buddyList.delete()
        //def buddyList2 = member.buddyList
//        println BuddyListMember?.findAllByBuddyList(buddyList)
        //println BuddyListMember.findAllByBuddyList(buddyList2)
        // buddyList = buddyList.refresh()
        // render the template
        //render(template:'/common/buddyList',  var: 'list', collection:BuddyList.findAllByOwner(buddyListOwner))
        // render(template:'/common/buddyList', var: 'list' , collection: buddyList )
        // render(template:'/common/buddyList',  var: 'list', collection:BuddyList.findAllByOwner(session.user))
        //bidekji brisenjeto ne po ajax

        //Test
        def sp = request.getServletPath()
        def ru = request.getRequestURI()
        def cp = request.getContextPath()

        //def cau = getGrailsAttributes().getControllerActionUri(getServletContext().getServlet())
        def cn = webRequest.getControllerName()

        println "***request testovi***"
        println sp
        println ru
        println cp
        println cn
        println "***hope***"
        //println "${params.get('cn')}"
        //println "${params['cn']}"
        String cnn = params.cnn
        session.cn = cnn




        redirect(controller: cnn, view:'index')
    }
}
