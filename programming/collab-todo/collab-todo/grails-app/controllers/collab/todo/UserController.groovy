package collab.todo

import grails.converters.JSON
import groovy.text.SimpleTemplateEngine
import org.apache.commons.codec.digest.DigestUtils
import org.codehaus.groovy.grails.web.servlet.mvc.GrailsWebRequest
import org.codehaus.groovy.grails.web.util.WebUtils
import org.springframework.mail.MailException
import org.codehaus.groovy.grails.commons.ApplicationHolder as AH



class UserController  {

   // def scaffold = User

   // def index = { redirect(action:list,params:params) }



    def simpleCaptchaService

    def mailAuthenticationService





    def index = { render(view: 'register') }

    // the delete, save and update actions only accept POST requests
    def allowedMethods = [delete:'POST', save:'POST', update:'POST']

    def list = {
        if(!params.max)params.max = 10

        [ userList: User.list( params ) , userInstanceTotal: User.count() ]
    }

    def show = {
        [ userInstance : User.get( params.id ) ]
    }

    def delete = {

//        if (session.user?.id != params.id) {
//    		flash.message = "You can only delete yourself"
//    		redirect(action:list)
//    		return
//        }

        def user = User.get(params.id)

        if(user) {
            user.delete()
            flash.message = "User ${params.id} deleted."
            redirect(action:list)
        }
        else {
            flash.message = "User not found with id ${params.id}"
            redirect(action:list)
        }
    }

    def register = {
        println "Register controller"
    }

    def handleRegistration = {
        def user = new User()

        //GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()

        //def session = webRequest.session

       /* println "${session.captcha}"
        println "2"
        println session.simpleCaptcha
        println "1"
        println session['captcha']
        println "${params}"
        println params
        println "drugo"
        println session.captcha

        GrailsWebRequest webRequest = WebUtils.retrieveGrailsWebRequest()

        def sesija = webRequest.session

        println sesija.captcha     */


        boolean captchaValid = simpleCaptchaService.validateCaptcha(params.captcha.toUpperCase())


        log.info("HANDLE REGISTRATION")
        // Process the captcha request
       // def captchaText = session.simpleCaptcha
       // session.captcha = null
        if (captchaValid) {
            if(params.password != params.confirmPassword) {
                flash.message = "The two passwords you entered don't match!"
                println "**password fail**"
                redirect(action:register)
            }
            else {
                log.info "before save"
                // Let's hash the password
                def hashPassd = DigestUtils.md5Hex(params.password)

                user.properties = params
               // user.userRealName = "${params.firstName}"+" "+"${params.lastName}"
                user.password = hashPassd
                //user.enabled = true

                def userAuth = Authority.findByAuthority("ROLE_USER")
                userAuth.addToPeople(user)
                userAuth.save()

                println(user.dump())
                if(user.save()) {

                    // also add this user to the authority system
                    //def userAuth = Authority.findByAuthority("ROLE_USER")
                    //userAuth.addToPeople(user)
                    // not sure if the save is necessary.
                    //userAuth.save()

                    //Potvrduvacki email
                    sendAcknowledgment (user)

                    log.info "saved redirecting to user controller"
                    // Let's log them in
                    session.user = user

                    redirect(controller:'todo')
                }
                else {
                    log.info "didn't save"
                    flash.user = user
                    redirect(action:register)
                }
            }
        }
        else {
            log.info "Captcha Not Filled In"
            flash.message = "Access code did not match."
            redirect(controller:'user')
        }
    }


    def login = {
        redirect (controller:'login' , action: 'auth')
    }


   /*
    def handleLogin = {

        def user = User.findByUserName(params.userName)

        if (!user)
        {
            flash.message = "User not found for userName: ${params.userName}"
            redirect(action:'login')

        }
        session.user = user

        redirect(controller:'todo')
    }
       */
    def logout = {
        if(session.user) {
            session.user = null
            redirect(controller: 'logout' , action: 'index')
        }
    }

    def save = {
        def user = new User()
        user.properties = params
        if(user.save()) {
            flash.message = "user.saved.message"
            flash.args = [user.firstName,user.lastName]
            flash.defaultMsg = "User saved."
            redirect(action:show,id:user.id)
        }
        else {
            render(view:'create',model:[user: user])
        }
    }

    def edit = {


        def user
        user = User.get(params.id)

        if(!user) {
            flash.message = "User not found with id ${params.id}"
            redirect(action:list)
        }
        else {
            return [ userInstance: user ]
        }
    }

    def update = {

//        if (params.id != session.user?.id) {
//    		flash.message = "You can only update yourself"
//    		redirect(action:list)
//    		return
//        }

        def user = User.get( params.id )

        if(user) {
            user.properties = params
            if(user.save()) {
                flash.message = "User ${params.id} updated."
                redirect(action:show,id:user.id)
            }
            else {
                render(view:'edit',model:[user:user])
            }
        }
        else {
            flash.message = "User not found with id ${params.id}"
            redirect(action:edit,id:params.id)
        }
    }

    def create = {
        def user = new User()
        user.properties = params
        return ['user':user]
    }

    private sendAcknowledgment  = { user ->

        // let's first design the email that we want to send
        //this.class treba fizicki da bide vo istiot folder za da funkcionira
        //this.class.classLoader se referencira spored tomcat i raboti relativno vo odnos na WEB-INF
        //def emailTpl = this.class.classLoader.getResource('web-app/WEB-INF/templates/registrationEmail.gtpl')
        //def emailTpl2 = this.class.getResource('/WEB-INF/templates/registrationEmail.gtpl')
        //def d = g.resource(dir:'templates',file: 'registrationEmail.gtpl')
        //def emailTpl3 = this.class.classLoader.getResource("web-app/WEB-INF/templates/registrationEmail.gtpl").toString()
        //def emailTpl4 = this.class.getResource('/WEB-INF/templates/registrationEmail.gtpl').toString()
        //def email1 = this.class.getResource('/collab-todo/templates/registrationEmail.gtpl')
        ///def email2 = this.class.classLoader.getResource('/collab-todo/templates/registrationEmail.gtpl')
        def falaBogu = grailsAttributes.getApplicationContext().getResource("/WEB-INF/templates/registrationEmail.gtpl").getFile()
        //def falaBogu2 = grailsAttributes.getApplicationContext().getResource("/WEB-INF/templates/registrationEmail.gtpl").toString()
        //println emailTpl
        //println emailTpl2
        //println d
        //println emailTpl3
        //println emailTpl4
        //println email1
        //println email2
        //println falaBogu
        //println falaBogu2
        def binding = ["user": user]
        def engine = new SimpleTemplateEngine()
        def template = engine.createTemplate(falaBogu).make(binding)
        def body = template.toString()

        println body
        // set up the email to send.
        def email = [
                to: user.email,
                subject: "Your Collab-Todo Report",
                text: 	body
        ]

        println email

        if(email != null){
            println mailAuthenticationService.class
        }
        else {
            println "Ete"
        }

        try {
            mailAuthenticationService.sendEmail(email, [])

        } catch (MailException ex) {
            log.error("Failed to send emails", ex)
            return false
        }
        true
    }

    def findUsers = {
        log.info "Find users"

        println "***${params.term}***"
        // lets query the database for any close matches to this
        def users = User.createCriteria().list {


            or {
                like("userName", "%${params.term}%")
                like("firstName", "%${params.term}%")
                like("lastName", "%${params.term}%")
            }
            projections { // good to select only the required columns.
                property("id")
                property("lastName")
                property("firstName")
            }
            order("lastName")
            order("firstName")
        }

        // let's build our output XML
        /*def writer = new StringWriter()

        // build it
        new groovy.xml.MarkupBuilder(writer).ul {
            for (u in users) {
                li(id: u.id, "${u.lastName}, $u.firstName")
            }
        }

        println "***${writer}***"
        render writer.toString() */

        def buddySelectList = [] // to add each company details
       users.each {
           println it[0]
           println it[1]
           println it[2]
           println it

            def buddyMap = [:] // add to map. jQuery autocomplete expects the JSON object to be with id/label/value.
            buddyMap.put("id", it[0])
            buddyMap.put("lastName", it[1])
            buddyMap.put("firstName", it[2])

            buddySelectList.add(buddyMap) // add to the arraylist
        }
        //return buddySelectList
        println buddySelectList as JSON
       render buddySelectList as JSON

    }



}
