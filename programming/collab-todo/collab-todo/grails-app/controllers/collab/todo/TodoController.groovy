package collab.todo

import org.springframework.dao.DataIntegrityViolationException
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.multipart.commons.CommonsMultipartFile



class TodoController {

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]


    def index() {
        def us = User.get(session.user.id)
        println "${us.id}"
        redirect(action: "list", params: params)
    }

   def beforeInterceptor = [action:this.&beforeAudit,except:['list']]
    def afterInterceptor = [action:{model ->this.&afterAudit(model)},
            except:['list']]

    def beforeAudit = {
        log.trace("${session?.user?.userName} Start action " +
                "${controllerName}Controller.${actionName}() : parameters $params")
    }
    def afterAudit = { model ->
        log.trace("${session?.user?.userName} End action " +
                "${controllerName}Controller.${actionName}() : returns $model")
    }




    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        def user = User.get(session.user.id)
        [todoInstanceList: Todo.findAllByOwner(user,params), todoInstanceTotal: Todo.count()]
    }

    def create() {
       // def todo = new Todo()
       // todo.properties = params
       // def owner = User.get(session.user.id);
       // todo.owner = owner
       // return ['todoInstance':todo]
        [todoInstance: new Todo(params)]
    }

    def save() {

        def todoInstance = new Todo(params)
        def owner = User.get(session.user.id)
        todoInstance.owner = owner
        todoInstance.createDate = new Date()
        todoInstance.lastModifiedDate = new Date()

        uploadFileData(todoInstance)

        //println todoInstance.id
        //println todoInstance.getId()

     /* if(todoInstance.id > 0){
          //[todoInstance: todoInstance]
          println "**update**"
          render(view:'list', model:[todoInstance: todoInstance, todoInstanceList: listByOwner()])
          return
      } */

        if (!todoInstance.save(flush: true)) {
            render(view:'list', model:[todoInstance:todoInstance, todoInstanceList: listByOwner()])
            return

        }

        flash.message = message(code: 'default.created.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])

        render(template:'detail', var: 'todo', collection:listByOwner())
    }
    //potrebno za rss
    def show(Long id) {
        def todoInstance = Todo.get(id)
        if (!todoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "list")
            return
        }

        [todoInstance: todoInstance]
    }

    def edit(Long id) {
        println id
        def todoInstance = Todo.get(id)
        if (!todoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "list")
            return
        }
        print "***VLEGUVA***"

        //[todoInstance: todoInstance]
        render(view:'edit', model:[todoInstance: todoInstance, todoInstanceList: listByOwner()])
    }

    def update(Long id, Long version) {
        def todoInstance = Todo.get(params.id)
        if (!todoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "list")
            return
        }

        if (version != null) {
            if (todoInstance.version > version) {
                todoInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
                          [message(code: 'todo.label', default: 'Todo')] as Object[],
                          "Another user has updated this Todo while you were editing")
                //render(view: "list", model: [todoInstance: todoInstance , todoInstanceList: listByOwner()])
                render(template:'detail', var: 'todo', collection:listByOwner())
                return
            }
        }

        todoInstance.properties = params
        println params
        println ""
        println params.completedDate

        if (params.completedDate!=null){
            println params.completedDate.class
            //Date d = new Date(params.completedDate)
            //println d
            Date g = new Date()
            println g
            println g.class
            println todoInstance.createDate.class
            println todoInstance.name.class
            println todoInstance.completeDate.class
            //todoInstance.completeDate = d
        }
        todoInstance.lastModifiedDate = new Date()
        // upload the file if it is there
        if(todoInstance.associatedFile!=null){
        uploadFileData(todoInstance)
        }

        if (!todoInstance.save(flush: true)) {
            //render(view: "list", model: [todoInstance: todoInstance , todoInstanceList: listByOwner()])
            println "Objava za greski"
            render(template:'detail', var: 'todo', collection:listByOwner())
            return
        }

        flash.message = message(code: 'default.updated.message', args: [message(code: 'todo.label', default: 'Todo'), todoInstance.id])
        redirect(action: "list")
    }

    def delete(Long id) {
        def todoInstance = Todo.get(id)
        if (!todoInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "list")
            return
        }

        try {
            todoInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'todo.label', default: 'Todo'), id])
            redirect(action: "show", id: id)

        }
    }

    def uploadFileData = { todo ->

        println "**Pocetok na upload**"

        if (request instanceof MultipartHttpServletRequest) {

            MultipartHttpServletRequest multiRequest = (MultipartHttpServletRequest)request;

            CommonsMultipartFile file = (CommonsMultipartFile)multiRequest.getFile("associatedFile");
            // Save the object items.
            todo.fileName = file.originalFilename
            todo.contentType = file.contentType
            todo.associatedFile = file.bytes

            println "**Zavrsen na upload**"
        }
    }

    def downloadFile = {

        def todo = Todo.get( params.id )

        response.setHeader("Content-disposition", "attachment; filename=${todo.fileName}")
        response.contentType = todo.contentType
        response.outputStream << todo.associatedFile
    }

    /**
     * This is used if you want to refresh the list.
     * via an ajax'ish way
     */
    private def listByOwner = {
        log.info("List")
        if(!params.max)params.max = 100
        def owner = User.get(session.user.id)
        //Todo.findAllByOwnerOrderByLastModifiedDate(owner, params)
        //Todo.findAllByOwner(owner, params, order: 'lastModifiedDate')
        // this needs to have an order by
        def list = Todo.findAllByOwner(owner, params)
        list
    }

    /**
     * This completes the task.
     * It will save the completed date and re render that section of the page.
     */
    def completeTask = {
        log.info "Complete Task"
        println "***VLEZ ***"
        Todo t = Todo.get( params['id'] )
        // update the date now
        t.completeDate = new Date()
        t.lastModifiedDate = new Date()
        t.save()
        // model is for a specific name .... bean is for "it"
        render(template:'detail', model:[todo:t])
    }

    /**
     * Used to show thet note information.
     */
    def showNotes = {
        def note = Todo.executeQuery("select t.note From Todo as t Where t.id = ${params['id']}")
        // why am i having to do an array?
        render note[0]
    }

    /**
     * Used to remove the task from the page
     * We won't want to render anything on the page after.
     */
    def removeTask = {
        Todo t = Todo.get( params['id'] )
        log.info "Delete - $t"
        t.delete();
        // now lets just eliminate our item from the page.
        render ''
        //redirect(action:'list')
        //render(template:'detail', var: 'todo', collection:listByOwner())
    }

    def userTodo = {
        println "TIMESTAMP-TODO"
        def user = User.get(session.user.id)
        return Todo.findAllByOwner(user)
    }


}
