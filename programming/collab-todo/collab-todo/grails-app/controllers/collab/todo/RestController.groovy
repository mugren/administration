package collab.todo

import org.apache.commons.codec.digest.DigestUtils
import org.codehaus.groovy.grails.commons.GrailsClass
import org.codehaus.groovy.grails.commons.GrailsDomainClass
import static org.apache.commons.lang.StringUtils.*
import org.codehaus.groovy.runtime.InvokerHelper
import collab.todo.Error





/**
 * Scaffolding like controller for exposing RESTful web services
 * for any domain object in both XML and JSON formats.
 */

class RestController {

    //private GrailsDomainClass domainClass
    private GrailsClass domainClass
    private String domainClassName
    // RESTful actions excluded

    def beforeInterceptor = {
        println "***${params.domain}***"
        println "***${params.controller}***"

        def authHeader = request.getHeader("Authorization")
        if (authHeader) {
            def tokens = authHeader.split(' ')
            def user_password = tokens[1]
            tokens = user_password.split(':')
            def userid = tokens[0]
            def password = tokens[1]
            def hashPassd = DigestUtils.md5Hex(password)
            // At this point, the userid and password could be verified
            // to make sure that the person making the request is authenticated
            //
            // << AUTHENTICATION LOGIC / CALL >>
            //
            // Put look up the user object and put it into session for use
            // later by the controllers.
            println "USER ID : ${userid}"

            def user = User.findByUserName(userid)
            def pass = user.getPassword()
            println pass
            println hashPassd
            if (user && pass.equals(hashPassd)) {

                session.user = user
                session.ident = user.id
            } else {
                session.user = null
            }

            println "${session.user}"
        }


        domainClassName = capitalize(params.domain)
        println domainClassName
        domainClass = grailsApplication.getArtefact("Domain", domainClassName)

        //Bidekji gornoto ne funkcionira najdeno e drugo zaobikoleno resenie
        if (!domainClass) {
            def artefacts = grailsApplication.getArtefacts("Domain")
            domainClass = artefacts.find {item ->
                item.name == domainClassName
            }
        }

        println domainClass
    }
    private invoke(method, parameters) {
        //InvokerHelper.invokeStaticMethod(domainClass.getClazz(), method, parameters)
        println domainClass.getClazz()
        InvokerHelper.invokeStaticMethod(domainClass.getClazz(),method,parameters)
    }
    private format(obj) {
        def restType = (params.rest == "rest")?"XML":"JSON"
        render obj."encodeAs$restType"()
    }

    def show = {
        def result

        println "PARAMS: ${params}"

        if(session.user) {
        if(params.id) {
            result = invoke("get", params.id)
        } else {
            if(!params.max) params.max = 10
            result = invoke("list", params)

        }
            println "RESULT: ${result}"
            println result.class

            params.id = session.ident

            response.setHeader("User","${session.ident}")

            println "PARAMS2: ${params}"
            println "ODGOVOR ${response}"
            format(result)

    }
    }

    def delete = {
        def result = invoke("get", params.id);
        println result
        if(result) {
            println "???"
            result.delete()
        } else {
            result = new Error(message: "${domainClassName} not found with id ${params.id}")
        }
        format(result)
    }

    def update = {
        def result
        def domain = invoke("get", params.id)
        if(domain) {
            domain.properties = params
            if(!domain.hasErrors() && domain.save()) {
                result = domain
            } else {
                result = new Error(message: "${domainClassName} could not be saved")
            }
        } else {
            result = new Error(message: "${domainClassName} not found with id ${params.id}")
        }
        format(result)
    }

    def create = {
        def result
        def domain = InvokerHelper.invokeConstructorOf(domainClass.getClazz(), null)
        println domain.class
        println domain
        def input = ""
        request.inputStream.eachLine {
            input += it
        }
        println input
        // convert input to name/value pairs
        if(input && input != '') {
            input.tokenize('&').each {
                def nvp = it.tokenize('=');
                params.put(nvp[0],nvp[1]);
            }
        }
        params.put("category",Category.findByName('Work'))
        params.put("lastModifiedDate",new Date())
        println params

        domain.properties = params

        if(!domain.hasErrors() && domain.save()) {
            result = domain
        } else {
            result = new Error(message: "${domainClassName} could not be created")
        }
        domain.errors.allErrors.each {
            println it
        }
        format(result)
    }





}
