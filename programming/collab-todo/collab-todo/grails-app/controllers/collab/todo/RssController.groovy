package collab.todo

import feedsplugin.FeedBuilder

class RssController {

    def builder = new FeedBuilder()

    def feed = {

        render(feedType:"rss") { // optional - , feedVersion:"2.0") {
            title = "Todo List"
            link = "http://localhost:8080/collab-todo/rss"
            description = "Funky Todo"
            Todo.list(sort: "name", order: "asc").each {
                def todo = it
                entry(it.name) {
                    title = "${todo.name}"
                    link = "http://localhost:8080/collab-todo/todo/show/${todo.id}"
                    author = "${todo.owner.lastName}, ${todo.owner.firstName}"
                    publishedDate = todo.createDate
                    //link = "http://your.test.server/article/${it.id}"
                    //it.content // return the content
                }
            }
        }
    }

}
