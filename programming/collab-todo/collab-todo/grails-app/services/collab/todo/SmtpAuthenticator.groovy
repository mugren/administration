package collab.todo
/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 1/17/13
 * Time: 2:58 PM
 * To change this template use File | Settings | File Templates.
 */
import javax.mail.Authenticator

class SmtpAuthenticator extends Authenticator{

    private String username;
    private String password;

    public SmtpAuthenticator(String username, String password) {
        super();
        this.username = username;
        this.password = password;
    }
    public javax.mail.PasswordAuthentication getPasswordAuthentication() {
        return new javax.mail.PasswordAuthentication(username, password);
    }

}
