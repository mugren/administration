package collab.todo

import groovy.text.SimpleTemplateEngine
import org.springframework.beans.BeansException
import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.mail.MailException

class BatchService implements ApplicationContextAware {

    static transactional = false


    @Override
    void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        //To change body of implemented methods use File | Settings | File Templates.
        this.applicationContext = applicationContext
    }

    ReportService reportService // Inject ReportService

    def ApplicationContext applicationContext
    MailAuthenticationService mailAuthenticationService
    /*
    * Runs nightly reports
    */
    def nightlyReports = {
        log.info "Running Nightly Reports Batch Job: "+new Date()
        println "***Running Nightly Reports Batch Job***"
        // 1. Gather user w/ email addresses.
        def users = User.withCriteria {
            isNotNull('email')
        }
        users?.each { user ->
            // 2. Invoke report service for each user.
            //    Can't reuse ReportController because it makes too
            //    many assumptions, such as access to session.class.
            //
            //    Reuse Report Service and pass appropriate params.
            // Gather the data to be reported.
            def inputCollection = Todo.findAllByOwner(user)
            // To be completed in the next section
            Map params = new HashMap()
            params.inputCollection = inputCollection
            params.userName = user.firstName+" "+user.lastName
            // Load the report file.
           // def reportFile = this.class.getClassLoader().getResource("web-app/reports/userTodo.jasper")
           // def reportFile = applicationContext.class.getClassLoader().getResource("web-app/reports/userTodo.jasper").getFile()
          def reportFile = applicationContext.getResource("/reports/userTodo.jasper")
            def reportFile1 = applicationContext.servletContext.getResource("/reports/userTodo.jasper")

            println reportFile
            println reportFile1
            println params



            def byteArray = reportService.generateReport(reportFile1, 1,params )

            println "delimiter"

            Map attachments = new HashMap()
            attachments.put("TodoReport.pdf", byteArray.toByteArray())
            // 3. Email results to the user.
            sendNotificationEmail(user, attachments)

        }
        log.info "Completed Nightly Reports Batch Job:  "+new Date()
    }

    def private sendNotificationEmail = {User user, Map attachments ->

        println "***Send Email Notification***"

        //def emailTpl = this.class.getClassLoader().getResource("web-app/WEB-INF/nightlyReportsEmail.gtpl")
        def emailTpl = applicationContext.getResource("/WEB-INF/templates/nightlyReportsEmail.gtpl").getFile()

        def binding = ["user": user]
        def engine = new SimpleTemplateEngine()
        def template = engine.createTemplate(emailTpl).make(binding)
        def body = template.toString()

           def email = [
                         to: user.email,
                         subject: "Your Collab-Todo Report",
                         text:   body
                      ]

             try {
                 // EMailProperties eMailProperties = applicationContext.getBean("eMailProperties")

                   mailAuthenticationService.sendEmail(email,attachments)

               } catch (MailException ex) {
                   log.error("Failed to send emails", ex)
                }


          }


    }




