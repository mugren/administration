package collab.todo

import org.springframework.context.ApplicationContext
import org.springframework.context.ApplicationContextAware
import org.springframework.core.io.ByteArrayResource
import org.springframework.mail.javamail.JavaMailSender
import org.springframework.mail.javamail.MimeMessageHelper

import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeMessage

class MailAuthenticationService implements ApplicationContextAware {

    static transactional = false
    JavaMailSender mailSender

    //EMailProperties eMailProperties
    def ApplicationContext applicationContext

    public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext
    }


    def sendEmail = { mail,  attachements ->

        println "***TELOTO SE IZVRSUVA***"



        EMailProperties eMailProperties = applicationContext.getBean("eMailProperties")



        MimeMessage mimeMessage = mailSender.createMimeMessage()

        // start creation of mime message
        MimeMessageHelper helper = new MimeMessageHelper(mimeMessage, true, "ISO-8859-1")

        def list = [eMailProperties.from,mail.to]
        println list

        helper.from = new InternetAddress(eMailProperties.from)
        //helper.to = getInternetAddresses(mail.to) as InternetAddress
        helper.to = new InternetAddress(mail.to)
        helper.subject = mail.subject
        helper.setText(mail.text, true)
        if(mail.bcc) helper.setBcc(getInternetAddresses(mail.bcc));
        if(mail.cc) helper.setCc(getInternetAddresses(mail.cc));

        // add any attachments
        attachements.each { key, value ->
            helper.addAttachment(key, new ByteArrayResource(value))
        }

        println mimeMessage

        mailSender.send mimeMessage

      /*if (mailSender.hasErrors())  {
        mailSender.errors.allErrors.each {
            print it
        }
      }  */
    }

    private static InternetAddress[] getInternetAddresses(List emails) {
        InternetAddress[] mailAddresses = new InternetAddress[emails.size()];
        emails.eachWithIndex {mail, i ->
            mailAddresses[i] = new InternetAddress(mail)
        }
        return mailAddresses;
    }
}
