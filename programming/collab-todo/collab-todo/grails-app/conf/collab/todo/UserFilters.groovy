package collab.todo

class UserFilters {

    def filters = {
       // all(controller: '*', action: '*') {
        userModificationCheck(controller: 'user', action: '*') {
            before = {
                // Workaround Grails 1.0 and 1.1 Bug
                // actionName was not set properly when posted from a form
                /*
                def currActionName = actionName ?: params.find {it.key ==~ "_action_.*"}?.value?.toLowerCase()
                */
                def currActionName = actionName
                if (currActionName == 'edit' ||
                        currActionName == 'update' ||
                        currActionName == 'delete') {
                    String userId = session?.user?.id
                    String paramsUserId = params?.id
                    if (userId != paramsUserId) {
                        flash.message = "You can only modify yourself"
                        redirect(action: 'list')
                        return false
                    }
                }
            }

            }
           // after = { Map model ->

           // }
           // afterView = { Exception e ->

          //  }
        }
    }
//someOtherFilter(uri: '\/user\/*') { }
