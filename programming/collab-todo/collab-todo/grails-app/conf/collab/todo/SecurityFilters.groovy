package collab.todo

class SecurityFilters {

    def authenticateService

    def filters = {
       /* all(controller: '*', action: '*') {
            before = {

            }
            after = { Map model ->

            }
            afterView = { Exception e ->

            }
        }   */


            putUserIntoSession(controller:'todo', action:'list') {
                before = {
                    if(!session.user) {
                        User user = User.get(authenticateService.userDomain().id)
                        session.user = user

                    }
                }
        }
        putUserIntoSessionAlternative(uri: "/" ) {
            before = {
                if(!session.user) {
                    User user = User.get(authenticateService.userDomain()?.id)
                    session.user = user

                }
            }
        }

        passTheNameOfController(controller: "*")
                {
                    before = {
                        session.cn = webRequest.getControllerName()
                    }
                }

    }
}
