security {

    // see DefaultSecurityConfig.groovy for all settable/overridable properties

    active = true

    loginUserDomainClass = "collab.todo.User"
    authorityDomainClass = "collab.todo.Authority"
    requestMapClass = "collab.todo.Requestmap"

    userName = 'userName'
    password = 'password'
    enabled = 'active'

    algorithm = 'MD5'
}
