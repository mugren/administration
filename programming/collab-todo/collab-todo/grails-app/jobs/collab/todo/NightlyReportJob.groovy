package collab.todo



class NightlyReportJob {

   // static sessionRequired = false        // za dali da bajnduva hibernate sesija
   // static concurrent = false             // za dali izvrsuvanjeto na cron da bide konkurentno

   // def cronExpression = "0 0 1 * * ?" // Run every day at 1:00 a.m.

    def name = "NightlyReport" // Job name
    def group = "CollabTodo"   // Job group

    //def startDelay = 60000 // Wait 20 seconds to start the job
    //def timeout = 120000    // Execute job once every 60 seconds

    static triggers = {
        //simple repeatInterval: 5000l // execute job once in 5 seconds
        cron name:'Nightly' , cronExpression: "0 0 1 * * ?"
    }


    def batchService

    def execute() {
        // execute job
        log.info "Starting Nightly Job: "+new Date()
        batchService.nightlyReports.call()
        log.info "Finished Nightly Job: "+new Date()
    }

}

