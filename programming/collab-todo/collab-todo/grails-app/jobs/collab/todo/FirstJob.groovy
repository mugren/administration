package collab.todo



class FirstJob {
    static triggers = {
      simple repeatInterval: 5000l // execute job once in 5 seconds
    }

    def execute() {
        // execute job
       // println "Hello from FirstJob: "+ new Date()
    }
}
