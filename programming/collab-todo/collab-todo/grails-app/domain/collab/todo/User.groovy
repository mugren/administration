package collab.todo

class User {

   /* static mapping = {

        //tablePerHierarchy true

        userName name: '`username`'
        password name: '`passwd`'
        active name: '`enabled`'

        columns {
            //address lazy:false
            //address cache:true
            userName name: '`username`'
            password name: '`passwd`'
            active name: '`enabled`'
        }
    }   */


    static transients = ['confirmPassword']

    static embedded = ['address']

    String userName
    //String userRealName
    String firstName
    String lastName
    String password
    String email
    String confirmPassword
    Address address = new Address()

    //boolean enabled

    boolean active = true

    //boolean emailShow

    /** description */
    //String description = ''

    /** plain password to create a MD5 password */
    //String pass = '[secret]'


    static hasMany = [authorities: Authority, buddyList: BuddyList, todos: Todo , categories: Category]
    static belongsTo = Authority

    static constraints = {

        userName(blank:false,unique:true)
        //userRealName(blank: false)
        firstName(blank:false, minLength: 3)
        lastName(blank:false)
        password(blank:false)
        email(email:true)
        //enabled()
        //confirmPassword(nullable:true)
        address(nullable:true)
    }



    @Override
    String toString () {
        "$lastName, $firstName"
    }


    String dump() {
        return "FirstName: ${firstName}, LastName: ${lastName}, UserName: ${userName}, Password: ${password}, Email: ${email}"
    }

}

class Address {

    String addressLine1
    String addressLine2
    String city
    String state
    String zipCode
    String country

    static constraints = {
        addressLine1(nullable:true)
        addressLine2(nullable:true)
        city(nullable:true)
        state(nullable:true)
        zipCode(nullable:true)
        country(nullable:true)
    }
}