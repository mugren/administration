package collab.todo
/**
 * collab.todo.Authority domain class.
 */
class Authority {

    static hasMany = [people: User]

    /** description */
    String description
    /** ROLE String */
    String authority = "ROLE_"

    static constraints = {
        authority(blank: false, unique: true)
        description()
    }
}
