package collab.todo

/**
 * Defines the member to a buddy list.
 * Buddy list members belong to a buddy list and can
 * have a different display name to their normal name.
 */

class BuddyListMember {

    static constraints = {
        nickName(blank:false)
        user(nullable:false)
    }

    String nickName
    User user
    BuddyList buddyList

    static belongsTo = BuddyList

    String  toString () {
        return "$nickName - $buddyList.name - $user.userName"
    }
}
