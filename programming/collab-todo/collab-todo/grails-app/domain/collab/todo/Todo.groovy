package collab.todo


class Todo {

    //static withTable = "todo_tbl"
    static searchable = true





    String name
    String note
    Date createDate
    Date dueDate
    Date completeDate
    Date lastModifiedDate
    Date startDate
    String priority
    String status
    byte[] associatedFile
    String fileName
    String contentType


    User owner
    Category category

    static belongsTo = [User,Category]
    static hasMany = [keywords:Keyword]

    static constraints = {

        owner(nullable:false)
        name(blank:false)
        createDate()
        priority()
        category(nullable:false)
        status()
        note(maxSize:1000, nullable:true)
        completeDate(nullable:true,
                validator: {
                        // val is this value, obj is the Todo object
                    val, obj ->
                        if (val != null) {
                            return val.after(obj.createDate)
                        }
                        return true
                })
        dueDate(nullable:true)
        startDate(nullable:true)
               /* validator: {
                    // can't start in the past
                    if (it?.compareTo(new Date() - 1) < 0 ) {
                        return false
                    }
                    return true
                })  */
        associatedFile(nullable:true)
              /*  validator: {
                    if (it?.length > 10000) {
                        return false
                    }
                    return true
                }) */
        fileName(nullable:true)
        contentType(nullable:true)


    }

   /* static mapping = {

        table 'todo_tbl'
        //cache true
       // cache usage:'read-write', include:'lazy'

        columns {
            name column:'name_str'
            note column:'note_str'

           // name index:'Name_Idx, Name_Create_Date_Idx'
           // createDate index:'Name_Create_Date_Idx'

        }

        id generator:'hilo', params:[table:'hi_value',column:'next_value',max_lo:100]
        //id composite:['name', 'dueDate]'

        version false

        //autoTimestamp false
    }  */

    def beforeInsert() {
        startDate = new Date()
    }

    String toString()
    {
        return "Name: ${name}, note: ${note}"
    }

    String dump() {
        return "Owner: ${owner}, Name: ${name}, Note: ${note}, Create Date: ${createDate}, Due Date: ${dueDate}, Complete Date: ${completeDate}, Priority: ${priority}, Status: ${status}"
    }

    // Auto run some itmes.
    def beforeInsert = {
        createDate = new Date()
        lastModifiedDate = new Date()
    }
    def beforeUpdate = {
        lastModifiedDate = new Date()
    }


}


/*
enum Priorities {
    Low(1), Medium(2), High(3)

    private final Integer value

    Priorities(Integer value) {
        this.value = value
    }

    Integer getId(){
        value
    }


}

enum Statuses {
    Incomplete(1), Complete(2)

    private final Integer value


Statuses(Integer value){
    this.value = value
}

Integer getId(){
    value
}



}  */