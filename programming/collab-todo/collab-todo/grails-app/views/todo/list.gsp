<%@ page import="collab.todo.Todo" %>
<%@ page import="collab.todo.Category" %>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main" />
    <title>Todo List</title>




</head>
<body>
<g:preLoadShowHide/>

<div class="body">
    <h2>Todo List
        <g:showHide update="addToDo">
        <img border=0 src="${createLinkTo(dir:'images',file:'add_obj.gif')}"
             alt="[ADD]"/>
        </g:showHide>
    </h2>

    <div id="messageDisplay">
        <g:if test="${flash.message}">
            <div class="message">
                ${flash.message}
            </div>
        </g:if>
        <g:hasErrors>
            <div class="message">
                <g:eachError bean="${todoInstance}">
                    <li><g:message error="${it}" /></li>
                </g:eachError>
            </div>
        </g:hasErrors>
    </div>


    <!-- Add -->
    <div id="addToDo" style="display:none">

           <g:formRemote name="todoForm"
                         url="[controller:'todo',action:'save']"
                         update="todoList"
                         onComplete="showhide('addToDo')"

                         enctype="multipart/form-data">

               <!--
                    update="[success:'todoList',failure:'error']"
                     update="[success:'todoListId',failure:'error']"
                     onComplete="showhide('addToDo')">
                     - use when using the ajaxForm
                     -->
            <g:hiddenField name="id" value="${todoInstance?.id}" />

            <div class="dialog">
                <table>
                    <tbody>
                    <tr class='prop'><td valign='top' class='name'>
                        <label for='category'>Category:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'owner','errors')}'>
                            <g:select optionKey="id" optionValue="name" from="${Category.list()}"
                                      name='category.id' value="${todoInstance?.category?.id}">

                            </g:select>
                        </td>
                    </tr>

                    <tr class='prop'><td valign='top' class='name'><label for='name'>Name:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'name','errors')}'>
                        <input type="text" name='name' value="${todoInstance?.name?.encodeAsHTML()}"/></td></tr>

                    <tr class='prop'><td valign='top' class='name'>
                        <label for='createDate'>Start Date:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'startDate','errors')}'>
                            <g:datePicker name='startDate' value="${todoInstance?.createDate}" precision="day" years="${2012..2017}"></g:datePicker></td></tr>


                    <tr class='prop'><td valign='top' class='name'>
                        <label for='priority'>Priority:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'priority','errors')}'>
                            <g:select from="${["Low", "Medium", "High"]}" name='priority' value="${todoInstance?.priority}"/>
                        </td></tr>

                    <tr class='prop'><td valign='top' class='name'><label for='status'>Status:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'status','errors')}'>
                            <g:select from="${["Incomplete", "Complete"]}" name='status' value="${todoInstance?.status}"/>
                        </td></tr>

                    <tr class='prop'><td valign='top' class='name'><label for='dueDate'>Due Date:</label></td><td valign='top' class='value ${hasErrors(bean:todoInstance,field:'dueDate','errors')}'>
                        <g:datePicker name='dueDate' value="${todoInstance?.dueDate}"></g:datePicker></td></tr>

                    <tr class='prop'><td valign='top' class='name'><label for='dueDate'>File:</label></td><td valign='top' class='value ${hasErrors(bean:todoInstance,field:'associatedFile','errors')}'>
                        <input type="file" name="associatedFile" />
                    </td></tr>

                    <tr class='prop'>
                        <td valign='top' class='name'><label for='note'>Note:</label></td>
                        <td valign='top' class='value ${hasErrors(bean:todoInstance,field:'note','errors')}'>
                            <g:textArea name="note" value="${todoInstance?.note?.encodeAsHTML()}" rows="4" cols="50"/>
                        </td></tr>

                    </tbody>
                </table>
            </div>

            <div class="buttons">
                <span class="formButton">
                    <input type="submit" value="${todoInstance?.id > 0 ? 'Update' : 'Create'}"/>
                </span>

                <span class="formButton">
                    <a class="formButton" href="${createLink(controller: 'todo', action: 'list')}">Cancel</a>
                </span>
            </div>

        </g:formRemote>

    </div>



<!-- to do list -->
    <div id="todoList">
        <g:render template="detail" var="todo" collection="${todoInstanceList}" />
        ${flash.clear()}

    </div>

    <g:report id="todoReport" controller="TodoController"
              action="userTodo" report="userTodo"
              format="PDF,HTML,CSV,XLS,RTF,TXT,XML">
        <input type="hidden" name="userName" value="${todoInstanceList[0]?.owner}" />
    </g:report>



</div>
</body>
</html>