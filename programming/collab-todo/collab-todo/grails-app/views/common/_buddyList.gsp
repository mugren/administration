<%@ page import="collab.todo.BuddyListMember" %>


<div id="buddyListMember${list.id}">

    ${list.name}




    <g:link controller="buddyList" action="delete" id="${list.id}" params="[cnn : session.cn ]" >
        <img border=0 src="${createLinkTo(dir:'images',file:'delete_obj.gif')}" alt="[-]"/>
    </g:link>

    <g:showHide update="buddyListAdd$list.id">
        <img border=0 src="${createLinkTo(dir:'images',file:'add_obj.gif')}" alt="[*]"/>
    </g:showHide><br />

    <!-- The way to add someone to the form -->

    <div id="buddyListAdd${list.id}" style="display:none">

        <g:formRemote name="buddyListForm${list.id}"
                      url="[controller:'buddyListMember',action:'add']"
                      update="buddyListMembers${list.id}"
                      onComplete="showhide('buddyListAdd${list.id}')">

            <input type="hidden" name="buddyListId" value="${list.id}"/>
            <input type="hidden" name="userName" id="userNameId${list.id}" value=""/>

            <g:textField id="autocomplete${list.id}" name="userId" />

            <span id="indicator1" style="display: none">
                <img src="/collab-todo/images/spinner.gif" alt="Working..." /></span>

           <!-- <div id="autocomplete_choices${list.id}" class="autocomplete"></div>  -->


            <script type="text/javascript">


                $("#autocomplete${list.id}").autocomplete({
                        source:function(request,response){
                                    $.ajax({
                                                url:"/collab-todo/user/findUsers",
                                        dataType: "json",
                                        data: request  ,
                                                success: function(data) {
                                                        response($.map(data, function(item) {
                                                    return {
                                                        label: item.lastName,
                                                        id: item.id

                                                    };

                                                }));
                                                }
                                            });
                                }  ,
                                minLength: 2, // triggered only after minimum 2 characters have been entered.
                                select: function(event, ui) { // event handler when user selects from the list.
                                    $("#userNameId${list.id}").val(ui.item.id); // update the hidden field.

                                }
                                });//autocomplete

            </script>

        </g:formRemote>

    </div>

    <div id="buddyListMembers${list.id}">
        <g:render template="/common/buddyListMember" var="buddy" collection="${BuddyListMember.findAllByBuddyList(list)}" />
    </div>
    <br style="line-height: 40%"/>
</div>