
<div class="buddy" id="buddy${buddy.id}">

    ${buddy.nickName}
    <g:remoteLink controller="buddyListMember" action="delete" id="${buddy.id}"
                  update="buddy${buddy.id}">
        <img border=0 src="${createLinkTo(dir:'images',file:'delete_obj.gif')}" alt="[-]"/>
    </g:remoteLink>

</div>

