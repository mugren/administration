<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 1/11/13
  Time: 12:32 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page contentType="text/html;charset=UTF-8" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="layout" content="main" />
    <title>Registration</title>
</head>
<body>

<div class="nav">
    <span class="menuButton"><a class="home" href="${createLinkTo(dir:'')}"><g:message code="default.home.label"/></a></span>
    <span class="menuButton"><g:link class="list" action="list">User List</g:link></span>
</div>

<div class="body">

    <h1>Create User</h1>
    <g:if test="${flash.message}">
        <div class="message">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${user}">
        <div class="errors">
            <g:renderErrors bean="${user}" as="list" />
        </div>
    </g:hasErrors>

    <g:form action="handleRegistration" method="post" >
        <div class="dialog">
            <table>
                <tbody>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="login">Login:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'userName','errors')}'>
                        <input type="text" name="userName" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="password">Password:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'password','errors')}'>
                        <input type="password" name="password" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="confirm">Confirm Password:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'password','errors')}'>
                        <input type="password" name="confirmPassword" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="firstName">First Name:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'firstName','errors')}'>
                        <input type="text" name="firstName" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="lastName">Last Name:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'lastName','errors')}'>
                        <input type="text" name="lastName" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="email">Email:</label>
                    </td>
                    <td valign='top' class='valueClear ${hasErrors(bean:user,field:'email','errors')}'>
                        <input type="text" name="email" />
                    </td>
                </tr>

                <tr class='prop'>
                    <td valign='top' class='nameClear'>
                        <label for="code">Enter Code:</label>
                    </td>
                    <td valign='top' class='valueClear'>
                        <input type="text" name="captcha"><br/>
                        <img src="${createLink(controller:'simpleCaptcha', action:'captcha')}" />
                    </td>
                </tr>

                </tbody>
            </table>
        </div>

        <div class="buttons">
            <span class="button"><input class="save" type="submit" value="Register"></input></span>
        </div>

    </g:form>
</div>
</body>
</html>