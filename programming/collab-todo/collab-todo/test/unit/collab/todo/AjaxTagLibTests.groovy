package collab.todo



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.web.GroovyPageUnitTestMixin} for usage instructions
 */
@TestFor(AjaxTagLib)
class AjaxTagLibTests {

    void testSomething() {
        fail "Implement me"
    }
}
