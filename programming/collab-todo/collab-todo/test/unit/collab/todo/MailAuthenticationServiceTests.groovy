package collab.todo



import grails.test.mixin.*
import org.junit.*

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(MailAuthenticationService)
class MailAuthenticationServiceTests {

    void testSomething() {
        fail "Implement me"
    }
}
