package cli

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 2/11/13
 * Time: 4:12 PM
 * To change this template use File | Settings | File Templates.
 */

import jline.ConsoleReader

ConsoleReader cr = new jline.ConsoleReader()
// Prompt for UserID and Password
print "User id : "
def userid = cr.readLine();
print "Password: "
def password = cr.readLine(new Character('*' as char));


def url = "http://localhost:8080/collab-todo/rest/todo"
def userInfoUrl = "http://localhost:8080/collab-todo/userInfo?rest=rest"
def slurper = new XmlSlurper()

// Get the User Info
// It will be used later
def user_id
def user_firstName
def user_lastName

def conn = new URL(url).openConnection()
conn.requestMethod = "GET"
conn.doOutput = true

if (userid && password) {
    conn.setRequestProperty("Authorization", "Basic ${userid}:${password}")
}

if (conn.responseCode == conn.HTTP_OK) {
    def response
    conn.inputStream.withStream {
        response = slurper.parse(it)
        println "${response}"
       // user_id = response.todo[0].@owner.id
       // user_firstName = response.todo[0].firstName
       // user_lastName = response.todo[0].lastName
    }


    //user_id=response.@id
    //user_firstName=response.firstName
    //user_lastName=response.lastName
    println "${conn.getHeaderFields()}"
    println "${conn.getHeaderField("User")}"
    user_id = conn.getHeaderField("User")

}



// Create the to-do
conn = new URL(url).openConnection()
conn.requestMethod = "PUT"
conn.doOutput = true
conn.doInput = true

if (userid && password) {
    conn.setRequestProperty("Authorization", "Basic ${userid}:${password}")
}

// Values for CreatedDate
Calendar createDate = Calendar.getInstance();
def cdYear = createDate.get(Calendar.YEAR)
def cdMonth = createDate.get(Calendar.MONTH) + 1
def cdDay = createDate.get(Calendar.DAY_OF_MONTH)
def cdHour = createDate.get(Calendar.HOUR_OF_DAY)
def cdMin = createDate.get(Calendar.MINUTE)

// Prompt for Todo Information
println ""
print "Name:     "
def name = cr.readLine();
print "Priority: "
def priority = cr.readLine();
print "Status:   "
def status = cr.readLine();
print "Note:     "
def note = cr.readLine();

def data = "name=${name}&note=${note}&owner.id=${user_id}\
&priority=${priority}&status=${status}&createDate=struct\
&createDate_hour=${cdHour}&createDate_month=${cdMonth}\
&createDate_minute=${cdMin}&createDate_year=${cdYear}\
&createDate_day=${cdDay}"



/*
def data = "name=fred&note=cool&owner.id=1&priority=Low&status=Incomplete&"+
        "createDate=struct&createDate_hour=00&createDate_month=12&" +
        "createDate_minute=15&createDate_year=2007&createDate_day=11" */

conn.outputStream.withWriter { out ->
    out.write(data)
    out.flush()
}

if (conn.responseCode == conn.HTTP_OK) {
    input = conn.inputStream
    input.eachLine {
        println it
    }
}
conn.disconnect()
