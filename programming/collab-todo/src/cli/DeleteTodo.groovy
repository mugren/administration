package cli

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 2/11/13
 * Time: 6:15 PM
 * To change this template use File | Settings | File Templates.
 */

import jline.ConsoleReader

ConsoleReader cr = new jline.ConsoleReader()

// Prompt for UserID and Password
def userid = cr.readLine("User id : ")
def password = cr.readLine("Password: ", new Character('*' as char))
println ""
def todo_id = cr.readLine("Todo Item id: ")

def url = "http://localhost:8080/collab-todo/rest/todo/${todo_id}"
def conn = new URL(url).openConnection()
conn.requestMethod = "DELETE"
conn.doOutput = true

if (userid && password) {
    conn.setRequestProperty("Authorization", "Basic ${userid}:${password}")
}

if (conn.responseCode == conn.HTTP_OK) {
    input = conn.inputStream
    input.eachLine {
        println it
    }
}

conn.disconnect()