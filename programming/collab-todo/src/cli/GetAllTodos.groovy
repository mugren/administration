package cli

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 2/11/13
 * Time: 3:26 PM
 * To change this template use File | Settings | File Templates.
 */

import groovy.util.XmlSlurper

if (args.size() < 2 )
{
    //USAGE()
    println """Usage: groovy GetAllTodos userid password"""
    System.exit(1)
}

// Define some thing we will need.
def userid = args[0]
def password = args[1]
def url = "http://localhost:8080/collab-todo/rest/todo"
//def userInfoUrl = "http://localhost:8080/collab-todo/userInfo?rest=rest"
def slurper = new XmlSlurper()

println "\nGetting All Todos:"

def conn = new URL(url).openConnection()
conn.requestMethod = "GET"
conn.doOutput = true

if (userid && password) {
    conn.setRequestProperty("Authorization", "Basic ${userid}:${password}")
}

if (conn.responseCode == conn.HTTP_OK) {
    def response

    conn.inputStream.withStream {
        //println "$it"
        response = slurper.parse(it)
        println "$response"
    }
    println "\nNo. of Todo Records: ${response.todo.size()}"
    response.todo.each {
        println "-------------------------------------"
        println "Id:             ${it.@id}"
        println "Name:           $it.name"
        println "Note:           $it.note"
        println "Owner:          ${it.owner.@id}"
        println "Create Date:    $it.createdDate"
        println "Completed Date: $it.completedDate"
        println "Due Date:       $it.dueDate"
        println "Priority:       $it.priority"
        println "Status:         $it.status"
    }
}

conn.disconnect()