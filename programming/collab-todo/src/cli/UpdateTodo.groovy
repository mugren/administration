package cli

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 2/11/13
 * Time: 6:30 PM
 * To change this template use File | Settings | File Templates.
 */
import jline.ConsoleReader

ConsoleReader cr = new jline.ConsoleReader()

// Prompt for UserID and Password
def userid = cr.readLine("User id : ")
def password = cr.readLine("Password: ", new Character('*' as char))
println ""
def todo_id = cr.readLine("Todo Item id: ")

// Things that can be updated
def id
def name
def priority
def status

// Get the current values
def url = "http://localhost:8080/collab-todo/rest/todo/${todo_id}"
def slurper = new XmlSlurper()

def conn = new URL(url).openConnection()
conn.requestMethod = "GET"
conn.doOutput = true
conn.setRequestProperty("Authorization", "Basic ${userid}:${password}")

if (conn.responseCode == conn.HTTP_OK) {
    def response

    conn.inputStream.withStream {
        response = slurper.parse(it)
    }
    response.each {
        id = it.@id
        name = it.name
        priority = it.priority
        status = it.status
    }
}

conn.disconnect()

// Prompt for Changes with current values
def tname = cr.readLine("Name (${name}): ")
// Ako tname e null togos name=name , ako tname != null togas name=tname
name = tname ? tname : name
def tstatus = cr.readLine("Status (${status}): ")
status = tstatus ? tstatus : status
def tpriority = cr.readLine("Priority (${priority}): ")
priority = tpriority ? tpriority : priority

// Update the Todo
url = "http://localhost:8080/collab-todo/rest/todo"
conn = new URL(url).openConnection()
conn.requestMethod = "POST"
conn.doOutput = true
conn.doInput = true

def data = "id=${id}&name=${name}&status=${status}&priority=${priority}"

conn.outputStream.withWriter { out ->
    out.write(data)
    out.flush()
}

if (conn.responseCode == conn.HTTP_OK) {
    input = conn.inputStream
    input.eachLine {println it }
}
conn.disconnect()