package main.groovy

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 1/25/13
 * Time: 5:13 PM
 * To change this template use File | Settings | File Templates.
 */
def url = "http://localhost:8080/collab-todo/rest/todo/3"
def conn = new URL(url).openConnection()
conn.requestMethod = "DELETE"
conn.doOutput = true
if (conn.responseCode == conn.HTTP_OK) {
    input = conn.inputStream
    input.eachLine {
        println it
    }
}
conn.disconnect()

