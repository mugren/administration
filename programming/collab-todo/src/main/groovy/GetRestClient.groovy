package main.groovy



def slurper = new XmlSlurper()

def url = "http://localhost:8080/collab-todo/rest/todo/1"

def conn = new URL(url).openConnection()
conn.requestMethod = "GET"
conn.doOutput = true


if (conn.responseCode == conn.HTTP_OK) {
    def response

    conn.inputStream.withStream {
        response = slurper.parse(it)
    }

    def id = response.@id
    println "$id - $response.name"
}
conn.disconnect()
