package main.groovy

/**
 * Created with IntelliJ IDEA.
 * User: igor
 * Date: 1/25/13
 * Time: 5:32 PM
 * To change this template use File | Settings | File Templates.
 */

def url = "http://localhost:8080/collab-todo/rest/todo"
def conn = new URL(url).openConnection()
conn.requestMethod = "POST"
conn.doOutput = true
conn.doInput = true

def data = "id=4&note=" + new Date()
conn.outputStream.withWriter { out ->
    out.write(data)
    out.flush()
}
if (conn.responseCode == conn.HTTP_OK) {
    input = conn.inputStream
    input.eachLine {
        println it
    }
}
conn.disconnect()
