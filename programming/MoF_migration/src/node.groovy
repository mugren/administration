/**
 * Created by igor on 1/15/14.
 */

drupal_sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/drupal?autoReconnect=true",
        "drupal",
        "ada",
        "com.mysql.jdbc.Driver")

drupal_backup_sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/drupal_backup?autoReconnect=true",
        "drupal",
        "ada",
        "com.mysql.jdbc.Driver")

def nodeLang = [:]

//drupal_backup_sql.eachRow('select * from localizernode'){
//    println it
//    nodeLang.put(it.nid,it.locale)
//}
//
//nodeLang.each {node->
//    drupal_sql.executeUpdate('update node set language = ? where nid like ?',
//            [node.value.toString(),node.key])
//}

drupal_backup_sql.eachRow('select * from localizer'){
    println it
    nodeLang.put(it.nid,it.pid)
}

nodeLang.each {node->
    drupal_sql.executeUpdate('update node set tnid = ? where nid like ?',
            [node.value,node.key])
}