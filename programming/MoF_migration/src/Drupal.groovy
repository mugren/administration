/**
 * Created by igor on 1/15/14.
 */
class Drupal {
    private String name
    private int version
    private String codeName

    Drupal() {
    }

    String getName() {
        return name
    }

    void setName(String name) {
        this.name = name
    }

    int getVersion() {
        return version
    }

    void setVersion(int version) {
        this.version = version
    }

    String getCodeName() {
        return codeName
    }

    void setCodeName(String codeName) {
        this.codeName = codeName
    }
}
