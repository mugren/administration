/**
 * Created by igor on 1/15/14.
 */

drupal_sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/drupal?autoReconnect=true",
        "drupal",
        "ada",
        "com.mysql.jdbc.Driver")

drupal_backup_sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/drupal_backup?autoReconnect=true",
        "drupal",
        "ada",
        "com.mysql.jdbc.Driver")

def ids = []
def idLang = [:]
drupal_sql.eachRow('select pid from url_alias'){
    println it
    ids.add(it.pid)
}

ids.each {pid->
    drupal_sql.eachRow("select language from node where nid like $pid "){
        println it
        idLang.put(pid,it.language)
    }
}

idLang.each{idl->
    drupal_sql.executeUpdate('update url_alias set language = ? where pid like ?',
            [idl.value,idl.key])
}
