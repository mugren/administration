grails {
    mongo {
        host = "10.10.30.30"
        port = 27017
        username = "grails_user"
        password = "grails_pwd"
        databaseName = "grails_test"
    }
}