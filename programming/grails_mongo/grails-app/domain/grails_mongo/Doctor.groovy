package grails_mongo

class Doctor {

    static constraints = {
    }

    String first
    String last
    String degree
    String specialty
}
