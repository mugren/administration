
/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 10/8/13
 * Time: 12:06 AM
 * To change this template use File | Settings | File Templates.
 */
sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/grails?autoReconnect=true",
        "grails_user",
        "server",
        "com.mysql.jdbc.Driver")

ddl = """
CREATE TABLE usgs_airports (
  airport_id bigint(20) not null,
  locid varchar(4),
  feature varchar(80),
  airport_name varchar(80),
  state varchar(2),
  county varchar(50),
  latitude varchar(30),
  longitude varchar(30),
  primary key(airport_id)
);
"""

sql.execute(ddl)