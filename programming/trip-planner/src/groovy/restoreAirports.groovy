
/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 10/8/13
 * Time: 12:00 AM
 * To change this template use File | Settings | File Templates.
 */

if(args.size()){
    f = new File(args[0])
    println f

    sql = groovy.sql.Sql.newInstance(
            "jdbc:mysql://10.10.30.30:3306/grails?autoReconnect=true",
            "grails_user",
            "server",
            "com.mysql.jdbc.Driver")

    items = new groovy.util.XmlParser().parse(f)
    items.item.each{item ->
        println "${item.@id} -- ${item.title.text()}"
        sql.execute(
                "insert into item (version, title, short_description, description, url, type, date_posted, posted_by)" +
                " values(?,?,?,?,?,?,?,?)",
        [0, item.title.text(), item.shortDescription.text(), item.description.text(),
                item.url.text(), item.type.text(), item.datePosted.text(),
                item.postedBy.text()]
        )
    }
}
else{
    println "USAGE: itemsRestore [filename]"
}