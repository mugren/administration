
/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 10/7/13
 * Time: 11:19 PM
 * To change this template use File | Settings | File Templates.
 */

sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.30:3306/grails?autoReconnect=true",
        "grails_user",
        "server",
        "com.mysql.jdbc.Driver")

x = new groovy.xml.MarkupBuilder()

x.airports{
    sql.eachRow("select * from airport order by id"){ row ->
        airport(id:row.id){
            version(row.version)
            name(row.name)
            city(row.city)
            state(row.state)
            country(row.country)
            iata(row.iata)
            lat(row.lat)
            lng(row.lng)
        }
    }
}