package trip.planner;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 10/8/13
 * Time: 1:28 AM
 * To change this template use File | Settings | File Templates.
 */
import javax.persistence.*;
@Entity
@Table(name="usgs_airports")
public class AirportAnnotation {
    private long id;
    private String name;
    private String iata;
    private String state;
    private String lat;
    private String lng;

    @Id
    @Column(name="airport_id", nullable=false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Column(name="airport_name", nullable=false)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name="locid", nullable=false)
    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    @Column(name="state", nullable=false)
    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    @Column(name="latitude", nullable=false)
    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    @Column(name="longitude", nullable=false)
    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String toString(){
        return iata + " - " + name;
    }
}

//class AirportAnnotationConstraints {
//
//    static constraints = {
//        name()
//        iata(maxSize:3)
//        state(maxSize:2)
//        lat()
//        lng()
//    }
//}
