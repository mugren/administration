package trip.planner;

/**
 * Created with IntelliJ IDEA.
 * User: Igor
 * Date: 10/8/13
 * Time: 1:07 AM
 * To change this template use File | Settings | File Templates.
 */
public class AirportHbm {
    private long id;
    private String name;
    private String iata;
    private String state;
    private String lat;
    private String lng;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getIata() {
        return iata;
    }

    public void setIata(String iata) {
        this.iata = iata;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String toString(){
        return iata + " - " + name;
    }
}

//class AirportHbmConstraints {
//    static constraints = {
//        name()
//        iata(maxSize:3)
//        state(maxSize:2)
//        lat()
//        lng()
//    }
//}