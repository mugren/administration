package trip.planner



import grails.test.mixin.*
import org.junit.*
import org.springframework.web.context.request.RequestAttributes
import org.springframework.web.context.request.RequestContextHolder

import java.text.SimpleDateFormat

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(HotelStay)
class HotelStayTests extends GroovyTestCase {

    //@Mock
    private RequestAttributes attrs;

    @Before
    public void before() {
       // MockitoAnnotations.initMocks(this);
        RequestContextHolder.setRequestAttributes(attrs);

        // do you when's on attrs
    }

    void testSomething() {
        HotelStay hs = new HotelStay(hotel:"Sheraton")
        assertEquals "Sheraton", hs.hotel
    }

    void testToString() {
        def h = new HotelStay(hotel:"Hilton")
        def df = new SimpleDateFormat("MM/dd/yyyy")
        h.checkIn = df.parse("10/1/2008")
        h.checkOut = df.parse("10/5/2008")
        println h
        assertEquals "Hilton (Wednesday to Sunday)",h.toString()
        assertToString h,"Hilton (Wednesday to Sunday)"
    }

    void testBlankHotel(){
        def h = new HotelStay(hotel:"")
        println h.validate()
        assertFalse "there should be errors", h.validate()
        println h.hasErrors()
        assertTrue "another way to check for errors after you call validate()", h.hasErrors()
    }

    void testBlankHotelExt(){
        def h = new HotelStay(hotel:"")
        assertFalse "there should be errors", h.validate()
        assertTrue "another way to check for errors after you call validate()", h.hasErrors()

        println "\nErrors:"
        println h.errors ?: "no errors found"

        def badField = h.errors.getFieldError('hotel')
        println "\nBadField:"
        println badField ?: "hotel wasn't a bad field"
        assertNotNull "I'm expecting to find an error on the hotel field", badField


        def code = badField?.codes.find {it == 'hotelStay.hotel.blank'}
        println "\nCode:"
        println code ?: "the blank hotel code wasn't found"
        assertNotNull "the blank hotel field should be the culprit", code
    }

    void testCheckOutIsNotBeforeCheckIn(){
        def h = new HotelStay(hotel:"Radisson")
        def df = new SimpleDateFormat("MM/dd/yyyy")
        h.checkIn = df.parse("10/15/2008")
        h.checkOut = df.parse("10/10/2008")

        assertFalse "there should be errors", h.validate()
        def badField = h.errors.getFieldError('checkOut')
        assertNotNull "I'm expecting to find an error on the checkOut field", badField
        def code = badField?.codes.find {it == 'hotelStay.checkOut.validator.invalid'}
        assertNotNull "the checkOut field should be the culprit", code
    }

    void testNoFormat() {
        def output =
            new DateTagLib().customDateFormat(format:null, body:"2008-10-01 00:00:00.0")
        println "\ncustomDateFormat using the default format:"
        println output

        assertEquals "was the default format used?", "10/01/2008", output
    }

    void testCustomFormat() {
        def output =
            new DateTagLib().customDateFormat(format:"EEEE", body:"2008-10-01 00:00:00.0")
        assertEquals "was the custom format used?", "Wednesday", output
    }
}
