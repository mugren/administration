package trip.planner



import org.junit.*
import grails.test.mixin.*

@TestFor(AirportMappingController)
@Mock(AirportMapping)
class AirportMappingControllerTests {

    def populateValidParams(params) {
        assert params != null
        // TODO: Populate valid properties like...
        //params["name"] = 'someValidName'
    }

    void testIndex() {
        controller.index()
        assert "/airportMapping/list" == response.redirectedUrl
    }

    void testList() {

        def model = controller.list()

        assert model.airportMappingInstanceList.size() == 0
        assert model.airportMappingInstanceTotal == 0
    }

    void testCreate() {
        def model = controller.create()

        assert model.airportMappingInstance != null
    }

    void testSave() {
        controller.save()

        assert model.airportMappingInstance != null
        assert view == '/airportMapping/create'

        response.reset()

        populateValidParams(params)
        controller.save()

        assert response.redirectedUrl == '/airportMapping/show/1'
        assert controller.flash.message != null
        assert AirportMapping.count() == 1
    }

    void testShow() {
        controller.show()

        assert flash.message != null
        assert response.redirectedUrl == '/airportMapping/list'

        populateValidParams(params)
        def airportMapping = new AirportMapping(params)

        assert airportMapping.save() != null

        params.id = airportMapping.id

        def model = controller.show()

        assert model.airportMappingInstance == airportMapping
    }

    void testEdit() {
        controller.edit()

        assert flash.message != null
        assert response.redirectedUrl == '/airportMapping/list'

        populateValidParams(params)
        def airportMapping = new AirportMapping(params)

        assert airportMapping.save() != null

        params.id = airportMapping.id

        def model = controller.edit()

        assert model.airportMappingInstance == airportMapping
    }

    void testUpdate() {
        controller.update()

        assert flash.message != null
        assert response.redirectedUrl == '/airportMapping/list'

        response.reset()

        populateValidParams(params)
        def airportMapping = new AirportMapping(params)

        assert airportMapping.save() != null

        // test invalid parameters in update
        params.id = airportMapping.id
        //TODO: add invalid values to params object

        controller.update()

        assert view == "/airportMapping/edit"
        assert model.airportMappingInstance != null

        airportMapping.clearErrors()

        populateValidParams(params)
        controller.update()

        assert response.redirectedUrl == "/airportMapping/show/$airportMapping.id"
        assert flash.message != null

        //test outdated version number
        response.reset()
        airportMapping.clearErrors()

        populateValidParams(params)
        params.id = airportMapping.id
        params.version = -1
        controller.update()

        assert view == "/airportMapping/edit"
        assert model.airportMappingInstance != null
        assert model.airportMappingInstance.errors.getFieldError('version')
        assert flash.message != null
    }

    void testDelete() {
        controller.delete()
        assert flash.message != null
        assert response.redirectedUrl == '/airportMapping/list'

        response.reset()

        populateValidParams(params)
        def airportMapping = new AirportMapping(params)

        assert airportMapping.save() != null
        assert AirportMapping.count() == 1

        params.id = airportMapping.id

        controller.delete()

        assert AirportMapping.count() == 0
        assert AirportMapping.get(airportMapping.id) == null
        assert response.redirectedUrl == '/airportMapping/list'
    }


    void testWithBadIata(){
        def controller = new AirportMappingController()
        controller.metaClass.getParams = {->
            return ["id":"foo"]
        }
        controller.iata()
        def response = controller.response.contentAsString
        assertTrue response.contains("\"name\":\"Not found\"")
        println "Response for airport/iata/foo: ${response}"
    }
    //This test is not working
    void testWithGoodIata(){
        def controller = new AirportMappingController()
        controller.metaClass.getParams = {->
            return ["id":"den"]
        }
        controller.iata()
        def response = controller.response.contentAsString
        assertTrue response.contains("Denver")
        println "Response for airport/iata/den: ${response}"
    }

}
