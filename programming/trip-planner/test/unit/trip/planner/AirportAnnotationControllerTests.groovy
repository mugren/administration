package trip.planner



import grails.test.mixin.*

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(AirportAnnotationController)
class AirportAnnotationControllerTests {

    void testSomething() {
        fail "Implement me"
    }
}
