package trip.planner

import grails.converters.JSON
import org.springframework.dao.DataIntegrityViolationException

class AirportMappingController {

    static allowedMethods = []

    def geocoderService

    def index() {
        redirect(action: "list", params: params)
    }

    def list(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        [airportMappingInstanceList: AirportMapping.list(params), airportMappingInstanceTotal: AirportMapping.count()]
    }

    def show(Long id) {
        def airportMappingInstance = AirportMapping.get(id)
        if (!airportMappingInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'airportMapping.label', default: 'AirportMapping'), id])
            redirect(action: "list")
            return
        }

        [airportMappingInstance: airportMappingInstance]
    }

    def iata = {
        def iata = params.id?.toUpperCase() ?: "NO IATA"
        println iata
        //def airport = AirportMapping.findByIata(iata)
        def airport = geocoderService.geocodeAirportJSON(iata)
        println  airport
//        def i = AirportMapping.findByIata("DEN")
//        println i
//        def j = AirportMapping.findByIata("ILI")
//        println j
        if(!airport){
            airport = new AirportMapping(iata:iata, name:"Not found")
        }
        render airport as JSON
    }

}
