package trip.planner

import org.springframework.dao.DataIntegrityViolationException
import grails.converters.*

class AirportController {

    def geocoderService

    static allowedMethods = [save: "POST", update: "POST", delete: "POST"]

    def index() {
//        redirect(action: "list", params: params)
        switch(request.method){
            case "POST":
                println params
                def airport = new Airport()
                airport.iata = request.XML.@iata
                airport.name = request.XML."official-name"
                airport.city = request.XML.city
                airport.state = request.XML.state
                airport.country = request.XML.country
                airport.lat = request.XML.location.@latitude
                airport.lng = request.XML.location.@longitude
                if(airport.save()){
                    response.status = 201 // Created
                    render airport as XML
                }
                else{
                    response.status = 500 //Internal Server Error
                    render "Could not create new Airport due to errors:\n ${airport.errors}"
                }
                //render "Create\n"
                break
            case "GET":
                if(params.iata){
                    render Airport.findByIata(params.iata) as XML
                }
                else{
                    render Airport.list() as XML
                }
                //render "Retrieve\n"
                break
            case "PUT":
                println params
                def airport = Airport.findByIata(request.XML.@iata.toString())
                airport.properties = request*.XML
                if(airport.save()){
                    response.status = 200 // OK
                    render airport as XML
                }
                else{
                    response.status = 500 //Internal Server Error
                    render "Could not create new Airport due to errors:\n ${airport.errors}"
                }
                //render "Update\n"
                break
            case "DELETE":
                if(params.iata){
                    def airport = Airport.findByIata(params.iata)
                    if(airport){
                        airport.delete()
                        render "Successfully Deleted."
                    }
                    else{
                        response.status = 404 //Not Found
                        render "${params.iata} not found."
                    }
                }
                else{
                    response.status = 400 //Bad Request
                    render """DELETE request must include the IATA code
                  Example: /rest/airport/iata
        """
                }
               // render "Delete\n"
                break
        }
    }

    def debugAccept = {
        def clientRequest = request.getHeader("accept")
        def serverResponse = request.format
        render "Client: ${clientRequest}\nServer: ${serverResponse}\n"
    }

    def xmlList = {
        render Airport.list() as XML
    }

    def xmlShow = {
        render Airport.get(params.id) as XML
    }

    def customXmlList = {
        def list = Airport.list()
        render(contentType:"text/xml"){
            airports{
                for(a in list){
                    airport(id:a.id, iata:a.iata){
                        "official-name"(a.name)
                        city(a.city)
                        state(a.state)
                        country(a.country)
                        location(latitude:a.lat, longitude:a.lng)
                    }
                }
            }
        }
    }

    def list(Integer max) {
        if(!params.max) params.max = 10
        def list = Airport.list(params)
        withFormat{
            html{
                return [airportInstanceList:list]
            }
            xml{
                render list as XML
            }
        }
//        params.max = Math.min(max ?: 10, 100)
//        [airportInstanceList: Airport.list(params), airportInstanceTotal: Airport.count()]
    }

    def mlist = {
        if(!params.max) params.max = 5
        [airportList:Airport.list(params)]
    }

    def getXml = {
        render Airport.findByIata(params.iata) as XML
    }

    def getJson = {
        def airport = Airport.findByIata(params.iata)

        if(!airport){
            airport = new Airport(iata:params.iata, name:"Not found")
        }

        render airport as JSON
    }

    def geocode = {
        def result = geocoderService.geocodeAirport(params.iata)
        render result as JSON
    }

    def create() {
        [airportInstance: new Airport(params)]
    }

//    def save() {
//        def airportInstance = new Airport(params)
//        if (!airportInstance.save(flush: true)) {
//            render(view: "create", model: [airportInstance: airportInstance])
//            return
//        }
//
//        flash.message = message(code: 'default.created.message', args: [message(code: 'airport.label', default: 'Airport'), airportInstance.id])
//        redirect(action: "show", id: airportInstance.id)
//    }

    def save = {
        def results = geocoderService.geocodeAirport(params.iata)
        println results
        //merging two HashMaps in Groovy is as simple as adding them together.
        def airport = new Airport(params + results)
        if(!airport.hasErrors() && airport.save()) {
            flash.message = "Airport ${airport.id} created"
            redirect(action:'show',id:airport.id)
        }
        else {
            render(view:'create',model:[airport:airport])
        }
    }
    def show(Long id) {
        def airportInstance = Airport.get(id)
        if (!airportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'airport.label', default: 'Airport'), id])
            redirect(action: "list")
            return
        }

        [airportInstance: airportInstance]
    }

    def edit(Long id) {
        def airportInstance = Airport.get(id)
        if (!airportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'airport.label', default: 'Airport'), id])
            redirect(action: "list")
            return
        }

        [airport: airportInstance]
    }
//
//    def update(Long id, Long version) {
//        def airportInstance = Airport.get(id)
//        if (!airportInstance) {
//            flash.message = message(code: 'default.not.found.message', args: [message(code: 'airport.label', default: 'Airport'), id])
//            redirect(action: "list")
//            return
//        }
//
//        if (version != null) {
//            if (airportInstance.version > version) {
//                airportInstance.errors.rejectValue("version", "default.optimistic.locking.failure",
//                        [message(code: 'airport.label', default: 'Airport')] as Object[],
//                        "Another user has updated this Airport while you were editing")
//                render(view: "edit", model: [airportInstance: airportInstance])
//                return
//            }
//        }
//
//        airportInstance.properties = params
//
//        if (!airportInstance.save(flush: true)) {
//            render(view: "edit", model: [airportInstance: airportInstance])
//            return
//        }
//
//        flash.message = message(code: 'default.updated.message', args: [message(code: 'airport.label', default: 'Airport'), airportInstance.id])
//        redirect(action: "show", id: airportInstance.id)
//    }



    def update = {
        def airport = Airport.get( params.id )
        if(airport) {
            def results = geocoderService.geocodeAirport(params.iata)
            airport.properties = params + results
            if(!airport.hasErrors() && airport.save()) {
                flash.message = "Airport ${params.id} updated"
                redirect(action:'show',id:airport.id)
            }
            else {
                render(view:'edit',model:[airport:airport])
            }
        }
        else {
            flash.message = "Airport not found with id ${params.id}"
            redirect(action:'edit',id:params.id)
        }
    }

    def delete(Long id) {
        def airportInstance = Airport.get(id)
        if (!airportInstance) {
            flash.message = message(code: 'default.not.found.message', args: [message(code: 'airport.label', default: 'Airport'), id])
            redirect(action: "list")
            return
        }

        try {
            airportInstance.delete(flush: true)
            flash.message = message(code: 'default.deleted.message', args: [message(code: 'airport.label', default: 'Airport'), id])
            redirect(action: "list")
        }
        catch (DataIntegrityViolationException e) {
            flash.message = message(code: 'default.not.deleted.message', args: [message(code: 'airport.label', default: 'Airport'), id])
            redirect(action: "show", id: id)
        }
    }

    def map = {
        [airportList: Airport.list()]
    }
}
