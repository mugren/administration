package trip.planner

import java.text.SimpleDateFormat

class HotelStay {

    String hotel
    Date checkIn
    Date checkOut

    static constraints = {
        hotel(blank: false)
        checkIn()
        //obj is this, val is the entered value in field
        checkOut(validator:{val, obj->
            return val.after(obj.checkIn)
        })
    }

    String toString(){
        def sdf = new SimpleDateFormat("EEEE")
        "${hotel} (${sdf.format(checkIn)} to ${sdf.format(checkOut)})"
    }
}
