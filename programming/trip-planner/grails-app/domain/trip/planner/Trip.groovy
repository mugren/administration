package trip.planner

class Trip {

    String name
    static hasMany = [flights:Flight]


    static constraints = {
        name()
    }


}
