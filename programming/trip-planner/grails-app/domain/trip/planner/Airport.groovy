package trip.planner

class Airport {



    String name
    String iata
    String city
    String state
    String country
    String lat
    String lng

    static constraints = {
        name()
        iata(maxSize:3)
        city()
        state(maxSize:2)
        country()
    }

    String toString(){
        "${iata} - ${name}"
    }
}
