package trip.planner

class Airline {

    static hasMany = [flights:Flight]

//    static mapping = {
//        table 'some_other_table_name'
//        columns {
//            name column:'airline_name'
//            url column:'link'
//            frequentFlyer column:'ff_id'
//        }
//    }

    String name
    String iata
    String frequentFlyer


    static constraints = {
        name(blank:false, maxSize:100)
        iata(maxSize:3)
        frequentFlyer(blank:true)

    }

    String toString(){
        "${iata} - ${name}"
    }
}
