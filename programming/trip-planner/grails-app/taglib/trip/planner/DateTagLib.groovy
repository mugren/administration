package trip.planner

import java.text.SimpleDateFormat

class DateTagLib {
    def thisYear = {
        out << Calendar.getInstance().get(Calendar.YEAR)
    }

    def copyright = { attrs, body ->
        out << "<div id='copyright'>"
        out << "&copy; ${attrs['startYear']} - ${thisYear()}, ${body()}"
        out << " All Rights Reserved."
        out << "</div>"
    }

    def customDateFormat = {attrs, body ->
        def b = attrs.body ?: body()
        //The body is actual new Date() object
        def d = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").parse(b.toString())

        //if no format attribute is supplied, use this
        def pattern = attrs["format"] ?: "MM/dd/yyyy"
        out << new SimpleDateFormat(pattern).format(d)
    }
}
