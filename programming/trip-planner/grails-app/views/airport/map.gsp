<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="initial-scale=1.0, user-scalable=no" />
    <style type="text/css">
    html { height: 100% }
    body { height: 100%; margin: 0; padding: 0 }
    #map-canvas { height: 100% }
    </style>
    <script type="text/javascript"
            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH1YPI3zpCSpikz_ehcXY6ZDVHslGTo14&sensor=true">
    </script>
    <script type="text/javascript">

        function initialize() {



            var mapOptions = {
                center: new google.maps.LatLng(39.833333, -98.583333),
                zoom: 4,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };


            var map = new google.maps.Map(document.getElementById("map-canvas"),
                    mapOptions);

            <g:each in="${airportList}" status="i" var="airport">

            var point${airport.id} = new google.maps.LatLng(${airport.lat}, ${airport.lng});

            var marker${airport.id} = new google.maps.Marker(
                    {
                        position:point${airport.id} ,
                        map:map
                    });

            var infoWindow${airport.id} = new google.maps.InfoWindow(
                    {
                       content:"${airport.iata}<br/>${airport.name}"
                    }
            ) ;
                    ;
            google.maps.event.addListener(marker${airport.id}, 'click',
                    function() {
                        infoWindow${airport.id}.open(map, marker${airport.id});
                    });

            </g:each>


//            var myLatlng = new google.maps.LatLng(39.8583188, -104.6674674);
//            var marker = new google.maps.Marker({
//                position:myLatlng,
//                map:map
//
//            });

            var infoWindow = new google.maps.InfoWindow({
                content: "DEN<br/>Denver International Airport"
            });

            google.maps.event.addListener(marker, 'click',
                function() {
                infoWindow.open(map, marker);
            });

        }  // Initialize
//        function toggleBounce() {
//
//            if (marker.getAnimation() != null) {
//                marker.setAnimation(null);
//            } else {
//                marker.setAnimation(google.maps.Animation.BOUNCE);
//            }
//        }

        google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>
<body>
<div id="map-canvas" style="width: 800px; height: 400px"/>
</body>
</html>