<%@ page import="trip.planner.Airport" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'airport.label', default: 'Airport')}" />
		<title><g:message code="default.edit.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#edit-airport" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="edit-airport" class="content scaffold-edit" role="main">
			<h1><g:message code="default.edit.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${airport}">
			<ul class="errors" role="alert">
				<g:eachError bean="${airport}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			%{--<g:form method="post" >--}%
				%{--<g:hiddenField name="id" value="${airportInstance?.id}" />--}%
				%{--<g:hiddenField name="version" value="${airportInstance?.version}" />--}%
				%{--<fieldset class="form">--}%
					%{--<g:render template="form"/>--}%
				%{--</fieldset>--}%
				%{--<fieldset class="buttons">--}%
					%{--<g:actionSubmit class="save" action="update" value="${message(code: 'default.button.update.label', default: 'Update')}" />--}%
					%{--<g:actionSubmit class="delete" action="delete" value="${message(code: 'default.button.delete.label', default: 'Delete')}" formnovalidate="" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--}%
				%{--</fieldset>--}%
			%{--</g:form>--}%
            <g:form method="post" >
                <input type="hidden" name="id" value="${airport?.id}" />
                <div class="dialog">
                    <table>
                        <tbody>
                        <tr class="prop">
                            <td valign="top" class="name"><label for="iata">Iata:</label></td>
                            <td valign="top"
                                class="value ${hasErrors(bean:airport,field:'iata','errors')}">
                                <input type="text"
                                       maxlength="3"
                                       id="iata"
                                       name="iata"
                                       value="${fieldValue(bean:airport,field:'iata')}"/>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name"><label for="city">City:</label></td>
                            <td valign="top"
                                class="value ${hasErrors(bean:airport,field:'city','errors')}">
                                <input type="text"
                                       id="city"
                                       name="city"
                                       value="${fieldValue(bean:airport,field:'city')}"/>
                            </td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Name:</td>
                            <td valign="top" class="value">${airport.name}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">State:</td>
                            <td valign="top" class="value">${airport.state}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Country:</td>
                            <td valign="top" class="value">${airport.country}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Lat:</td>
                            <td valign="top" class="value">${airport.lat}</td>
                        </tr>
                        <tr class="prop">
                            <td valign="top" class="name">Lng:</td>
                            <td valign="top" class="value">${airport.lng}</td>
                        </tr>
                        </tbody>
                    </table>
                </div>
                <div class="buttons">
                    <span class="button"><g:actionSubmit class="save" value="Update" /></span>
                    <span class="button">
                        <g:actionSubmit class="delete"
                                        onclick="return confirm('Are you sure?');"
                                        value="Delete" />
                    </span>
                </div>
            </g:form>
		</div>
	</body>
</html>
