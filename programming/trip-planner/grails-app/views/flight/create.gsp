<%@ page import="trip.planner.Flight" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'flight.label', default: 'Flight')}" />
		<title><g:message code="default.create.label" args="[entityName]" /></title>
        <g:javascript library="jquery" plugin="jquery"/>



        <script type="text/javascript">

            function get(airportField){

                console.log("get");
                var baseUrl = "${createLink(controller:'airport', action:'getJson')}"
                var element = "#"+airportField+"Iata";
                console.debug(element)
                var url = baseUrl + "?iata=" + $(element).val();
                console.debug(url)
                $.ajax( {
                    url:url,
                    type: 'GET',
                    async: true,
                    dataType:"json",
                    success: function(req) {
                        console.debug(req)
                        update(req, airportField);
                    }
                });
            }

            function update(airport, airportField){
//                var airport = eval( "(" + json + ")" )
                console.log(airportField);
                var aerodromPole = "#"+airportField + "Iata";

                console.log("Output: "+aerodromPole);
                var hidden = "#"+airportField + "\\.id"  ;
                console.log("Hidden:"+hidden);
                var innerHtml = $(aerodromPole).html();
                console.log(innerHtml);
                //$(aerodromPole).innerHTML = airport.iata + " - " + airport.name;
                $(aerodromPole).val(airport.iata+" - "+airport.name);
                innerHtml = $(aerodromPole).html();
                console.log(innerHtml);

                 airport.id == null ? $(hidden).val('-1') : $(hidden).val(airport.id);

                //aerodromPole.val("");

                console.log($(hidden).val());
                console.log($(aerodromPole).val());

            }

            function validate(){
                if( ("#departureAirport\\.id").val() == -1 ){
                    alert("Please supply a valid Departure Airport")
                    return false
                }

                if( ("#arrivalAirport\\.id").val() == -1 ){
                    alert("Please supply a valid Arrival Airport")
                    return false
                }

                return true
            }


        </script>

    </head>
	<body>
		<a href="#create-flight" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="create-flight" class="content scaffold-create" role="main">
			<h1><g:message code="default.create.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<g:hasErrors bean="${flightInstance}">
			<ul class="errors" role="alert">
				<g:eachError bean="${flightInstance}" var="error">
				<li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
				</g:eachError>
			</ul>
			</g:hasErrors>
			<g:form action="save" >
				<fieldset class="form">
					<g:render template="form"/>
				</fieldset>
				<fieldset class="buttons">
					<g:submitButton name="create" class="save" value="${message(code: 'default.button.create.label', default: 'Create')}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
