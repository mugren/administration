
<%@ page import="trip.planner.AirportMapping" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'airportMapping.label', default: 'AirportMapping')}" />
		<title><g:message code="default.show.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#show-airportMapping" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="list" action="list"><g:message code="default.list.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="show-airportMapping" class="content scaffold-show" role="main">
			<h1><g:message code="default.show.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<ol class="property-list airportMapping">
			
				<g:if test="${airportMappingInstance?.name}">
				<li class="fieldcontain">
					<span id="name-label" class="property-label"><g:message code="airportMapping.name.label" default="Name" /></span>
					
						<span class="property-value" aria-labelledby="name-label"><g:fieldValue bean="${airportMappingInstance}" field="name"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${airportMappingInstance?.iata}">
				<li class="fieldcontain">
					<span id="iata-label" class="property-label"><g:message code="airportMapping.iata.label" default="Iata" /></span>
					
						<span class="property-value" aria-labelledby="iata-label"><g:fieldValue bean="${airportMappingInstance}" field="iata"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${airportMappingInstance?.state}">
				<li class="fieldcontain">
					<span id="state-label" class="property-label"><g:message code="airportMapping.state.label" default="State" /></span>
					
						<span class="property-value" aria-labelledby="state-label"><g:fieldValue bean="${airportMappingInstance}" field="state"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${airportMappingInstance?.lat}">
				<li class="fieldcontain">
					<span id="lat-label" class="property-label"><g:message code="airportMapping.lat.label" default="Lat" /></span>
					
						<span class="property-value" aria-labelledby="lat-label"><g:fieldValue bean="${airportMappingInstance}" field="lat"/></span>
					
				</li>
				</g:if>
			
				<g:if test="${airportMappingInstance?.lng}">
				<li class="fieldcontain">
					<span id="lng-label" class="property-label"><g:message code="airportMapping.lng.label" default="Lng" /></span>
					
						<span class="property-value" aria-labelledby="lng-label"><g:fieldValue bean="${airportMappingInstance}" field="lng"/></span>
					
				</li>
				</g:if>
			
			</ol>
			<g:form>
				<fieldset class="buttons">
					<g:hiddenField name="id" value="${airportMappingInstance?.id}" />
				</fieldset>
			</g:form>
		</div>
	</body>
</html>
