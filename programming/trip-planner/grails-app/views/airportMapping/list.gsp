
<%@ page import="trip.planner.AirportMapping" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'airportMapping.label', default: 'AirportMapping')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-airportMapping" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
			</ul>
		</div>
		<div id="list-airportMapping" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
			<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
				<thead>
					<tr>
					
						<g:sortableColumn property="name" title="${message(code: 'airportMapping.name.label', default: 'Name')}" />
					
						<g:sortableColumn property="iata" title="${message(code: 'airportMapping.iata.label', default: 'Iata')}" />
					
						<g:sortableColumn property="state" title="${message(code: 'airportMapping.state.label', default: 'State')}" />
					
						<g:sortableColumn property="lat" title="${message(code: 'airportMapping.lat.label', default: 'Lat')}" />
					
						<g:sortableColumn property="lng" title="${message(code: 'airportMapping.lng.label', default: 'Lng')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${airportMappingInstanceList}" status="i" var="airportMappingInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${airportMappingInstance.id}">${fieldValue(bean: airportMappingInstance, field: "name")}</g:link></td>
					
						<td>${fieldValue(bean: airportMappingInstance, field: "iata")}</td>
					
						<td>${fieldValue(bean: airportMappingInstance, field: "state")}</td>
					
						<td>${fieldValue(bean: airportMappingInstance, field: "lat")}</td>
					
						<td>${fieldValue(bean: airportMappingInstance, field: "lng")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${airportMappingInstanceTotal}" />
			</div>
		</div>
	</body>
</html>
