<%@ page import="trip.planner.AirportMapping" %>



<div class="fieldcontain ${hasErrors(bean: airportMappingInstance, field: 'name', 'error')} ">
	<label for="name">
		<g:message code="airportMapping.name.label" default="Name" />
		
	</label>
	<g:textField name="name" value="${airportMappingInstance?.name}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: airportMappingInstance, field: 'iata', 'error')} ">
	<label for="iata">
		<g:message code="airportMapping.iata.label" default="Iata" />
		
	</label>
	<g:textField name="iata" maxlength="3" value="${airportMappingInstance?.iata}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: airportMappingInstance, field: 'state', 'error')} ">
	<label for="state">
		<g:message code="airportMapping.state.label" default="State" />
		
	</label>
	<g:textField name="state" maxlength="2" value="${airportMappingInstance?.state}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: airportMappingInstance, field: 'lat', 'error')} ">
	<label for="lat">
		<g:message code="airportMapping.lat.label" default="Lat" />
		
	</label>
	<g:textField name="lat" value="${airportMappingInstance?.lat}"/>
</div>

<div class="fieldcontain ${hasErrors(bean: airportMappingInstance, field: 'lng', 'error')} ">
	<label for="lng">
		<g:message code="airportMapping.lng.label" default="Lng" />
		
	</label>
	<g:textField name="lng" value="${airportMappingInstance?.lng}"/>
</div>

