<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 12/5/13
  Time: 12:56 PM
--%>

<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <title>Plan</title>

    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDH1YPI3zpCSpikz_ehcXY6ZDVHslGTo14&sensor=true"
            type="text/javascript"></script>
    <script type="text/javascript">
        var  map;
        var mapOptions = {
            center: new google.maps.LatLng(39.833333, -98.583333),
            zoom: 4,
            mapTypeId: google.maps.MapTypeId.ROADMAP
        };
        function load() {

            map = new google.maps.Map(document.getElementById("map"),
                    mapOptions);
        }
        google.maps.event.addDomListener(window, 'load', load);
    </script>

    <script type="text/javascript">
        var airportMarkers = []
        var line
        function addAirport(response, position) {
            console.log(response);
            var airport = eval('(' + response.responseText + ')')
            var label = airport.iata + " -- " + airport.name

            var location = new google.maps.LatLng(airport.lat,airport.lng);
            var marker = new google.maps.Marker({
                position: location,
                map: map
            });
            var j = i + 1;
            marker.setTitle(airport.title);
            addMarker(marker,label)

            if(airportMarkers[position] != null){
                line.removeMap(map);
            }
            if(airport.name != "Not found"){
                airportMarkers[position] = location
            }
            document.getElementById("airport_" + position).innerHTML = airport.name
            drawLine()
            showHotelsLink()
        }
        function drawLine(){
            if(line != null){
                line.removeMap(map);
            }

            if(airportMarkers.length == 2){
                line = new google.maps.Polyline({
                    path: location,
                    geodesic: true,
                    strokeColor: '#FF0000',
                    strokeOpacity: 1.0,
                    strokeWeight: 2
                });
                line.setMap(map);
            }
        }

        function addMarker(marker,message) {

            var infowindow = new google.maps.InfoWindow(
                    { content: message[number],
                        size: new google.maps.Size(50,50)
                    });
            google.maps.event.addListener(marker, 'click', function() {
                infowindow.open(map,marker);
            });
        }

        function showHotelsLink(){
            if(airportMarkers[1] != null){
                var hotels_link = document.getElementById("hotels_link")
                hotels_link.innerHTML = "<a href='#' onClick='loadHotels()'>Show Nearby Hotels...</a>"
            }
        }

        function loadHotels(){
            var url = "${createLink(controller:'hotelStay', action:'near')}"
            url += "?lat=" + airportMarkers[1].getLatLng().lat()
            url += "&lng=" + airportMarkers[1].getLatLng().lng()
            new Ajax.Request(url,{
                onSuccess: function(req) { showHotels(req) },
                onFailure: function(req) { displayError(req) }
            })
        }

        function displayError(response){
            var html = "response.status=" + response.status + "<br />"
            html += "response.responseText=" + response.responseText + "<br />"
            var hotels = document.getElementById("hotels")
            hotels.innerHTML = html
        }

        function showHotels(response){
            var results = eval( '(' + response.responseText + ')')
            var resultCount = 1 * results.ResultSet.totalResultsReturned
            var html = "<ul>"
            for(var i=0; i < resultCount; i++){
                html += "<li>" + results.ResultSet.Result[i].Title + "<br />"
                html += "Distance: " + results.ResultSet.Result[i].Distance + "<br />"
                html += "<hr />"
                html += "</li>"
            }
            html += "</ul>"
            var hotels = document.getElementById("hotels")
            hotels.innerHTML = html
        }
    </script>

</head>
<body onload="load()" onunload="GUnload()">
<div class="body">

    <div id="search" style="width:25%; float:left">
        <h1>Where to?</h1>
        <g:formRemote name="from_form"
                      url="[controller:'airportMapping', action:'iata']"
                      onSuccess="addAirport(e, 0)">
            From:<br/>
            <input type="text" name="id" size="3"/>
            <input type="submit" value="Search" />
        </g:formRemote>
        <div id="airport_0"></div>
        <g:formRemote name="to_form"
                      url="[controller:'airportMapping', action:'iata']"
                      onSuccess="addAirport(e, 1)">
            To: <br/>
            <input type="text" name="id" size="3"/>
            <input type="submit" value="Search" />
        </g:formRemote>
        <div id="airport_1"></div>
        <div id="hotels_link"></div>
        <div id="hotels"></div>
    </div>

    <div id="map" style="width:75%; height:100%; float:right"></div>
</div>
</body>
</html>