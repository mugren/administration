package trip.planner

import groovy.json.JsonSlurper

class GeocoderService {
    boolean transactional = false // if true wraps all db queries in a single method into one transaction. Reverts on error.

    // http://ws.geonames.org/search?name_equals=den&fcode=airp&style=full
    def geocodeAirport(String iata) {
        def base = "http://ws.geonames.org/search?"
        def qs = []
        qs << "name_equals=" + URLEncoder.encode(iata)
        qs << "fcode=airp"
        qs << "style=full"
        println qs
        def url = new URL(base + qs.join("&"))
        println url
        def connection = url.openConnection()

        def result = [:]
        if(connection.responseCode == 200){
            def xml = connection.content.text
            def geonames = new XmlSlurper().parseText(xml)
            result.name = geonames.geoname.name as String
            result.lat = geonames.geoname.lat as String
            result.lng = geonames.geoname.lng as String
            result.state = geonames.geoname.adminCode1 as String
            result.country = geonames.geoname.countryCode as String
        }
        else{
            log.error("GeocoderService.geocodeAirport FAILED")
            log.error(url)
            log.error(connection.responseCode)
            log.error(connection.responseMessage)
        }
        return result
    }

    def geocodeAirportJSON(String iata){
        def base = "http://ws.geonames.org/search?"
        def qs = []
        qs << "name_equals=" + URLEncoder.encode(iata)
        qs << "fcode=airp"
        qs << "style=full"
        qs << "type=json"
        println qs
        def url = new URL(base + qs.join("&"))
        println url
        def connection = url.openConnection()

        def result = [:]
        if(connection.responseCode == 200){
            def json = connection.content.text
            println json
            def geonames = new JsonSlurper().parseText(json)
            println geonames
            println geonames.get('totalResultsCount')
            println geonames.get('geonames').name
            println geonames.get('geonames').lat
            println geonames.get('geonames').lng
            println geonames.get('geonames').adminCode1
            println geonames.get('geonames').countryCode
            result.name = geonames.get('geonames').name
            result.lat = geonames.get('geonames').lat
            result.lng = geonames.get('geonames').lng
            result.state = geonames.get('geonames').adminCode1
            result.country = geonames.get('geonames').countryCode
        }
        else{
            log.error("GeocoderService.geocodeAirport FAILED")
            log.error(url)
            log.error(connection.responseCode)
            log.error(connection.responseMessage)
        }
        return result
    }

}
