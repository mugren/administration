<%--
  Created by IntelliJ IDEA.
  User: igor
  Date: 6/20/13
  Time: 5:00 PM
  To change this template use File | Settings | File Templates.
--%>



<% response.setContentType("text/vnd.wap.wml") %>
<?xml version="1.0"?>
<!DOCTYPE wml PUBLIC "-//PHONE.COM//DTD WML 1.1//EN"
   "http://www.phone.com/dtd/wml11.dtd" >

<wml>
    <card id="f1" title="Flight 1">
        <p mode="wrap">From: DEN</p>
        <p mode="wrap">To: ORD</p>
        <p mode="wrap">UAL 1234</p>
        <p mode="wrap">Jun 30, 10:30am</p>
        <p>
            <anchor>Next<go href="#f2"/></anchor>
        </p>
    </card>

    <card id="f2" title="Flight 2">
        <p mode="wrap">From: ORD</p>
        <p mode="wrap">To: DEN</p>
        <p mode="wrap">UAL 9876</p>
        <p mode="wrap">Jul 02, 1:15pm</p>
        <p>
            <anchor>Previous<go href="#f1"/></anchor>
        </p>
    </card>
</wml>



%{--<% response.setContentType("text/vnd.wap.wml") %>--}%
%{--<%--}%
    %{--def flightList = []--}%
    %{--flightList << [iata1:"DEN", iata2:"ORD"]--}%
    %{--flightList << [iata1:"ORD", iata2:"DEN"]--}%
%{--%>--}%

%{--<?xml version="1.0"?>--}%
%{--<!DOCTYPE wml PUBLIC "-//PHONE.COM//DTD WML 1.1//EN"--}%
   %{--"http://www.phone.com/dtd/wml11.dtd" >--}%

%{--<wml>--}%
    %{--<g:each in="${flightList}" var="${flight}" status="i">--}%
        %{--<card id="f${i}" title="Flight ${i}">--}%
            %{--<p mode="wrap">From: ${flight.iata1}</p>--}%
            %{--<p mode="wrap">To: ${flight.iata2}</p>--}%
            %{--<g:if test="${flightList.size() > i+1}">--}%
                %{--<p>--}%
                    %{--<anchor>Next<go href="#f${i}"/></anchor>--}%
                %{--</p>--}%
            %{--</g:if>--}%
        %{--</card>--}%
    %{--</g:each>--}%
%{--</wml>--}%