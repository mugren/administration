#!/bin/bash

PROJECT_NAME=$1

# Kopiraj hook skripti
cp -a /opt/migration/scripts/hooks/* /opt/svn/repositories/genrepsoft/$PROJECT_NAME/hooks/
# Zemi go UUID-to na orginalniot proekt
REMOTE_UUID=`ssh svn svnlook uuid /opt/svn/repositories/genrepsoft/$PROJECT_NAME`
# Postavigo toa UUID na ovoj mirror proekt
svnadmin setuuid /opt/svn/repositories/genrepsoft/$PROJECT_NAME $REMOTE_UUID
# Inicijaliziraj go user-ot svnsync so pocetna revizija na remote proektot
svnsync initialize http://svnmirror/genrepsoft/$PROJECT_NAME http://svn/genrepsoft/$PROJECT_NAME --sync-username svnsync --sync-password maverick@2010u
# Sinhroniziraj go user-ot svnsync so remote proektot
svnsync synchronize http://svnmirror/genrepsoft/$PROJECT_NAME --source-username svnsync --source-password maverick@2010u
