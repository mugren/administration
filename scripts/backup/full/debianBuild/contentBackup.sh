﻿#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

#* Backup partition *#
BACKUPPARTITION="/storage";

#* Backup dir *#
BACKUPDIRDAILY="$BACKUPPARTITION/debianBuild/$DATEYYMM/$DATEDD";

# Location of opt directory
OPT_DIR=opt

# Location of Hudson's defaults file
HUDSON_DEFAULTS_FILE=etc/default/hudson

# Location of MySQL configuration file
MYSQL_CONF_FILE=etc/mysql/my.cnf

# Location of scheduler-web properties file
SCHEDULER_PROPERTY_FILE=root/web-scheduler.properties

# List of Content to Backup:
IDS="
$OPT_DIR
$HUDSON_DEFAULTS_FILE
$MYSQL_CONF_FILE
$SCHEDULER_PROPERTY_FILE
"

# We start to compress applications one by one
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done
