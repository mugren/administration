﻿#!/bin/bash -e

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

#* Backup partition *#
BACKUPPARTITION="/storage";

#* Backup dir *#
BACKUPDIRDAILY="$BACKUPPARTITION/debianBuild/$DATEYYMM/$DATEDD";

### Server Setup ###
#* MySQL login username *#
MUSER="root";

#* MySQL login PASSWORD *#
MPASS="meerkat%2010j";

#* MySQL login HOST  name*#
MHOST="localhost";

#* MySQL login PORT number *#
MPORT="3306";

# *DO NOT BACKUP these databases *#
IGNOREDB="
information_schema
"

#* MySQL binaries *#
MYSQL=`which mysql`;
MYSQLDUMP=`which mysqldump`;

#* gzip binaries *#
GZIP=`which gzip`;

# Check if backup partition is mounted
if [ -n "$(mount -l | grep $BACKUPPARTITION)" ]; then
    echo "$BACKUPPARTITION is allready mounted";
else
    echo "Mounting $BACKUPPARTITION ...";
    mount $BACKUPPARTITION;
fi

# assuming that backup partition is mounted 
if [ ! -d $BACKUPDIRDAILY ]; then
    mkdir -p $BACKUPDIRDAILY
else
    echo "$BACKUPDIRDAILY allready exists!!!";
    exit 1;
fi

# get all database listing
DBS="$(mysql -u $MUSER -p$MPASS -h $MHOST -P $MPORT -Bse 'show databases')"

# start to dump database one by one
for db in $DBS
do
        DUMP="yes";
        if [ "$IGNOREDB" != "" ]; then
                for i in $IGNOREDB # Store all value of $IGNOREDB ON i
                do
                        if [ "$db" == "$i" ]; then # If result of $DBS(db) is equal to $IGNOREDB(i) then
                                DUMP="NO";         # SET value of DUMP to "no"
                                echo "$i database is being ignored!";
                        fi
                done
        fi

        if [ "$DUMP" == "yes" ]; then # If value of DUMP is "yes" then backup database
                FILE="$BACKUPDIRDAILY/mysql-$db.gz";
                echo "BACKING UP $db";
                $MYSQLDUMP --add-drop-database --opt --lock-all-tables -u $MUSER -p$MPASS -h $MHOST -P $MPORT $db | gzip > $FILE
        fi

done
