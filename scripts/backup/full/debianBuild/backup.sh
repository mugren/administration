﻿#!/bin/bash

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep "inet " | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Hudson"
/etc/init.d/hudson stop
echo "Stopping Nexus"
/etc/init.d/nexus stop
echo "Stopping Tomcat"
/etc/init.d/tomcat stop
echo "Backing up mysql databases..."
/storage/backup/full/bin/backupmysql.sh
echo "Backing up Content..."
/storage/backup/full/bin/contentBackup.sh
echo "Starting Hudson"
/etc/init.d/hudson start
echo "Starting Nexus"
/etc/init.d/nexus start
echo "Starting Tomcat"
/etc/init.d/tomcat start
echo "********************************************************************"
echo "********************************************************************"
