﻿#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/storage/backup/full/bin/backup.sh > /storage/backup/logs/$DATE.log
