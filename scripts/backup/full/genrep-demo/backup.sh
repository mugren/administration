﻿#!/bin/bash

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep "inet " | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Apache Tomcat Akademika"
/etc/init.d/tomcat-akademika stop
echo "Stopping Apache Tomcat Archive"
/etc/init.d/tomcat-archive stop
echo "Stopping Apache Tomcat MORM Author"
/etc/init.d/tomcat-author stop
echo "Stopping Apache Tomcat BJN Survey"
/etc/init.d/tomcat-bjn-survey stop
echo "Stopping Apache Tomcat MORM Internal1"
/etc/init.d/tomcat-internal1 stop
echo "Stopping Apache Tomcat MORM Internal2"
/etc/init.d/tomcat-internal2 stop
echo "Stopping Apache Tomcat MIS"
/etc/init.d/tomcat-mis stop
echo "Stopping Apache Tomcat MORM Portal"
/etc/init.d/tomcat-portal stop
echo "Stopping Apache Tomcat MORM Public"
/etc/init.d/tomcat-public stop
echo "Stopping Apache Tomcat Asseco Survey"
/etc/init.d/tomcat-survey stop
echo "Backing up mysql databases..."
/root/backup/full/bin/backupmysql.sh
echo "Backing up Content..."
/root/backup/full/bin/contentBackup.sh
echo "Starting Apache Tomcat Akademika"
/etc/init.d/tomcat-akademika start
echo "Starting Apache Tomcat Archive"
/etc/init.d/tomcat-archive start
echo "Starting Apache Tomcat MORM Author"
/etc/init.d/tomcat-author start
echo "Starting Apache Tomcat BJN Survey"
/etc/init.d/tomcat-bjn-survey start
echo "Starting Apache Tomcat MORM Internal1"
/etc/init.d/tomcat-internal1 start
echo "Starting Apache Tomcat MORM Internal2"
/etc/init.d/tomcat-internal2 start
echo "Starting Apache Tomcat MIS"
/etc/init.d/tomcat-mis start
echo "Starting Apache Tomcat MORM Portal"
/etc/init.d/tomcat-portal start
echo "Starting Apache Tomcat MORM Public"
/etc/init.d/tomcat-public start
echo "Starting Apache Tomcat Asseco Survey"
/etc/init.d/tomcat-survey start
echo "********************************************************************"
echo "********************************************************************"
