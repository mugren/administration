﻿#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

#* Backup partition *#
BACKUPPARTITION="/mnt/genrep-demo";

#* Backup dir *#
BACKUPDIRDAILY="$BACKUPPARTITION/backup/full/$DATEYYMM/$DATEDD";

# Location of opt directory
OPT_DIR=opt

# Location of e-arhiver properties file
ARCHIVER_PROPERTY_FILE=root/archive.properties

# Location of MySQL configuration file
MYSQL_CONF_FILE=etc/mysql/my.cnf

# List of Content to Backup:
IDS="
$OPT_DIR
$ARCHIVER_PROPERTY_FILE
$MYSQL_CONF_FILE
"

# We start to compress applications one by one
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done

# Unmount backup partition when the backup is done
echo "Unmounting $BACKUPPARTITION ...";
umount $BACKUPPARTITION;