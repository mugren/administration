#!/bin/bash

# DB2 Instance name
INSTANCE=$1

# DB2 Instance Database
DATABASE=$2

# Database export dir
EXPORTDIR="/home/$INSTANCE/backup/$DATABASE";

# We export the database
# Load the instance profile
if [ -f /home/$INSTANCE/sqllib/db2profile ]; then
	. /home/$INSTANCE/sqllib/db2profile
fi

db2 -v CONNECT TO $DATABASE USER $INSTANCE USING $INSTANCE;
db2 -v QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS;
db2 -v CONNECT RESET;
db2 -v BACKUP DATABASE $DATABASE TO "$EXPORTDIR" WITH 2 BUFFERS BUFFER 1024 PARALLELISM 1 WITHOUT PROMPTING;
db2 -v CONNECT TO $DATABASE;
db2 -v UNQUIESCE DATABASE;
db2 -v CONNECT RESET;

