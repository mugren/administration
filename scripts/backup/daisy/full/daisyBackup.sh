#!/bin/bash

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
cat /etc/network/interfaces | grep address | awk '{print $2}'
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop
echo "Backing up mysql databases..."
/backup/bin/backupmysql.sh
echo "Backing up Daisy Content..."
/backup/bin/daisyContentBackup.sh
echo "Starting Daisy Repository and Daisy Wiki: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
echo "********************************************************************"
echo "********************************************************************"
