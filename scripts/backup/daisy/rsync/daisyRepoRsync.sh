#!/bin/bash

# Remote Hostname or IP Address
REMOTEHOST="remotehostname";

# Remote Dir
REMOTEDIR="/backup/data2/daisyrepodata/";

# Local Dir
LOCALDIR="/data2/daisyrepodata/";

echo "Replicating Daisy Repository"
rsync -ave "ssh -i /backup/id_daisybackup_rsa" --delete \
--exclude "logs/*" \
--exclude "service/*" \
$LOCALDIR $REMOTEHOST:$REMOTEDIR

