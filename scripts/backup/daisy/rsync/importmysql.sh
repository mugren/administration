#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/backup/daisy/$DATEYYMM/$DATEDD";

### Server Setup ###
#* MySQL login user name *#
MUSER="root";

#* MySQL login PASSWORD name *#
MPASS="password";

#* MySQL login HOST name *#
MHOST="hostname";
MPORT="3306";

# DO NOT IMPORT these databases
IGNOREDB="
information_schema
mysql
"

#* MySQL binaries *#
MYSQL=`which mysql`;

# get all database listing
DBS="$(mysql -u $MUSER -p$MPASS -h $MHOST -P $MPORT -Bse 'show databases')";

# start to dump database one by one
for db in $DBS
do
        IMPORT="yes";
        if [ "$IGNOREDB" != "" ]; then
                for i in $IGNOREDB # Store all value of $IGNOREDB ON i
                do
                        if [ "$db" == "$i" ]; then # If result of $DBS(db) is equal to $IGNOREDB(i) then
                                IMPORT="NO";       # SET value of IMPORT to "no"
                                echo "$i database is being ignored!";
                        fi
                done
        fi

        if [ "$IMPORT" == "yes" ]; then # If value of IMPORT is "yes" then import database
                FILE="$BACKUPDIR/$db";
				if [ -f $FILE ]; then
					echo "IMPORTING $db";
					$MYSQL -u $MUSER -p$MPASS -h $MHOST -P $MPORT $db < $FILE;
				else
					echo "$FILE does not exists, not importing $db";
				fi
				
        fi

done

