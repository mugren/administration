#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/backup/bin/daisyRsync.sh > /backup/logs/daisyRsync$DATE.log

SUBJECT="Daisy Cron Daemon"
cat /backup/logs/$DATE.log | mail -s "$SUBJECT" mail@domail.com
