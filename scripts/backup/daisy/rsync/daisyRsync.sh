#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/backup/daisy/$DATEYYMM/$DATEDD";

# Remote Backup dir
REMOTEBACKUPDIR="/backup/daisy/$DATEYYMM/$DATEDD";

echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop
echo "Backing up mysql databases..."
/backup/bin/backupmysql.sh
echo "Importing mysql databases..."
/backup/bin/importmysql.sh
echo "Backing up Daisy Content..."
/backup/bin/daisyWikiRsync.sh
/backup/bin/daisyRepoRsync.sh
echo "Starting Daisy Repository and Daisy Wiki: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
echo "********************************************************************"
echo "********************************************************************"
