#!/bin/bash

# Remote Hostname or IP Address
REMOTEHOST="remotehostname";

# Remote Dir
REMOTEDIR="/backup/daisy/daisywikidata/";

# Local Dir
LOCALDIR="/data2/daisywikidata/";

echo "Replicating Daisy Wiki"
rsync -ave "ssh -i /backup/id_daisybackup_rsa" --delete \
--exclude "logs/*" \
--exclude "tmp/*" \
--exclude "service/*" \
$LOCALDIR $REMOTEHOST:$REMOTEDIR

