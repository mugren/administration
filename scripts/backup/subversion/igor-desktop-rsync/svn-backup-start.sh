#!/bin/bash



echo "********************************************************************"

echo "********************************************************************"

echo "The name of this script is $0"

echo "On the host:"

hostname

echo "With IP Address:"

ip addr show enp2s0 | grep inet | awk '{print $2}' | head -n 1

echo "With uptime:"

uptime

echo "********************************************************************"

echo "Report from SVN backup on: `date`"

echo "********************************************************************"

echo "********************************************************************"

echo "Syncronizing remote SVN source dumps with local directory..."

/opt/backup/bin/svn-backup-rsync.sh

echo "********************************************************************"

echo "********************************************************************"
