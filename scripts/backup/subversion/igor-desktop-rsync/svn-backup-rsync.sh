#!/bin/bash

RSYNC=/usr/bin/rsync 
SSH=/usr/bin/ssh 
KEY=/home/igor/.ssh/id_rsa

# Now in format: YYYY-MM-DD
DATE=`date +%F`;
# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;
# Now in format: DD
DATEDD=`date +%d`;

# Destination to write:
DATA_DIR="/storage/svn-backups";
LOCAL_DIR="${DATA_DIR}/${DATE}";
# Source of backup:
REMOTE_DIR="/storage/nfs_backups/subversion/svn/source/${DATEYYMM}/${DATEDD}/";


# Remote user and password:
RUSER="root";
RPASS="meerkat%2010j";
# Remote host:
RHOST="nas";

# Begin remote synchronization
# rsync 'options' 'source' 'destination'
if [ ! -d $LOCAL_DIR ]; then
mkdir "${LOCAL_DIR}"
fi
$RSYNC -vr -e "$SSH -i $KEY -l  $RUSER" $RHOST:$REMOTE_DIR $LOCAL_DIR 
