﻿#!/bin/sh
# Subversion Repositories backup script
# by Adeel Ahmad {adeel.ahmad+hackinggurus@networkncc.com}
# for http://www.hacking-gurus.net

# Please start configuring here
svnbasedir="/home/svn"
svnfullbkdir="/home/svn/backups/full" #Don't put a trailing slash!
logfile="/home/svn/backups/log.txt"
tmpdir="/tmp"

# dont change below

#check if log file exists
touch ${logfile}
#redirect stdout and stderr to the logfile
mkdir -p $(dirname ${logfile})
exec >> ${logfile}
exec 2>&1

echo "+++ Backing up subversion repositories"
#Looks for dirs containing a file "format", that's not in the /db/ directory.
#This seems to match the root of a SVN repo. YMMV
repos=$(find $svnbasedir -name format  -a \! -path '*/db/*'  -printf '%h\n')

#Make sure that our backup location exists
mkdir -p ${svnfullbkdir}

for repo in $repos ; do
	subpath=$(echo ${repo} | sed -e "s#${svnbasedir}/##")
	name=$(basename $subpath)
	subpath=$(dirname $subpath)
	echo "   $repo"
	lastrev=$(svnlook youngest ${repo})
	
	mkdir -p ${svnfullbkdir}/${subpath}
	svnadmin dump -q ${repo} > ${svnfullbkdir}/${name}.dump
	echo  "Dumping ${repo} to  ${name}.dump  \n" 

	
done
