#!/bin/bash

REPOS_DIR="/opt/svn/repositories/genrepsoft";
LOCAL_SVN_URL="http://svnmirror/genrepsoft";
SYNC_USER="svnsync";
SYNC_PASS="maverick@2010u";
SVNSYNC=`which svnsync`;
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`

sync_repos()
{
echo "Starting synchronization of repositories";
for repo in $LOCAL_REPOS
do
	echo "Synchronizing repository $repo";
	$SVNSYNC synchronize $LOCAL_SVN_URL/$repo --source-username $SYNC_USER --source-password $SYNC_PASS
	echo "Finished sinchronization of repository $repo";
done
echo "Finished synchronization of repositories";
}

sync_repos

