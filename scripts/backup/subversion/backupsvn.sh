﻿#!/bin/bash
SVN_REPOSITORY="/var/svn";
TMP_DIR="/tmp";
NOW=`date +%Y%m%d-%H%M`;
BACKUP_DIR="/backup/backups";
BACKUP_FILE="my_backup-.$NOW";
GZIPED_FILE="$BACKUP_DIR/$BACKUP_FILE.gz";
SVN=`which svn`;
SVNLOOK=`which svnlook`;
SVNADMIN=`which svnadmin`;
YOUNGEST_REV=`$SVNLOOK youngest $SVN_REPOSITORY`;
DUMP_COMMAND="$SVNADMIN -q dump $SVN_REPOSITORY > $BACKUP_DIR/$BACKUP_FILE";
BACKUP_SVR_USER="backup";
BACKUP_SVR="backupserver";

echo -e "\nDumping Subversion repo $SVN_REPOSITORY to $BACKUP_FILE...\n";
echo -e `$DUMP_COMMAND`;
echo -e "Backing up through revision $YOUNGEST_REV... \n";
echo -e "\nCompressing dump file...\n";
echo -e `gzip -9 $BACKUP_DIR/$BACKUP_FILE`;
echo -e "\nCreated $GZIPED_FILE\n";
echo -e `scp $GZIPED_FILE $BACKUP_SVR_USER@$BACKUP_SVR:/backup/`;
echo -e "\n$BACKUP_FILE.gz transfered to $BACKUP_SVR\n";

#Test Backup
echo -e "\n---------------------------------------\n";
echo -e "Testing Backup";
echo -e "\n---------------------------------------\n";
echo -e "Downloading $BACKUP_FILE.gz from $BACKUP_SVR\n";
echo -e `scp $BACKUP_SVR_USER@$BACKUP_SVR:/backup/$BACKUP_FILE.gz $TMP_DIR/`;
echo -e "Unzipping $BACKUP_FILE.gz\n";
echo -e `gunzip $TMP_DIR/$BACKUP_FILE.gz`;
echo -e "Creating test repository\n";
echo -e `$SVNADMIN create $TMP_DIR/test_repo`;
echo -e "Loading repository\n";
echo -e `$SVNADMIN -q load $TMP_DIR/test_repo < $TMP_DIR/$BACKUP_FILE`;
echo -e "Checking out repository\n";
echo -e `$SVN -q co file://$TMP_DIR/test_repo $TMP_DIR/test_checkout`;
echo -e "Cleaning up\n";
echo -e `rm -f $TMP_DIR/$BACKUP_FILE`;
echo -e `rm -rf $TMP_DIR/test_checkout`;
echo -e `rm -rf $TMP_DIR/test_repo`;