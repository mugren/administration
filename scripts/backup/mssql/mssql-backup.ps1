#
# MS SQL Backup Script # PowerShell
# Author: Igor @genrepsoft
# 

# Import-Module first. We need the cmdlets
Import-Module sqlps

# Server Params
$dbinstance = "GIS-PORTAL\DB_APP"

# Databases for backup
$databases = "APP_DB","APP_BR"

# Credentials
#
# How we handle authentication. This is done before executing script:
# read-host -assecurestring | convertfrom-securestring | out-file C:\inetpub\Backup\Scripts\securepassword.txt
# Another option:
# $password = ConvertTo-SecureString "password" -AsPlainText -Force
#
$username = "app_db_user"
$password = cat C:\inetpub\Backup\Scripts\securepassword.txt | convertto-securestring
$cred = new-object -typename System.Management.Automation.PSCredential `
         -argumentlist $username, $password
# No need of Get-Credential this way.

# Destination Params
$dest_dir = "C:\inetpub\Backup\SQL"
$date = Get-Date -Format yyyy-MM-dd
$date_yyyyMM = Get-Date -Format yyyy-MM
$date_dd = Get-Date -Format dd
$full_dest = $dest_dir + "\" + $date_yyyyMM + "\" + $date_dd
$Log = "C:\inetpub\Backup\Logs\backup-$date.txt"

Function doBackup()
{
try{
if(!(Test-Path $full_dest))
{
	echo "Path doesn't exist. Creating one."
	New-Item -ItemType directory -Path $full_dest
	
	foreach($db in $databases)
	{
		Backup-SqlDatabase -ServerInstance $dbinstance -Database $db -Credential $cred -BackupFile "$full_dest\$db.bak"
	}
	
	echo "Backup is complete. Have a nice day."
}
else 
{
    echo "Backup exists. Bye."
}
}
catch [Exception]
{
	return $_.Exception.Message
	# write-host
}
}
doBackup | out-file $Log

