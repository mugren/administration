#
#  Remote Database Backup # PowerShell
#  Author: Igor @genrepsoft
# 
# Variables
$dbserver = "IIS-AP"
$location = "D:\BACKUP\SQL\"
#$user = "iis.tarifi"
#$pwd = "Tar123$"
#$dbName = "TarifiBaza2"
$user = "br"
$pwd = "Nomis11s"
$dbName = "CRMDB"

$date = Get-Date -Format yyyy-MM-dd
$timestamp=((get-date).toString("yyyy_MM_dd_hh_mm"))
$dir = $location + $date
$file = $location + $date + "\" + $dbName + "-" + $timestamp + ".bak"

[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.Smo") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.SmoExtended") | Out-Null
[System.Reflection.Assembly]::LoadWithPartialName("Microsoft.SqlServer.ConnectionInfo") | Out-Null

$connection = New-Object ('Microsoft.SqlServer.Management.Common.ServerConnection') 
$connection.ServerInstance =$dbserver
$connection.LoginSecure = $false
$connection.Login = $user 
$connection.Password = $pwd

$server = New-Object ('Microsoft.SqlServer.Management.Smo.Server') $connection
echo $server.Informaion
$server.ConnectionContext.StatementTimeout = 0

$db = New-Object Microsoft.SqlServer.Management.Smo.Database
$db = $server.Databases.Item($dbName)

$backup = New-Object "Microsoft.SqlServer.Management.Smo.Backup"
$backup.Action = [Microsoft.SqlServer.Management.Smo.BackupActionType]::Database
$backup.Incremental = $FALSE

$device = New-Object ('Microsoft.SqlServer.Management.Smo.BackupDeviceItem') ($file, [Microsoft.SqlServer.Management.Smo.DeviceType]::File)
#$device.DeviceType = 'File'
#$device.Name = $file

$backup.Devices.Add($device)
$backup.MediaDescription = "Disk"
$backup.Database= $dbName
try{
if(!(Test-Path $dir))
{
	echo "Path doesn't exist. Creating one."
	New-Item -ItemType directory -Path $dir
}
    $backup.SqlBackup($server)
}
catch [Exception]
{
	write-host $_.Exception
}