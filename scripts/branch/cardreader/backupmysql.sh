#!/bin/bash -e

# Get the schedule(weekly or monthly) from the first input parameter
SCHEDULE=$1;

# Get the project name from the second input parameter
PROJECT=$2;

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

#* Backup partition *#
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD/$PROJECT";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# List of databases to backup
DBS=`cat $BACKUPDIRBIN/$SCHEDULE/$PROJECT/dbs`;

### Server Setup ###
#* MySQL login username *#
MUSER="root";

#* MySQL login PASSWORD *#
MPASS="root";

#* MySQL login HOST  name*#
MHOST="localhost";

#* MySQL login PORT number *#
MPORT="3306";

#* MySQL dump binary *#
MYSQLDUMP=`which mysqldump`;

#* gzip binaries *#
GZIP=`which gzip`;

# start to dump database one by one
if [ "X$DBS" != "X" ]; then
	for db in $DBS
	do
		FILE="$BACKUPDIRDAILY/mysql-$db.gz";
		echo "BACKING UP $db";
		$MYSQLDUMP --add-drop-database --opt --lock-all-tables -u $MUSER -p$MPASS -h $MHOST -P $MPORT $db | gzip > $FILE
	done
else
	echo "There are not any MySQL databases to backup for project $PROJECT...";
fi
