#!/bin/bash
### MySQL Server Login Info ###
MUSER="root"
MPASS="root"
MHOST="localhost"
MYSQL="$(which mysql)"
MYSQLDUMP="$(which mysqldump)"
BAK="/media/storage/cardreaderBACKUPS"
BAK2="/home/cardreader/cardreaderBACKUPS"
GZIP="$(which gzip)"
NOW=$(date +"%d-%m-%Y")
 
### See comments below ###
### [ ! -d $BAK ] && mkdir -p $BAK || /bin/rm -f $BAK/* ###
[ ! -d "$BAK" ] && mkdir -p "$BAK"
[ ! -d "$BAK2" ] && mkdir -p "$BAK2"
 
 FILE=$BAK/CARDREADER.$NOW-$(date +"%T").gz
 FILE2=$BAK2/CARDREADER.$NOW-$(date +"%T").gz
 $MYSQLDUMP -u $MUSER -h $MHOST -p$MPASS CARDREADER | $GZIP -9 > $FILE
 $MYSQLDUMP -u $MUSER -h $MHOST -p$MPASS CARDREADER | $GZIP -9 > $FILE2