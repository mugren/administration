#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/backup/logs";

# What is the name of the log
LOG="$LOG_DIR/backup-$DATE.log";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# Backup script name
BACKUPBIN="backup.sh";

# DB2 Instance name
INSTANCE=$1

# DB2 Instance Database
DATABASE=$2

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN $INSTANCE $DATABASE > $LOG
