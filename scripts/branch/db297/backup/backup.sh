#!/bin/bash

# DB2 Instance name
INSTANCE=$1

# DB2 Instance Database
DATABASE=$2

# Backup dir
BACKUPDIR="/mnt/backup";

# Database backup dir
BACKUPDIRDB="$BACKUPDIR/$INSTANCE/$DATABASE";

# Database export dir
EXPORTDIR="/home/$INSTANCE/backup/$DATABASE";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# Backup script name
BACKUPBIN="exportdb.sh";

# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDB ]; then
		mkdir -p $BACKUPDIRDB;
	else
		echo "$BACKUPDIRDB allready exists!!!";
	fi
	
	#  We start backup of database
	# First we export the database
	su - $INSTANCE -c "bash -c '$BACKUPDIRBIN/$BACKUPBIN $INSTANCE $DATABASE'";
	# Then we find the backup file name of the exported database
	BACKUPFILE=`find $EXPORTDIR -maxdepth 1 -type f -mtime 0`;
	# We copy backup file to the nfs mount
	echo "Starting to copy the backup file: $BACKUPFILE";
	cp $BACKUPFILE $BACKUPDIRDB;
	# At the end we remove the backup file on local disk
	echo "Copying finished, removing the backup file: $BACKUPFILE";
	rm -rf $BACKUPFILE;
}

check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi
