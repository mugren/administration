#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/backup/logs";

# What is the name of the log
LOG="$LOG_DIR/backup-$DATE.log";

# Statistics script dir
BACKUPDIRBIN="/opt/backup/bin";

# Backup script name
BACKUPBIN="backupdomino.sh";

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN > $LOG
