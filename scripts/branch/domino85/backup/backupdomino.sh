#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Backup dir daily
BACKUPDIRDAILY="/mnt/backup/$DATEYYMM/$DATEDD";

# Domino Data DIR
NOTESDATA="/local/notesdata";

# All database files
NSFFILES=`ls $NOTESDATA/*.nsf`;

# All template database files
NTFFILES=`ls $NOTESDATA/*.ntf`;

# All id files
IDS="
admin.id
server.id
cert.id
";

# Domino Server configuration file
NOTESINI="$NOTESDATA/notes.ini";

# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY/{NSF,NTF,IDS,INI};
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	#  We start the backup
	# First we need to stop the server
	service rc_domino stop

	# We compress nsf files
	for nsf in $NSFFILES
	do
		echo "Compressing file: $nsf";
		tar -jcvPf $BACKUPDIRDAILY/NSF/`basename $nsf`.tar.bz2 $nsf;
	done;

	# We compress ntf files
	for ntf in $NTFFILES
	do
		echo "Compressing file: $ntf";
		tar -jcvPf $BACKUPDIRDAILY/NTF/`basename $ntf`.tar.bz2 $ntf;
	done;

	# We compress id's
	for id in $IDS
	do
		echo "Compressing file: $id";
		tar -jcvPf $BACKUPDIRDAILY/IDS/$id.tar.bz2 $NOTESDATA/$id;
	done;

	# We compress notes.ini
	echo "Compressing $NOTESINI";
	tar -jcvPf $BACKUPDIRDAILY/INI/`basename $NOTESINI`.tar.bz2 $NOTESINI;

	# On the end we start the server again
	service rc_domino start
}

check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi
