#!/bin/bash

# Repositories directory
REPOS_DIR="/opt/svn/repositories/genrepsoft";

# List of names of local repositories
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`;

START_DATE="2012-05-01"
END_DATE="2012-10-01"

SVN=`which svn`
AWK=`which awk`

USER=simon
DOMAIN="http://svn.genrepsoft.net/genrepsoft/"

#$SVN log "${DOMAIN}labs" --username igor --password xxx -r{$START_DATE}:{$END_DATE}  | $AWK '$3 == '\"$JAS\"' {print $1}'

#exit 0

for repo in $LOCAL_REPOS
        do
		echo "REPO: $repo URL: $DOMAIN$repo"
		for i in `$SVN log $DOMAIN$repo --username igor --password xxx -r{$START_DATE}:{$END_DATE} | $AWK '$3 == '\"${USER}\"' {print $1}'`;
			 do 
				echo "ENTER"
				echo $i;
				$SVN log $DOMAIN$repo --username igor --password xxx -v -$i;
			 done
#		$SVN log -v $DOMAIN$repo --username igor --password xxx -r{$START_DATE}:{$END_DATE} 
        done

