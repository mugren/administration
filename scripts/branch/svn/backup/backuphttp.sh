#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Hostname
HOSTNAME=`hostname`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$HOSTNAME/conf/$DATEYYMM/$DATEDD";

# Apache HTTP DIR
APACHE_DIR="etc/apache2";

# Apache HTTP & Subversion Authentication & Authorization DIR for genrepsoft repositories
GENREP_AUTH_DIR="opt/svn/repositories/genrepsoft/auth";

# Repositories directory
REPOS_DIR="/opt/svn/repositories/genrepsoft";

# List of names of local repositories
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`;

# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY;
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	# We compress apache2 directory
	echo "Compressing /$APACHE_DIR"
	tar -jcPf $BACKUPDIRDAILY/`basename /$APACHE_DIR`.tar.bz2 -C / $APACHE_DIR;

	# We compress genrepsoft auth directory
	echo "Compressing /$GENREP_AUTH_DIR"
	tar -jcPf $BACKUPDIRDAILY/`basename /$GENREP_AUTH_DIR`.tar.bz2 -C / $GENREP_AUTH_DIR;
	
	# We compress the hooks of the repositories one by one
	for repo in $LOCAL_REPOS
	do
		echo "Compressing hooks for repository $repo"
		tar -jcPf $BACKUPDIRDAILY/$repo-hooks.tar.bz2 --exclude='*.tmpl' $REPOS_DIR/$repo/hooks;
	done
}

check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi
