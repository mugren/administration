#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/opt/backup/bin/backup.sh > /opt/backup/logs/$DATE.log

SUBJECT="Backup Cron Daemon www.thinktogether.com.mk"
cat /opt/backup/logs/$DATE.log | mail -s "$SUBJECT" vpesov@genrepsoft.com simoncev@gmail.com aleksandar.silovski@gmail.com eleonorap@gmail.com
