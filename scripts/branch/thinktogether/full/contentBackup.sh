#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup";

# Backup dir
BACKUPDIRDAILY="$BACKUPDIR/full/$DATEYYMM/$DATEDD";

# Location of Wordpress Application
WORDPRESS_APP=opt/wordpress/wordpress

# Location of QAEducation Application
QAEDU_APP=opt/qaedu/QAEducation

# Location of Quercus Application
QUERCUS_APP=opt/glassfish3/glassfish/domains/domain1/applications/help-desk-quercus

# Location of JSPWiki Data
JSPWIKI_DATA=opt/jspwikidata

# Location of JSPWiki Application
JSPWIKI_APP=opt/apache-tomcat-5.5.33/webapps/FOSSWiki

# List of Content to Backup:
IDS="
$WORDPRESS_APP
$QAEDU_APP
$QUERCUS_APP
$JSPWIKI_DATA
$JSPWIKI_APP
"

# We start to compress applications one by one
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done

# Unmount backup partiotion when the backup is done
echo "Unmounting $BACKUPDIR ...";
umount $BACKUPDIR;

