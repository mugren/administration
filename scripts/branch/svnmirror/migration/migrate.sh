#!/bin/bash

REPOS_DIR="/opt/svn/repositories/genrepsoft";
REMOTE_REPOS=`ssh svn ls $REPOS_DIR | grep -v auth`
HOOK_SCRIPTS_DIR="/opt/migration/scripts/hooks";
LOCAL_SVN_URL="http://svnmirror/genrepsoft";
REMOTE_SVN_URL="http://svn/genrepsoft";
SYNC_USER="svnsync";
SYNC_PASS="maverick@2010u";
SVNADMIN=`which svnadmin`;
SVNSYNC=`which svnsync`;

create_repositories()
{
echo "Starting creation of repositories";
for repo in $REMOTE_REPOS
do
	echo "Creating repository $repo"
	$SVNADMIN create $REPOS_DIR/$repo
	chown -R www-data. $REPOS_DIR/$repo
	echo "Finished creation of repository $repo"
done
echo "Finished creation of repositories";
}

copy_hooks()
{
echo "Starting copying hooks scripts";
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`
for repo in $LOCAL_REPOS
do
        echo "Copying hooks to location: $REPOS_DIR/$repo/hooks/"
        cp -a $HOOK_SCRIPTS_DIR/* $REPOS_DIR/$repo/hooks/
        echo "Finished copying"
done
echo "Finished copying hooks scripts";
}

init_local_repos()
{
echo "Starting initialization of local repositories";
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`
for repo in $LOCAL_REPOS
do
	echo "Starting to initialize local repository $repo";
	$SVNSYNC initialize $LOCAL_SVN_URL/$repo $REMOTE_SVN_URL/$repo --sync-username $SYNC_USER --sync-password $SYNC_PASS
	echo "Finished initialization of local repository $repo";
done
echo "Finished initialization of local repositories";
}

sync_repos()
{
echo "Starting synchronization of repositories";
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`
for repo in $LOCAL_REPOS
do
	echo "Synchronizing repository $repo";
	$SVNSYNC synchronize $LOCAL_SVN_URL/$repo --source-username $SYNC_USER --source-password $SYNC_PASS
	echo "Finished sinchronization of repository $repo";
done
echo "Finished synchronization of repositories";
}

sync_uuids()
{
echo "Starting synchronization of uuids";
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`
for repo in $LOCAL_REPOS
do
        echo "Synchronizing uuid for repository $repo";
	REMOTE_UUID=`ssh svn svnlook uuid $REPOS_DIR/$repo`
	$SVNADMIN setuuid $REPOS_DIR/$repo $REMOTE_UUID
        echo "Finished sinchronization of uuid for repository $repo";
done
echo "Finished synchronization of uuids";
}

create_repositories
copy_hooks
init_local_repos
sync_repos
sync_uuids
