#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Hostname
HOSTNAME=`hostname`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$HOSTNAME/dumps/$DATEYYMM/$DATEDD";

# Repositories directory
REPOS_DIR="/opt/svn/repositories/genrepsoft";

# List of names of local repositories
LOCAL_REPOS=`ls $REPOS_DIR | grep -v auth`

# Full path of svn-backup-dumps script
SVN_BACKUP_DUMPS=`which svn-backup-dumps`;

# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY;
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	# We dump the repositories one by one
	for repo in $LOCAL_REPOS
	do
		echo "Dumping repository $repo"
		$SVN_BACKUP_DUMPS -b $REPOS_DIR/$repo $BACKUPDIRDAILY;
		echo "Finished dumping repository $repo"
	done
}

check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi



