#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/backup/logs";

# What is the name of the log
LOG="$LOG_DIR/dump-$DATE.log";

# Statistics script dir
BACKUPDIRBIN="/opt/backup/bin/dump";

# Backup script name
BACKUPBIN="backup_dump.sh";

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN > $LOG
