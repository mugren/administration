#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Hostname
HOSTNAME=`hostname`;

# Backup dir
BACKUPDIR="/storage/nfs_backups/nagios";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$HOSTNAME/$DATEYYMM/$DATEDD";

# NRPE Configuration File
NRPE_LOCAL="etc/nagios/nrpe_local.cfg";

# Sudoers Configuration File
SUDOERS="etc/sudoers";

# Nagios check_md_raid plugin
CHECK_MD_RAID="usr/lib/nagios/plugins/check_md_raid";

# All files and directories to backup
CONTENTS="
$NRPE_LOCAL
$SUDOERS
$CHECK_MD_RAID
";

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY;
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	# We compress contents
	for content in $CONTENTS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done
}

echo "Making backup...";
backup;
echo "Backup finished...";
