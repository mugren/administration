﻿#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# Content backup script name
BACKUPCONTENTBIN="contentBackup.sh";

# Mysql backup script name
BACKUPMYSQLBIN="backupmysql.sh";

# Get the schedule(weekly or monthly) from the first input parameter
SCHEDULE=$1;

# List of services
PROJECTS=`ls $BACKUPDIRBIN/$SCHEDULE`;

# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	for project in $PROJECTS
	do
		# assuming that NFS share is mounted via /etc/fstab
		if [ ! -d $BACKUPDIRDAILY/$project ]; then
			mkdir -p $BACKUPDIRDAILY/$project;
		else
			echo "$BACKUPDIRDAILY/$project allready exists!!!";
		fi

		# Stop the services which we want to backup
		# List of services
		SERVICES=`cat $BACKUPDIRBIN/$SCHEDULE/$project/services`;
		echo "Stopping services..."
		for serv in $SERVICES
		do
			echo "Stopping Service $serv...";
			service $serv stop;
		done
		# We backup mysql databases
		echo "Backing up mysql databases..."
		$BACKUPDIRBIN/$BACKUPMYSQLBIN $SCHEDULE $project
		# We compress contents
		echo "Backing up Content..."
		$BACKUPDIRBIN/$BACKUPCONTENTBIN $SCHEDULE $project
		# Start the services
		echo "Starting services..."
		for serv in $SERVICES
		do
			echo "Starting Service $serv...";
			service $serv start;
		done
	done
}

echo "********************************************************************"
echo "********************************************************************"
echo "Report from `hostname -f` backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi
echo "********************************************************************"
echo "********************************************************************"
