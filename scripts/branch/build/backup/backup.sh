#!/bin/bash

# Backup script dir
BACKUPDIRBIN=/opt/backup/bin

# Backup script name
BACKUPCONTENTBIN=contentBackup.sh

echo "********************************************************************"
echo "********************************************************************"
echo "Report from build.genrepsoft.inc backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping services..."
echo "Stopping Nexus..."
/etc/init.d/nexus stop
echo "Stopping Hudson..."
/etc/init.d/hudson stop
echo "Backing up Content..."
$BACKUPDIRBIN/$BACKUPCONTENTBIN
echo "Starting services..."
echo "Starting Nexus..."
/etc/init.d/nexus start
echo "Starting Hudson..."
/etc/init.d/hudson start
echo "********************************************************************"
echo "********************************************************************"
