#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD";

# Nexus Web App
NEXUS_HOME="opt/nexus/nexus";

# Nexus Work Directory
SONATYPE_WORK="opt/nexus/sonatype-work";

# Hudson Home Directory
HUDSON_HOME="opt/hudson/hudson-home";

# Hudson Defaults File
HUDSON_DEFAULT="etc/default/hudson";

# Hudson Logrotate File
HUDSON_LOGROTATE="etc/logrotate.d/hudsonlog";

# Wordpress portal home directory
PORTAL_HOME="opt/tomcat";

# All files and directories to backup
CONTENTS="
$NEXUS_HOME
$SONATYPE_WORK
$HUDSON_HOME
$HUDSON_DEFAULT
$HUDSON_LOGROTATE
$PORTAL_HOME
";
# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY;
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	# We compress contents
	for content in $CONTENTS
	do
		echo "Compressing /$content"
		if [ -h "/$content" ]; then 
		real_path=`readlink -f "/$content"`
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $real_path
	        fi
		else
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
		fi
	done
}

# Backup MySQL database(s)
backup_mysql(){
	### Server Setup ###
	#* MySQL login username *#
	MUSER="root";

	#* MySQL login PASSWORD *#
	MPASS="meerkat%2010j";

	#* MySQL login HOST  name*#
	MHOST="localhost";

	#* MySQL login PORT number *#
	MPORT="3306";

	#* MySQL dump binary *#
	MYSQLDUMP=`which mysqldump`;

	#* gzip binaries *#
	GZIP=`which gzip`;

	DB="wp_genrepsoft";

	FILE="$BACKUPDIRDAILY/mysql-$DB.gz";
	echo "BACKING UP $DB;";
	$MYSQLDUMP --add-drop-database --opt --lock-all-tables -u $MUSER -p$MPASS -h $MHOST -P $MPORT $DB | gzip > $FILE ;
}

#check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	backup_mysql;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi

