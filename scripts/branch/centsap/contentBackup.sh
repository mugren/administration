#!/bin/bash

#Get the schedule(weekly or monthly) from the first input parameter
SCHEDULE=$1;

# Get the project name from the second input parameter
PROJECT=$2;

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

#* Backup partition *#
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD/$PROJECT";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# List of databases to backup
CONTENTS=`cat $BACKUPDIRBIN/$SCHEDULE/$PROJECT/contents`;

# We start to compress applications one by one
if [ "X$CONTENTS" != "X" ]; then
	for content in $CONTENTS
	do
		echo "Compressing $content"
		tar -jcPf $BACKUPDIRDAILY/`basename $content`.tar.bz2 $content;
	done
else
	echo "There are not any files or directories to backup for project $PROJECT...";
fi

