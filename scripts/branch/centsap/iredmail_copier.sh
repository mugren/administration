#!/bin/bash

#Now in format: YYYY
DATEYY=`date +%Y`;

#Now in format: MM
DATEMM=`date +%m`;
	
# Now in format: DD
DATEDD=`date +%d`;

# Source dir
SOURCEDIR="/storage/backup/iredmail";

# Backup dir
BACKUPDIR="/mnt/backup/iredmail";

# Mount dir
MOUNTDIR="/mnt/backup";

# Source to copy on NAS
SOURCEDIRDAILY="$SOURCEDIR/$DATEYY/$DATEMM/$DATEDD";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYY/$DATEMM/$DATEDD";

# Backup script dir
BACKUPDIRBIN="/opt/backup/bin";

# Initialize the status
STATUS=0;
DIR=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $MOUNTDIR)" ]; then
		echo "$MOUNTDIR is allready mounted";
	else
		echo "Mounting $MOUNTDIR ...";
		mount $MOUNTDIR;
		if [ $? -eq 0 ]; then
			echo "$MOUNTDIR successfully mounted";
		else
			echo "Failed to mount $MOUNTDIR";
			STATUS=1;
		fi
	fi
}
check_directory(){
	if [ -d "$BACKUPDIRDAILY" ]; then
		DIR=1;
		echo "Backup dir exits!";
	else
  		mkdir -p $BACKUPDIRDAILY;
	fi
}
# Do the backup
backup(){
# Copy the files from source to backup dir
cp -r $SOURCEDIRDAILY/* $BACKUPDIRDAILY
}
echo "********************************************************************"
echo "********************************************************************"
echo "Report from `hostname -f` backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	check_directory;
	backup;
	echo "Backup finished";
else
	echo "Not doing backup, exiting...";
fi
echo "********************************************************************"
echo "********************************************************************"

