#!/usr/bin/env bash

# Author:   Igor Ivanovski (igor@genrepsoft.com)
# Date:     2013-10-10
# Modify below variables to fit your need ----
#########################################################

# Where to store backup copies.
export BACKUP_ROOTDIR="/storage/backup"

# Configuration we should backup
export CONF_DIRS=" /opt/iredapd /etc/httpd /etc/amavisd /etc/dovecot /etc/postfix"

# Compress plain SQL file: YES, NO.
export COMPRESS="YES"

# Delete plain SQL files after compressed. Compressed copy will be remained.
export DELETE_PLAIN_SQL_FILE="YES"

#########################################################
# You do *NOT* need to modify below lines.
#########################################################
export PATH='/usr/bin:/bin:/usr/sbin:/sbin:/usr/local/bin:/usr/local/sbin'

# Commands.
export CMD_DATE='/bin/date'
export CMD_DU='du -sh'
export CMD_COMPRESS='bzip2 -9'
export CMD_COMPRESS_DIR='gzip -r'

# Date.
export YEAR="$(${CMD_DATE} +%Y)"
export MONTH="$(${CMD_DATE} +%m)"
export DAY="$(${CMD_DATE} +%d)"
export TIME="$(${CMD_DATE} +%H.%M.%S)"
export TIMESTAMP="${YEAR}.${MONTH}.${DAY}.${TIME}"

# Pre-defined backup status
export BACKUP_SUCCESS='YES'

# Define, check, create directories.
export BACKUP_DIR="${BACKUP_ROOTDIR}/iredmail/${YEAR}/${MONTH}/${DAY}"

# Log file
export LOGFILE="${BACKUP_DIR}/${TIMESTAMP}.log"

# Check and create directories.
[ ! -d ${BACKUP_DIR} ] && mkdir -p ${BACKUP_DIR} 2>/dev/null


# Initialize log file.
echo "* Starting at: ${YEAR}-${MONTH}-${DAY}-${TIME}." >${LOGFILE}
echo "* Backup directory is: ${BACKUP_DIR}." >>${LOGFILE}

# Backup.

cd ${BACKUP_DIR}
echo "* Compressing of installation directories begins..." >> ${LOGFILE}
for dr in ${CONF_DIRS}; do
	
	if [ -d ${dr} ]; then
	output_zip=`basename ${dr}`"D-${TIMESTAMP}.tar.bz2"
		tar -jcPf ${BACKUP_DIR}/${output_zip} $dr;
#		${CMD_COMPRESS_DIR} ${dr} -c > `pwd`/${output_zip}
        else
	output_zip=`basename ${dr}`"F-${TIMESTAMP}.bz2"
		${CMD_COMPRESS} ${dr} -c > `pwd`/${output_zip}
        fi

	if [ X"$?" == X"0" ]; then
	    echo -e "  + ${dr} [DONE]" >> ${LOGFILE}
        else
            [ X"${BACKUP_SUCCESS}" == X"YES" ] && export BACKUP_SUCCESS='NO'
        fi

done

# Append file size of backup files.
echo -e "* File size:\n----" >>${LOGFILE}
${CMD_DU} ${BACKUP_DIR} >>${LOGFILE}
echo "----" >>${LOGFILE}

echo "* Backup completed (Success? ${BACKUP_SUCCESS})." >>${LOGFILE}

if [ X"${BACKUP_SUCCESS}" == X"YES" ]; then
    echo -e "\n[OK] Backup successfully completed.\n"
else
    echo -e "\n[ERROR] Backup completed with ERRORS.\n" 1>&2
fi

echo "* Backup log: ${LOGFILE}:"
cat ${LOGFILE}
