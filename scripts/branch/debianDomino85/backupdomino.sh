#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup/$DATEYYMM/$DATEDD";

# Domino Data DIR
NOTESDATA="/local/notesdata";
cd $NOTESDATA;

# All database files
NSFFILES=`ls *.nsf`;

# All template database files
NTFFILES=`ls *.ntf`;

# Domino Server configuration file
NOTESINI="notes.ini";

# All id files
IDS="
admin.id
server.id
cert.id
";

# First we need to stop the server
/etc/init.d/domino stop

# Assuming that NAS is mounted correctly
if [ ! -d $BACKUPDIR ]; then
	mkdir -p $BACKUPDIR
	mkdir -p $BACKUPDIR/NSF
	mkdir -p $BACKUPDIR/NTF
	mkdir -p $BACKUPDIR/IDS
	mkdir -p $BACKUPDIR/INI
fi

# We compress nsf files
for nsf in $NSFFILES
	do
		echo "Compressing file: $nsf";
		tar -jcf $BACKUPDIR/NSF/$nsf.bz2 $nsf;
	done;

# We compress ntf files
for ntf in $NTFFILES
	do
		echo "Compressing file: $ntf";
		tar -jcf $BACKUPDIR/NTF/$ntf.bz2 $ntf;
	done;

# We compress id's
for id in $IDS
	do
		echo "Compressing file: $id";
		tar -jcf $BACKUPDIR/IDS/$id.bz2 $id;
	done;

# We compress notes.ini
echo "Compressing $NOTESINI";
tar -jcf $BACKUPDIR/INI/$NOTESINI.bz2 $NOTESINI;

# On the end we start the server again
/etc/init.d/domino start
