//@Grab('mysql:mysql-connector-java:5.1.27')
//@GrabConfig(systemClassLoader=true)
//@Grab(group='mysql', module='mysql-connector-java', version='5.1.27')
import groovy.time.TimeCategory

import java.sql.Timestamp


/**
 * Created by igor on 8/22/14.
 */
this.getClass().classLoader.rootLoader.addURL(new File("mysql-connector-java-5.1.30-bin.jar").toURL())
//def printClassPath(classLoader) {
//    println "$classLoader"
//    classLoader.getURLs().each {url->
//        println "- ${url.toString()}"
//    }
//    if (classLoader.parent) {
//        printClassPath(classLoader.parent)
//    }
//}
//printClassPath this.class.classLoader

sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.15:3306/Class_Manager?autoReconnect=true",
        "bjnexam",
        "maverick@2010u",
        "com.mysql.jdbc.Driver")

def rows= [:]

sql.eachRow("select * from TRAINING_REQUEST_IMPORTED"){
    def impDates = new ImportedDates()
    impDates.IND_BIRTH_DATE = it.IND_BIRTH_DATE
    impDates.datePassedExam = it.datePassedExam
    impDates.dateLastResertification = it.dateLastResertification
    println impDates.IND_BIRTH_DATE?.class
    println "OLD DATES"
    println impDates.IND_BIRTH_DATE.toString()+" "+impDates.datePassedExam.toString()+" "+impDates.dateLastResertification.toString()
    if(impDates.IND_BIRTH_DATE!=null){
        use(TimeCategory){
            impDates.IND_BIRTH_DATE = impDates.IND_BIRTH_DATE - 1.month
        }
    }
    if(impDates.datePassedExam!=null){
        use(TimeCategory){
            impDates.datePassedExam = impDates.datePassedExam - 1.month
        }
    }
    if(impDates.dateLastResertification!=null){
        use(TimeCategory){
            impDates.dateLastResertification = impDates.dateLastResertification - 1.month
        }
    }
    println impDates.IND_BIRTH_DATE?.class
    println "NEW DATES"
    println impDates.IND_BIRTH_DATE.toString()+" "+impDates.datePassedExam.toString()+" "+impDates.dateLastResertification.toString()
    rows.put(it.UID,impDates)

}


rows.each {row->
    ImportedDates iD = row.value
    birthDate  = null
    if(iD.IND_BIRTH_DATE) birthDate = new Timestamp(iD.IND_BIRTH_DATE.getTime())
    passedExam = null
    if(iD.datePassedExam) passedExam = new Timestamp(iD.datePassedExam.getTime())
    lastSertification = null
    if(iD.dateLastResertification) lastSertification = new Timestamp(iD.dateLastResertification.getTime())

    sql.executeUpdate('update TRAINING_REQUEST_IMPORTED set IND_BIRTH_DATE = ? ' +
            ', datePassedExam = ? ' +
            ', dateLastResertification = ? ' +
            'where UID like ?',
            [birthDate,
             passedExam,
             lastSertification,
             row.key.toString()])
}
