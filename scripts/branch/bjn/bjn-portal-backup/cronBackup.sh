#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

# Where is backup logs directory
LOG_DIR=/storage/backup/logs

# What is the name of the log
LOG=$LOG_DIR/portalBackup-$DATE.log

# Backup script dir
BACKUPDIRBIN=/storage/backup/bin/

# Backup script name
BACKUPBIN=portalBackUp.sh

# Do the backup
$BACKUPDIRBIN/$BACKUPBIN > $LOG

# SMTP username
SMTPUSER="genrepsoft@gmail.com";

# SMTP password
SMTPPASS="nomis11s";

# SMTP server
SMTPSERVER="smtp.gmail.com";

# Mail from
MAILFROM="bjn@genrepsoft.com"

# Mail to
MAILTO="igor@genrepsoft.com";

# Mail to cc
MAILCCTO=

# Mail subject
MAILSUBJECT="BJN-portal Cron Daemon";

# Full path of SendEmail executable
SENDMAIL="/usr/bin/sendEmail";

# Send the message
$SENDMAIL -f $MAILFROM -t $MAILTO -cc $MAILCCTO -u "$MAILSUBJECT" -m "`cat $LOG`" -s $SMTPSERVER -xu $SMTPUSER -xp $SMTPPASS

