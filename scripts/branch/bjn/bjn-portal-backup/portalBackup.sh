#!/bin/bash

# Backup script dir
BACKUPDIRBIN=/storage/backup/bin

# Backup script name
BACKUPCONTENTBIN=contentBackup.sh

# Mysql backup script name
BACKUPMYSQLBIN=backupmysql.sh

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep inet[^6] | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "Informations about disk usage:"
df -h
echo "Informations about memory usage:"
free -m
vmstat 1 5
echo "********************************************************************"
echo "Report from BJN Portal backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping BJNPortal ..."
/etc/init.d/tomcat-bjn-portal stop
echo "Backing up Content..."
$BACKUPDIRBIN/$BACKUPCONTENTBIN

# We backup mysql database
                echo "Backing up mysql database..."
                $BACKUPDIRBIN/$BACKUPMYSQLBIN 
                
echo "Starting BJN Portal ..."
/etc/init.d/tomcat-bjn-portal start
echo "********************************************************************"
echo "********************************************************************"


