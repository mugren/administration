#!/bin/bash -e

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

#* Backup partition *#
BACKUPDIR="/storage/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD/$PROJECT";

# Backup script dir
BACKUPDIRBIN="/storage/backup/bin";

# List of databases to backup
DB="wp_bjn-portal";

### Server Setup ###
#* MySQL login username *#
MUSER="root";

#* MySQL login PASSWORD *#
MPASS="roottoor";

#* MySQL login HOST  name*#
MHOST="localhost";

#* MySQL login PORT number *#
MPORT="3306";

#* MySQL dump binary *#
MYSQLDUMP=`which mysqldump`;

#* gzip binaries *#
GZIP=`which gzip`;

# start to dump database one by one
if [ "X$DB" != "X" ]; then
        
                FILE="$BACKUPDIRDAILY/mysql-$DB.gz";
                echo "BACKING UP $DB";
                $MYSQLDUMP --add-drop-database --opt --lock-all-tables -u $MUSER -p$MPASS -h $MHOST -P $MPORT $DB | gzip > $FILE
else
        echo "There are not any MySQL databases to backup...";
fi




