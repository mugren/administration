#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: YYYY
DATEYYYY=`date +%Y`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/storage/backup/data";

# Backup dir daily
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYYY/portal/$DATEYYMM/$DATEDD";

# Location of bjn-portal Application
BJN_PORTAL_APP=|smeni:storage/akademika1/tomcat/webapps/ROOT|

# List of Content to Backup:
CONTENTS="
$BJN_PORTAL_APP
"

# We start to compress content one by one
for content in $CONTENTS
        do
                echo "Compressing /$content"
                tar -jcPf $BACKUPDIRDAILY/`basename $content`.tar.bz2 -C / /$content;
        done

