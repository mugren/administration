#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/backup/bin/logs";

# What is the name of the log
LOG="$LOG_DIR/backup-$DATE.log";

# Statistics script dir
BACKUPDIRBIN="/opt/backup/bin";

# Backup script name
BACKUPBIN="backup.sh";

# Get the schedule(weekly or monthly) from the first input parameter
SCHEDULE=$1;

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN $SCHEDULE > $LOG

# SMTP username
SMTPUSER="genrepsoft@gmail.com";

# SMTP password
SMTPPASS="nomis11s";

# SMTP server
SMTPSERVER="smtp.gmail.com";

# Mail from
MAILFROM="bjn@genrepsoft.com"

# Mail to
MAILTO="igor@genrepsoft.com";

# Mail to cc
MAILCCTO=

# Mail subject
MAILSUBJECT="BJN-portal Cron Daemon";

# Full path of SendEmail executable
SENDMAIL="/usr/bin/sendEmail";

# Send the message
$SENDMAIL -f $MAILFROM -t $MAILTO -cc $MAILCCTO -u "$MAILSUBJECT" -m "`cat $LOG`" -s $SMTPSERVER -xu $SMTPUSER -xp $SMTPPASS
