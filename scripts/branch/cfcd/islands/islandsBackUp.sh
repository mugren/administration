#!/bin/bash
SUBJECT="CFCD Cron Daemon"
export SUBJECT
echo "********************************************************************">>MESSAGE
echo "********************************************************************">>MESSAGE
echo "The name of this script is $0 , and it's location is:">>MESSAGE
pwd>>MESSAGE
echo "On the host:">>MESSAGE
hostname>>MESSAGE
echo "With IP Address:">>MESSAGE
cat /etc/network/interfaces | grep address | awk '{print $2}'>>MESSAGE
echo "With uptime:">>MESSAGE
uptime>>MESSAGE
echo "********************************************************************">>MESSAGE
echo "Report from Islands backup on: `date`">>MESSAGE
echo "********************************************************************">>MESSAGE
echo "********************************************************************">>MESSAGE
tar -jcvf /opt/backup/islands/webapp/webapp-`date +%F`.tar.bz2 /opt/tomcat6014/webapps/ROOT >> MESSAGE
tar -jcvf /opt/backup/islands/data/data-`date +%F`.tar.bz2 /opt/islands >> MESSAGE
echo "********************************************************************">>MESSAGE
echo "********************************************************************">>MESSAGE
#cat MESSAGE | mail -s "$SUBJECT" vpesov@genrepsoft.com simoncev@gmail.com vasko.gjurovski@gmail.com
rm -rf MESSAGE

