#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/opt/backup/bin/backup.sh > /opt/backup/logs/$DATE.log 2> /opt/backup/logs/$DATE.err

