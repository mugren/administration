#!/bin/bash -e

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/backup";

# Backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD";

### Server Setup ###
#* MySQL login user name *#
MUSER="root";

#* MySQL login PASSWORD name *#
MPASS="cfcdw3R00T";

#* MySQL login HOST name *#
MHOST="localhost";
MPORT="3306";

# DO NOT BACKUP these databases
IGNOREDB="
information_schema
performance_schema
mysql
test
"

#* MySQL binaries *#
MYSQL=`which mysql`;
MYSQLDUMP=`which mysqldump`;
GZIP=`which gzip`;

# If the backup directory doesn't exists
if [ ! -d $BACKUPDIRDAILY ]; then
    mkdir -p $BACKUPDIRDAILY
else
    echo "$BACKUPDIR allready exists!!!";
    exit 1;
fi

# get all database listing
DBS="$(mysql -u $MUSER -p$MPASS -h $MHOST -P $MPORT -Bse 'show databases')"

# start to dump database one by one
echo "Dumping the MySQL databases";
for db in $DBS
do
        DUMP="yes";
        if [ "$IGNOREDB" != "" ]; then
                for i in $IGNOREDB # Store all value of $IGNOREDB ON i
                do
                        if [ "$db" == "$i" ]; then # If result of $DBS(db) is equal to $IGNOREDB(i) then
                                DUMP="NO";         # SET value of DUMP to "no"
                                echo "$i database is being ignored!";
                        fi
                done
        fi

        if [ "$DUMP" == "yes" ]; then # If value of DUMP is "yes" then backup database
                FILE="$BACKUPDIRDAILY/$db.gz";
                echo "BACKING UP $db";
                $MYSQLDUMP --add-drop-database --opt --lock-all-tables -u $MUSER -p$MPASS -h $MHOST -P $MPORT $db | gzip > $FILE
        fi

done

echo "Dumping completed";

