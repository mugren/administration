#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/backup";

# Backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD";

# Location of cfcd-wp Application
CFCD_WP_APP=opt/cfcd/wordpress

# Location of cfcd-tenders Application
CFCD_TENDERS_APP=opt/cfcd/tenders/apache-tomcat/webapps/tenders

# Location of cfcd-tenders Documents
CFCD_TENDERS_DOCS=opt/cfcd/tenders/uploadedDocuments

# List of Content to Backup:
IDS="
$CFCD_WP_APP
$CFCD_TENDERS_APP
$CFCD_TENDERS_DOCS
"

# We start to compress applications one by one
echo "Compressing folders and files";
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done

echo "All files and folders compressed";

