@echo off
set working_dir="C:\Program Files\Oracle\VirtualBox\"
set vm_dir="D:\VirtualBoxVMs\Debian 7 MORM"
rem echo %working_dir%
rem CD /C %working_dir%
pushd %working_dir%
rem cd %working_dir%
rem set dir_path=%cd%
echo "Doing snapshot..."
rem .\VBoxManage.exe snapshot "Debian 7 MORM" take "Regular backup snapshot"
echo "Finished."
echo "Copying files to storage"
for /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set today=%%c-%%a-%%b)
rem mkdir D:\BACKUP_DATA\%today%
echo D:\BACKUP_DATA\%today%
xcopy %vm_dir% D:\BACKUP_DATA\%today% /E
pause
rem VBoxManage startvm "Virtual Name"
