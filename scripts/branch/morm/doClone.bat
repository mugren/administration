@echo off
echo First stop the service and power off the virtual machine
rem net stop VBoxVmService
set vms_dir="C:\vms"
set working_dir="C:\Program Files\Oracle\VirtualBox\"
pushd %vms_dir%
.\VmServiceControl.exe -sd 0
rem Sleep for 30 seconds
ping 1.2.3.4 -n 1 -w 30000 > nul
echo Now clone the VDI of the virtual machine
pushd %working_dir%
rem Get today date
for /f "tokens=2-4 delims=/ " %%a in ('date /t') do (set today=%%c-%%a-%%b)
.\VBoxManage.exe clonehd "D:\VirtualBoxVMs\Debian 7 MORM Clone\Debian 7 MORM Clone.vdi" "D:\BACKUP_DATA\%today%\backup.vdi"
rem wait  5 sec
ping 1.2.3.4 -n 1 -w 5000 > nul
rem "Set different uuid of the clone"
rem .\VBoxManage.exe internalcommands sethduuid D:\ExportVMs\BACKUP_DATA\2013-08-30\backup.vdi
rem "Copy the .vbox file"
echo Copy the configuration file of the virtual machine
copy "D:\VirtualBoxVMs\Debian 7 MORM Clone\Debian 7 MORM Clone.vbox" "D:\BACKUP_DATA\%today%\backup.vbox"
echo Start the machine again
rem 'Start the maching back'
pushd %vms_dir%
.\VmServiceControl.exe -su 0
echo bye
pause
rem .\VBoxManage.exe internalcommands sethduuid D:\ExportVMs\ScriptClone\cmd_clone.vdi
rem .\VBoxManage.exe export 'Debian 7 MORM' --output D:\ExportVMs\debian.ovf
rem .\VBoxManage.exe clonehd 'D:\VirtualBoxVMS\Debian 7 MORM\Debian 7 MORM.vdi' D:\ExportVMs\cmd_clone.vdi
