#! /bin/bash
# vboxcontrol   Startup script for VirtualBox Virtual Machines
#
# chkconfig: 345 99 01
# description: Manages VirtualBox VMs
# processname: vboxcontrol
#
# pidfile: /var/run/vboxcontrol/vboxcontrol.pid
#
### BEGIN INIT INFO
# Provides:          vboxcontrol
# Required-Start:    vboxdrv
# Required-Stop:     
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: VirtualBox service script
# Description:       Service script for stoping/starting virtualbox machines

### END INIT INFO
#
# Version 20130905 by igor based on:
# Version 20120317 by travisn000 based on:
# Version 20090301 by Kevin Swanson <kswan.info> based on:
# Version 2008051100 by Jochem Kossen <jochem.kossen@gmail.com>
# [url]http://farfewertoes.com[/url]
#
# Released in the public domain
#

# Author igor@genrepsoft.com

# Source function library.

#It is in /lib/lsb/init-functions

#if [ -f /etc/init.d/functions ] ; then
#. /etc/init.d/functions
#elif [ -f /etc/rc.d/init.d/functions ] ; then
#. /etc/rc.d/init.d/functions
#else
#exit 1
#fi

exec > /tmp/debug-my-script.txt 2>&1

################################################################################
# INITIAL CONFIGURATION
VBOXDIR="/etc/virtualbox"
VM_USER="genrep"
USE_NAT="no"

export PATH="${PATH:+$PATH:}/bin:/usr/bin:/usr/sbin:/sbin"

if [ -f $VBOXDIR/config ]; then
. $VBOXDIR/config
fi

SU="su $VM_USER -c"
VBOXMANAGE="VBoxManage -nologo"

################################################################################
# FUNCTIONS

# Determine if USE_NAT is set to "yes"
use_nat() {
if [ "$USE_NAT" = "yes" ]; then
return `true`
else
return `false`
fi
}

log_failure_msg() {
echo $1
}

log_action_msg() {
echo $1
}

# Check for running machines every few seconds; return when all machines are
# down
wait_for_closing_machines() {
RUNNING_MACHINES=`$SU "$VBOXMANAGE list runningvms" | wc -l`
if [ $RUNNING_MACHINES != 0 ]; then
sleep 5
echo "    ..waiting for VM shut-down to complete.."
wait_for_closing_machines
fi
}

read_argument_with_white_spaces(){
#echo $@
i=0;
vm_name='';
# Changes for names containing white spaces !!!
for p in $@; do
    if [ $i == 0 ] ; then
       i=1;
       continue;
    fi
    i=$(($i + 1))
    vm_name="${vm_name} $p"
done
vm_name=`echo "${vm_name}" | sed -e 's/^[ \s]*//' `
echo $vm_name
}

################################################################################
# RUN
case "$1" in

start)
if [ -f /etc/virtualbox/machines_enabled ]; then

cat /etc/virtualbox/machines_enabled | while read VM; do
log_action_msg "Starting VM: $VM ..."
$SU "$VBOXMANAGE startvm '${VM}' --type vrdp"
RETVAL=$?
done
#touch /var/lock/subsys/vboxcontrol
fi
;;

stop)
## NOTE: this stops all running VM's. Not just the ones listed in the config
## NOTE2: used controllvm 'savestate' instead of 'acpipowerbutton' to avoid hang 
##        with guest OS "..are you sure?" GUI prompts with acpipowerbutton
$SU "$VBOXMANAGE list runningvms" | cut -d\" -f2 | while read VM; do
log_action_msg "Saving state and powering off VM: $VM ..."
$SU "$VBOXMANAGE controlvm '$VM' savestate"
done
#rm -f /var/lock/subsys/vboxcontrol
wait_for_closing_machines

;;

start-vm)
vm_name=`read_argument_with_white_spaces $@`
log_action_msg "Starting VM: $vm_name ..."
$SU "$VBOXMANAGE startvm '$vm_name' -type vrdp"
;;

start-vm-headless)
vm_name=`read_argument_with_white_spaces $@`
log_action_msg "Starting VM: $vm_name headless..."
$SU "$VBOXMANAGE startvm '$vm_name' -type headless"
;;

stop-vm)
vm_name=`read_argument_with_white_spaces $@`
log_action_msg "Stopping VM: $vm_name ..."
$SU "$VBOXMANAGE controlvm '$vm_name' acpipowerbutton"
;;

savestate-vm)
vm_name=`read_argument_with_white_spaces $@`
log_action_msg "Saving state and powering off VM: $vm_name ..."
$SU "$VBOXMANAGE controlvm '$vm_name' savestate"
;;

poweroff-vm)
vm_name=`read_argument_with_white_spaces $@`
log_action_msg "Powering off VM: $vm_name ..."
$SU "$VBOXMANAGE controlvm '$vm_name' poweroff"
;;

status)
echo "The following virtual machines are currently running:"
#flag=0
# This way is the pipe spawns new process. Every variable in while is in child process!
$SU "$VBOXMANAGE list runningvms" | while read VM; do
	echo -n "$VM ("
	#echo -n `echo $SU "VBoxManage showvminfo ${VM%% *}|grep -m 1 Name:|sed -e 's/^Name:s*//g'"`
	echo -n `$SU "VBoxManage showvminfo ${VM% *}|grep -m 1 Name:|sed -e 's/^Name:s*//g'"`
	echo ')'
	#echo ${VM% *}
done
#for VM in `$SU "$VBOXMANAGE list runningvms"`;
#do
#        flag=1
#        echo -n "$VM ("
        #echo -n `echo $SU "VBoxManage showvminfo ${VM%% *}|grep -m 1 Name:|sed -e 's/^Name:s*//g'"`
#        echo -n `$SU "VBoxManage showvminfo ${VM% *}|grep -m 1 Name:|sed -e 's/^Name:s*//g'"`
#        echo ')'
        #echo ${VM% *}
#done
#echo $flag
#if [ $flag == 0 ];then
# echo "None!"
#fi
;;

*)
echo "Usage: $0 {start|stop|status|start-vm <VM name>|stop-vm <VM name>|start-vm-headless <VM name|savestate-vm <VM name>|poweroff-vm <VM name>}"
exit 3
esac

exit 0
