#!/bin/bash

#
# An interactive script for importing VirtualBox clone machines
# @Author: Igor Ivanovski igor@genrepsoft.com
#

VBOXMANAGE=`whereis vboxmanage`;

echo "Checking for existing virtual machines ..."
# Check if there are registered VMs
number_vms=`vboxmanage list vms | wc -l`;

if [ $number_vms == 1 ]; then
	echo "Virtual Machine found";
	echo "Old machine will be deleted. Are you sure? Type y to confirm.";
	read line
	if [ $line != 'y' -o $line != 'Y' ]; then
		echo "Exiting...";
		exit 1;
	fi
	vm_name=`vboxmanage list vms | awk -F "{" '{print $2}' | tr -d '}'`;	
	# Unregister the Virtual Machine
	echo "${vm_name}";
	vboxmanage unregistervm "${vm_name}";
	if [ $? == 0 ]; then
	echo "Machine unregistered!";
	# Removing old Hard disk
	echo "Removing stale hdds";
	hdd_uuid=`vboxmanage list hdds | grep UUID | head -n 1 | awk '{print $2}'`;
	VBoxManage closemedium disk "$hdd_uuid"
	fi
else
	echo "No machines found, continuing...";
fi

# Take file locations
vbox_conf=0;
vbox_vdi=0;
while :
do
# Vbox configuration location
if [ $vbox_conf == 0 ]; then
echo "Set location of the vbox configuration file"
read -e -p "File path:" line
	if [ -e "$line" ]; then
	vbox_conf=1;
	vbox_conf_file=$line;
	continue;
	else 
		echo "File doesn't exist. Enter valid location.";
		continue;
	fi
fi

# Vbox vdi location
if [ $vbox_vdi == 0 ]; then
echo "Set location of the vbox vdi disk"
read -e -p "File path:" line
	if [ -e "$line" ]; then
	vbox_conf=1;
	vbox_vdi_file=$line;
	break;
	else
		echo "File doesn't exist. Enter valid location.";
		continue;
	fi
fi
done
echo $vbox_conf_file;
echo $vbox_vdi_file;

# Now we proceed with registering of the NEW virtual machine
echo "Registering the new machine"
# change the adapter to the adequate
sed -i 's/<BridgedInterface name=".*"\/>/<BridgedInterface name="enp2s0"\/>/g' "$vbox_conf_file";
vboxmanage registervm "$vbox_conf_file";
if [ $? == 0 ]; then
	echo "Done successful"
else
	exit 1;
fi

# Detaching old disk and associating the new one
vm_name=`vboxmanage list vms | awk -F "{" '{print $2}' | tr -d '}'`;
echo $vm_name;
vboxmanage storageattach "$vm_name" --storagectl SATA --port 0 --medium emptydrive;
vboxmanage storageattach "$vm_name" --storagectl SATA --port 0 --type hdd --medium "$vbox_vdi_file";
echo "Importing finished. You can start the machine with the following command: '/etc/init.d/vboxconrol start-vm-headless <name>'";

