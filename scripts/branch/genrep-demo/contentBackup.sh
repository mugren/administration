#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup";

# Backup dir
BACKUPDIRDAILY="$BACKUPDIR/full/$DATEYYMM/$DATEDD";

# Location of morm-portal Application
MORM_PORTAL_APP=opt/morm-portals/tomcat-portal/webapps/morm-portal

# Location of morm-internal1 Application
MORM_INTERNAL1_APP=opt/morm-portals/tomcat-internal1/webapps/morm-internal1

# Location of morm-internal2 Application
MORM_INTERNAL2_APP=opt/morm-portals/tomcat-internal2/webapps/morm-internal2

# List of Content to Backup:
IDS="
$MORM_PORTAL_APP
$MORM_INTERNAL1_APP
$MORM_INTERNAL2_APP
"

# We start to compress applications one by one
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done

# Unmount backup partiotion when the backup is done
echo "Unmounting $BACKUPDIR ...";
umount $BACKUPDIR;

