#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup/$DATEYYMM/$DATEDD"

# OpenVPN server.conf
OPENVPN_CONF=etc/openvpn/server.conf

# OpenVPN bridge
OPENVPN_BRIDGE=etc/init.d/bridge

# OpenVPN vars
OPENVPN_VARS=etc/openvpn/easy-rsa/vars

# OpenVPN keys
OPENVPN_KEYS=etc/openvpn/easy-rsa/keys

# All id files
CONTENTS="
$OPENVPN_CONF
$OPENVPN_BRIDGE
$OPENVPN_VARS
$OPENVPN_KEYS
"

# Assuming that NAS is mounted correctly
if [ ! -d $BACKUPDIR ]; then
	mkdir -p $BACKUPDIR
fi

# We compress contents
for content in $CONTENTS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIR/`basename /$content`.tar.bz2 -C / $content;
	done
