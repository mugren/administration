#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup";

# Daily backup dir
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYMM/$DATEDD"

# OpenVPN server.conf
OPENVPN_CONF=etc/openvpn/server.conf

# Network Interfaces Configuration
NETWORK_INTERFACES=etc/network/interfaces

# OpenVPN vars
OPENVPN_VARS=etc/openvpn/easy-rsa/vars

# OpenVPN keys
OPENVPN_KEYS=etc/openvpn/easy-rsa/keys

# All files and directories to backup
CONTENTS="
$OPENVPN_CONF
$NETWORK_INTERFACES
$OPENVPN_VARS
$OPENVPN_KEYS
"
# Initialize the status
STATUS=0;

# Check if backup partition is mounted
check_mount(){
	if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
		echo "$BACKUPDIR is allready mounted";
	else
		echo "Mounting $BACKUPDIR ...";
		mount $BACKUPDIR;
		if [ $? -eq 0 ]; then
			echo "$BACKUPDIR successfully mounted";
		else
			echo "Failed to mount $BACKUPDIR";
			STATUS=1;
		fi
	fi
}

# Do the backup
backup(){
	# assuming that NFS share is mounted via /etc/fstab
	if [ ! -d $BACKUPDIRDAILY ]; then
		mkdir -p $BACKUPDIRDAILY;
	else
		echo "$BACKUPDIRDAILY allready exists!!!";
	fi
	
	# We compress contents
	for content in $CONTENTS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done
}

check_mount;
if [ "$STATUS" = "0" ]; then
	echo "Making backup...";
	backup;
	echo "Backup finished, unmounting $BACKUPDIR ...";
	umount $BACKUPDIR;
else
	echo "Not doing backup, exiting...";
fi
