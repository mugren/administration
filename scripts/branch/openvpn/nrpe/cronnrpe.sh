#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/nrpe/logs";

# What is the name of the log
LOG="$LOG_DIR/nrpe-$DATE.log";

# Statistics script dir
BACKUPDIRBIN="/opt/nrpe/bin";

# Backup script name
BACKUPBIN="backupnrpe.sh";

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN > $LOG
