#!/bin/bash

#
# This script tests for network connectivity and restarts ppp if it is found
# to be down.
#

# --- User-modifiable variables ---

echo "Test script started.";
echo "Timestamp of execution : `date '+%H:%M %d/%m/%y'`";

# Primary address
primaryaddr="77.28.0.1"

# Number of failed pings required to signify link failure
failedtrigger=2

# File to keep track of total number of failed pings
failedcountfile="/usr/local/etc/pingmonitor.missedping.count"


# SMTP username
SMTPUSER="igor@genrepsoft.net";

# SMTP password
SMTPPASS="s@pun1ca";

# SMTP server
SMTPSERVER="10.10.30.103";

# Mail from
MAILFROM="igor@genrepsoft.net"

# Mail to
MAILTO="ivanovski.i89@gmail.com";

# Mail to cc
#MAILCCTO="mihajlo.atanasoski@sapiens.eu, simoncev@gmail.com";

# Mail subject
MAILSUBJECT="PPP have recovered from an error and manually restarted";

# Mail message
MAILMESSAGE="Please don't reply on this message, it's auto-generated because of Point-to-Point Protocol failure on firewall.genrepsoft.inc. The service was successfully restarted on `date '+%H:%M %d/%m/%y'`";

# Full path of SendEmail executable
SENDMAIL="/usr/local/bin/sendEmail";

# --- End of user-modifiable variables ---

# Set umask
umask 137


# Check if we've had any previous failures
if [ -f $failedcountfile ]; then
        pingfailed=`head -n 1 $failedcountfile`
else
        pingfailed=0
fi

echo "Testing gateway accessability ...";
# Run the ping for the primary address - our default gateway.
/bin/ping -c 4 -t 5 -q -m 2 $primaryaddr > /dev/null 2> /dev/null

# If the ping failed. Check to see if the gateway is filtered.
if [ $? -ne 0 ]; then
        ping_error=1
else
        # No filtering. The default gateway responded to the initial ping.
        ping_error=0
fi


#
# Ping returns a non-zero error condition if ALL the ECHO_REQUEST packets did
# not return a ECHO_REPLY. If we received just one answer then ping returns
# a zero error condition which is perfect for our tests.
#

echo "Test ended: ";
echo "ping_error = ${ping_error}";

# Test the error condition
if [ $ping_error -ne 0 ]; then
        # Update and record the failure count
        pingfailed=$(($pingfailed + 1))
        echo $pingfailed > $failedcountfile
	echo "	Ping failed : ${pingfailed} time(s).";


        # Test if we've hit our failure trigger
        if [ $pingfailed -ge $failedtrigger ]; then
		echo "	Ping failed more than ${failedtrigger} times. Restarting pppd daemon.";
                # time to restart ppp so kill it first
                /usr/bin/killall pppd > /dev/null 2> /dev/null

                # wait for it to die
                sleep 5

                # really ensure ppp is dead - we can't risk two running
                /usr/bin/killall -9 pppd > /dev/null 2> /dev/null

                # wait again
                sleep 5

                # start up ppp again
                /usr/bin/pon dsl-provider > /dev/null 2> /dev/null

                # our work here is done
        fi
else

	echo "	Transsmission correct.";
        # the ping worked so check if we've just recovered from a failure
        if [ $pingfailed -ge $failedtrigger ]; then
		echo "	Successfully recovered modem hangup. Sending email to admin.";
                # we have just recovered so let the admin know
		$SENDMAIL -f $MAILFROM -t $MAILTO -u "$MAILSUBJECT" -m "$MAILMESSAGE" -s $SMTPSERVER -xu $SMTPUSER -xp $SMTPPASS > /dev/null 2> /dev/null
        fi


        # all's well now so remove the failure count
        rm -f $failedcountfile > /dev/null 2> /dev/null
fi

echo "End of script. Bye.";
# that's it

