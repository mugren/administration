#!/bin/bash

SMTPUSER="igor@genrepsoft.net";

# SMTP password
SMTPPASS="s@pun1ca";

# SMTP server
SMTPSERVER="10.10.30.103";

# Mail from
MAILFROM="igor@genrepsoft.net"

# Mail to
MAILTO="ivanovski.i89@gmail.com";

# Mail to cc
#MAILCCTO="mihajlo.atanasoski@sapiens.eu, simoncev@gmail.com";

# Mail subject
MAILSUBJECT="Test mail from bash script";

# Mail message
MAILMESSAGE="Tested on `date '+%H:%M %d/%m/%y'` . If you read this then its working.";

# Full path of SendEmail executable
SENDMAIL="/usr/local/bin/sendEmail";

$SENDMAIL -f $MAILFROM -t $MAILTO -u "$MAILSUBJECT" -m "$MAILMESSAGE" -s $SMTPSERVER -xu $SMTPUSER -xp $SMTPPASS > log.txt 2> error.txt

