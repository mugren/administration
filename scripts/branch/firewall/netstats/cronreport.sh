#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is statistics logs directory
LOG_DIR="/opt/netstats/logs";

# What is the name of the log
LOG="$LOG_DIR/netreport-$DATE.log";

# Statistics script dir
STATSDIRBIN="/opt/netstats/bin";

# Backup script name
STATSBIN="report.sh";

# Call the script to generate the report
$STATSDIRBIN/$STATSBIN > $LOG

