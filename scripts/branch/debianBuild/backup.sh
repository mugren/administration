#!/bin/bash

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep "inet " | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Backing up mysql databases..."
/opt/backup/bin/backupmysql.sh
echo "Backing up Content..."
/opt/backup/bin/contentBackup.sh
echo "********************************************************************"
echo "********************************************************************"
