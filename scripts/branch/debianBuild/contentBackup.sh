#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup";

# Backup dir
BACKUPDIRDAILY="$BACKUPDIR/full/$DATEYYMM/$DATEDD";

# Location of genrep-net-portal Application
GENREP_NET_PORTAL_APP=opt/tomcat/webapps/genrep-net-portal

# Location of genrep-net-test Application
GENREP_NET_TEST_APP=opt/tomcat/webapps/genrep-net-test

# Location of genrep-net-admin Application
GENREP_NET_ADMIN_APP=opt/tomcat/webapps/genrep-net-admin

# List of Content to Backup:
IDS="
$GENREP_NET_PORTAL_APP
$GENREP_NET_TEST_APP
$GENREP_NET_ADMIN_APP
"

# We start to compress applications one by one
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done

# Unmount backup partiotion when the backup is done
echo "Unmounting $BACKUPDIR ...";
umount $BACKUPDIR;

