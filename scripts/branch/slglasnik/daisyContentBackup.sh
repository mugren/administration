#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/data2Arch/backup/daisy/$DATEYYMM/$DATEDD"

# Location of Daisy Application
DAISY_HOME=data2/daisy-2.2

# Location of Daisy Wiki Data
DAISY_WIKI_DATA=data2/daisywikidata

# Location of Daisy Repository Data
DAISY_REPO_DATA=data2/daisyrepodata

# List of Content to Backup:
IDS="
$DAISY_HOME
$DAISY_WIKI_DATA
$DAISY_REPO_DATA
"

# We compress Daisy Application
for content in $IDS
	do
		echo "Compressing /$content"
		tar -cPf $BACKUPDIR/`basename /$content`.tar -C / --exclude=$DAISY_WIKI_DATA/logs/* \
                --exclude=$DAISY_WIKI_DATA/tmp/* \
                --exclude=$DAISY_REPO_DATA/logs/* \
                $content;
	done

