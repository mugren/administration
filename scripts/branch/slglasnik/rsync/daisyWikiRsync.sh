#!/bin/bash

# Remote Hostname or IP Address
REMOTEHOST="10.10.2.14";

# Remote Dir
REMOTEDIR="/backup/data2/daisywikidata/";

# Local Dir
LOCALDIR="/data2/daisywikidata/";

echo "Replicating Daisy Wiki"
rsync -ave "ssh -i /data2/backup/id_daisybackup_rsa" --delete \
--exclude "logs/*" \
--exclude "tmp/*" \
--exclude "service/*" \
$REMOTEHOST:$REMOTEDIR $LOCALDIR

