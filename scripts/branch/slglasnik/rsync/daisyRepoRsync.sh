#!/bin/bash

# Remote Hostname or IP Address
REMOTEHOST="10.10.2.14";

# Remote Dir
REMOTEDIR="/backup/data2/daisyrepodata/";

# Local Dir
LOCALDIR="/data2/daisyrepodata/";

echo "Replicating Daisy Repository"
rsync -ave "ssh -i /data2/backup/id_daisybackup_rsa" --delete \
--exclude "logs/*" \
--exclude "service/*" \
$REMOTEHOST:$REMOTEDIR $LOCALDIR

