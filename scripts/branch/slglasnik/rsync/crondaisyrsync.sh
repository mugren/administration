#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/data2/backup/bin/rsync/daisyRsync.sh > /data2/backup/logs/daisyRsync$DATE.log

#SUBJECT="Daisy Cron Daemon"
#cat /backup/logs/$DATE.log | mail -s "$SUBJECT" mail@domail.com
