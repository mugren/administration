#!/bin/bash

echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop
echo "Stopping Daisy Repository and Daisy Wiki on remote server: "
ssh -i /data2/backup/id_daisybackup_rsa 10.10.2.14 /etc/init.d/daisy-wiki stop
ssh -i /data2/backup/id_daisybackup_rsa 10.10.2.14 /etc/init.d/daisy-repo stop
echo "Backing up mysql databases..."
echo "Mysql database backup started on: `date`"
/data2/backup/bin/rsync/backupmysql.sh
echo "Mysql database backup completed on: `date`"
echo "Backing up Daisy Content..."
echo "Daisy Wiki Data replication started on: `date`"
/data2/backup/bin/rsync/daisyWikiRsync.sh
echo "Daisy Wiki Data replication completed on: `date`"
echo "Daisy Repo Data replication started on: `date`"
/data2/backup/bin/rsync/daisyRepoRsync.sh
echo "Daisy Repo Data replication completed on: `date`"
echo "Cleaning ActiveMQ messages: "
/data2/backup/bin/rsync/emptyJMSqueue.sh
echo "Starting Daisy Repository and Daisy Wiki services on remote server: "
ssh -i /data2/backup/id_daisybackup_rsa 10.10.2.14 /etc/init.d/daisy-repo start
sleep 60
ssh -i /data2/backup/id_daisybackup_rsa 10.10.2.14 /etc/init.d/daisy-wiki start
echo "Importing mysql databases..."
echo "Mysql database import started on: `date`"
/data2/backup/bin/rsync/importmysql.sh
echo "Mysql database import completed on: `date`"
echo "Starting Daisy Repository and Daisy Wiki services: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
#echo "Full backup started on: `date`"
#/data2/backup/bin/crondaisy.sh
#echo "Full backup completed on: `date`"
echo "********************************************************************"
echo "********************************************************************"

