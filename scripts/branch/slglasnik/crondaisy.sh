#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/data2/backup/bin/daisyBackup.sh > /data2/backup/logs/daisyFull$DATE.log

#SUBJECT="Daisy Cron Daemon"
#cat /data2/backup/logs/$DATE.log | mail -s "$SUBJECT" mail@domail.com
