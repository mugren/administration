#!/bin/bash

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ifconfig eth1 | grep "inet addr:" | awk '{print $2}' | cut -d: -f 2
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop
echo "Backing up mysql databases..."
echo "Mysql database backup started on: `date`"
/data2/backup/bin/backupmysql.sh
echo "Mysql database backup completed on: `date`"
echo "Backing up Daisy Content..."
echo "Daisy Content backup started on: `date`"
/data2/backup/bin/daisyContentBackup.sh
echo "Daisy Content backup completed on: `date`"
echo "Starting Daisy Repository and Daisy Wiki: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
echo "********************************************************************"
echo "********************************************************************"
