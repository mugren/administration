#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

# Where is backup logs directory
LOG_DIR=/storage/backup/logs

# What is the name of the log
LOG=$LOG_DIR/portalBackup-$DATE.log

# Backup script dir
BACKUPDIRBIN=/storage/backup/bin/portal/1.0

# Backup script name
BACKUPBIN=portalBackUp.sh

# Do the backup
$BACKUPDIRBIN/$BACKUPBIN > $LOG

# SMTP username
SMTPUSER="info@akademika.com.mk";

# SMTP password
SMTPPASS="akademik";

# SMTP server
SMTPSERVER="smtp.t-home.mk";

# Mail from
MAILFROM="info@akademika.com.mk"

# Mail to
MAILTO="vpesov@genrepsoft.com";

# Mail to cc
MAILCCTO="zenkovik.maja@gmail.com, mkceva@gmail.com, simoncev@gmail.com";

# Mail subject
MAILSUBJECT="Akademika Cron Daemon";

# Full path of SendEmail executable
SENDMAIL="/usr/bin/sendEmail";

# Send the message
$SENDMAIL -f $MAILFROM -t $MAILTO -cc $MAILCCTO -u "$MAILSUBJECT" -m "`cat $LOG`" -s $SMTPSERVER -xu $SMTPUSER -xp $SMTPPASS
