#!/bin/bash

# Backup script dir
BACKUPDIRBIN=/storage/backup/bin/portal/1.0

# Backup script name
BACKUPCONTENTBIN=contentBackup.sh

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep inet[^6] | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "Informations about disk usage:"
df -h
echo "Informations about memory usage:"
free -m
vmstat 1 5
echo "********************************************************************"
echo "Report from Akademika Portal backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Akademika Portal and Akademika Registration..."
/etc/init.d/tomcat-akademika1-registration stop
/etc/init.d/tomcat-akademika1 stop
echo "Backing up Content..."
$BACKUPDIRBIN/$BACKUPCONTENTBIN
echo "Starting Akademika Portal and Akademika Registration..."
/etc/init.d/tomcat-akademika1-registration start
/etc/init.d/tomcat-akademika1 start
echo "********************************************************************"
echo "********************************************************************"
