#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: YYYY
DATEYYYY=`date +%Y`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup";

# Backup dir daily
BACKUPDIRDAILY="$BACKUPDIR/$DATEYYYY/portal/$DATEYYMM/$DATEDD";

# Location of akademika-portal Application
AKADEMIKA_PORTAL_APP=storage/akademika1/tomcat/webapps/ROOT

# Location of akademika-registration Application
AKADEMIKA_REGISTRATION_APP=storage/akademika1/tomcat-registration/webapps/akademika-registration

# Location of akademika.properties
AKADEMIKA_PROPERTIES_FILE=storage/akademika1/akademika.properties

# Location of Derby Database Directory
DERBY_HOME=storage/akademika1/derbyroot

/storage/akademika1/derbyroot
# List of Content to Backup:
CONTENTS="
$AKADEMIKA_PORTAL_APP
$AKADEMIKA_REGISTRATION_APP
$AKADEMIKA_PROPERTIES_FILE
$DERBY_HOME
"

# Check if backup partition is mounted
if [ -n "$(mount -l | grep $BACKUPDIR)" ]; then
    echo "$BACKUPDIR is allready mounted";
else
    echo "Mounting $BACKUPDIR ...";
    mount $BACKUPDIR;
fi

# assuming that /mnt/backup is mounted
if [ ! -d $BACKUPDIRDAILY ]; then
    mkdir -p $BACKUPDIRDAILY
else
    echo "$BACKUPDIR allready exists!!!";
    exit 1;
fi

# We start to compress content one by one
for content in $CONTENTS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIRDAILY/`basename /$content`.tar.bz2 -C / $content;
	done
	
# Unmount backup partiotion when the backup is done
echo "Unmounting $BACKUPDIR ...";
umount $BACKUPDIR;
