#!/bin/bash
SUBJECT="Akademika Cron Daemon"
export SUBJECT
echo "********************************************************************">MESSAGE
echo "********************************************************************">>MESSAGE
echo "The name of this script is $0 , and it's location is:">>MESSAGE
pwd>>MESSAGE
echo "On the host:">>MESSAGE
hostname>>MESSAGE
echo "With IP Address:">>MESSAGE
cat /etc/network/interfaces | grep address | awk '{print $2}'>>MESSAGE
echo "With uptime:">>MESSAGE
uptime>>MESSAGE
echo "Informations about disk usage:">>MESSAGE
df -h>>MESSAGE
echo "Informations about memory usage:">>MESSAGE
free -m >> MESSAGE
vmstat 1 5 >> MESSAGE
echo "********************************************************************">>MESSAGE
echo "Report from Islands backup on: `date`">>MESSAGE
echo "********************************************************************">>MESSAGE
echo "********************************************************************">>MESSAGE
#tar -jcvf /mnt/backup/2010/webapp/webapp-`date +%F`.tar.bz2 /storage/tomcat/webapps/ROOT >> MESSAGE
tar -jcvf /mnt/backup/2011/data/data-`date +%F`.tar.bz2 /storage/tomcat/Data/Islands1 >> MESSAGE
tar -jcvf /mnt/backup/2011/webapp/akademika-registration-`date +%F`.tar.bz2 /storage/tomcat-registration/webapps/akademika-registration >> MESSAGE
tar -jcvf /mnt/backup/2011/data/registration-`date +%F`.tar.bz2 /storage/tomcat-registration/bin/registration >> MESSAGE
echo "********************************************************************">>MESSAGE
echo "********************************************************************">>MESSAGE
cat MESSAGE | mail -s "$SUBJECT" vpesov@genrepsoft.com lazovb@akademika.com.mk ristes@gmail.com simoncev@gmail.com
#rm -rf MESSAGE

