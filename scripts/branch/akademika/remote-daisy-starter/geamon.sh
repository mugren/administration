#!/bin/bash
#
# @author igor\smrdec
#

wiki_status=0
repo_status=0

daisy_init() {
# first arg is name if the script
# second argument is the calling command
/etc/init.d/daisy-"$1" "$2"
}

izlez_wiki=`sh /etc/init.d/daisy-wiki status`
izlez_wiki2=`echo "$izlez_wiki" | grep "is not running"`

if [ "X$izlez_wiki2" = "X" ]
	then
		echo "Wiki is running!"
		wiki_status=1
else	
	echo "Wiki is not running!"
fi

izlez_repo=`sh /etc/init.d/daisy-repo status`
izlez_repo2=`echo "$izlez_repo" | grep "is not running"`

if [ "X$izlez_repo2" = "X" ]
        then
                echo "Repo is running!"
                repo_status=1
else
        echo "Repo is not running!"
fi

if [[ "$wiki_status" -eq "1"  &&  "$repo_status" -eq "1" ]]
	then
		echo "Daisy Wiki && Repo are running, but malfunctioning!"
		echo "Doing restart..."
		daisy_init wiki stop
		sleep 10
		daisy_init repo stop
		sleep 60
		daisy_init repo start
		sleep 60
		daisy_init wiki start
		sleep 5
		echo "System restarted."
	fi

if [[ "$wiki_status" -eq "0" && "$repo_status" -eq "1" ]]
	then
		echo "Daisy Wiki is down."
		echo "Starting it up!"
		daisy_init wiki start
		sleep 5
		echo "Wiki started."
	fi

if [[ "$wiki_status" -eq "1" && "$repo_status" -eq "0" ]]
	then
		echo "Daisy Repo is not started."
		echo "Starting it up together with Daisy Wiki."
		daisy_init wiki stop
                sleep 10
                daisy_init repo start
                sleep 60
                daisy_init wiki start
                echo "System started."
	fi

if [[ "$wiki_status" -eq "0" && "$repo_status" -eq "0" ]]
	then
		echo "System is down."
		echo "Starting ..."
                daisy_init repo start
                sleep 60
                daisy_init wiki start
                sleep 5
                echo "System started."
	fi

echo "Bye!"
