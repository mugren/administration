#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`

/storage/backup/bin/daisyBackup.sh > /storage/backup/logs/$DATE.log

SUBJECT="Daisy Cron Daemon"
cat /storage/backup/logs/$DATE.log | mail -s "$SUBJECT" vpesov@genrepsoft.com lazovb@akademika.com.mk ristes@gmail.com simoncev@gmail.com vasko.gjurovski@gmail.com

