#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
BACKUPDIR="/mnt/backup/2010/daisy/$DATEYYMM/$DATEDD";

# assuming that /nas is mounted via /etc/fstab
if [ ! -d $BACKUPDIR ]; then
  mkdir -p $BACKUPDIR
fi


echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
#cat /etc/network/interfaces | grep address | awk '{print $2}'
ip addr show eth0 | grep inet | grep -v inet6 | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop
echo "Backing up mysql databases..."
/storage/backup/bin/backupmysql.sh
echo "Backing up Daisy Content..."
/storage/backup/bin/daisyContentBackup.sh
#cp -R /storage/backup/daisy/$DATEYYMM/$DATEDD/* $BACKUPDIR/
#rm -rf /storage/backup/daisy/$DATEYYMM/$DATEDD/*
echo "Starting Daisy Repository and Daisy Wiki: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
echo "********************************************************************"
echo "********************************************************************"
