#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`;

# Now in format: DD
DATEDD=`date +%d`;

# Backup dir
REMOTEBACKUPDIR="/storage/backup/daisy/$DATEYYMM/$DATEDD";

echo "********************************************************************"
echo "********************************************************************"
echo "The name of this script is $0"
echo "On the host:"
hostname
echo "With IP Address:"
ip addr show eth0 | grep inet[^6] | awk '{print $2}' | cut -d/ -f1
echo "With uptime:"
uptime
echo "Informations about disk usage:"
df -h
echo "Informations about memory usage:"
free -m
vmstat 1 5
echo "********************************************************************"
echo "Report from Daisy backup on: `date`"
echo "********************************************************************"
echo "********************************************************************"
echo "Stopping Daisy Wiki and Daisy Repository..."
/etc/init.d/daisy-wiki stop
/etc/init.d/daisy-repo stop

# checking existence of remote backup dir
ssh -i /storage/backup/bin/fullscp/id_rsa islands@192.168.1.101 "test -d $REMOTEBACKUPDIR"
if [[ "$?" -ne 0 ]]; then
	echo "Creating remote dir: $REMOTEBACKUPDIR"
	ssh -i /storage/backup/bin/fullscp/id_rsa islands@192.168.1.101 "mkdir -p $REMOTEBACKUPDIR"
else
	echo "Remote directory: $REMOTEBACKUPDIR allready exists"
fi

echo "Backing up mysql databases..."
/storage/backup/bin/fullscp/backupmysql.sh
echo "Backing up Daisy Content..."
/storage/backup/bin/fullscp/daisyContentBackup.sh
scp -ri /storage/backup/bin/fullscp/id_rsa /storage/backup/daisy/$DATEYYMM/$DATEDD/* islands@192.168.1.101:$REMOTEBACKUPDIR/
echo "Starting Daisy Repository and Daisy Wiki: "
/etc/init.d/daisy-repo start
sleep 60
/etc/init.d/daisy-wiki start
echo "********************************************************************"
echo "********************************************************************"
