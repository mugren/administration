#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/storage/backup/daisy/$DATEYYMM/$DATEDD"

# Location of Daisy Application
DAISY_HOME=storage/eJurist

# Location of Daisy Wiki Data
DAISY_WIKI_DATA=storage/eJuristData

# Location of Daisy Repository Data
DAISY_REPO_DATA=storage/repoData

# List of Content to Backup:
IDS="
$DAISY_HOME
$DAISY_WIKI_DATA
$DAISY_REPO_DATA
"

# We compress Daisy Application
for content in $IDS
	do
		echo "Compressing /$content"
		tar -jcPf $BACKUPDIR/`basename /$content`.tar.bz2 -C / --exclude=$DAISY_WIKI_DATA/logs/* \
                --exclude=$DAISY_WIKI_DATA/tmp/* \
                --exclude=$DAISY_REPO_DATA/logs/* \
                $content;
	done
