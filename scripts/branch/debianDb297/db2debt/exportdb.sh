#!/bin/bash

# The following three lines have been added by UDB DB2.
if [ -f /home/db2debt/sqllib/db2profile ]; then
    . /home/db2debt/sqllib/db2profile
fi

db2 -v CONNECT TO $1 USER db2debt USING db2debt;
db2 -v QUIESCE DATABASE IMMEDIATE FORCE CONNECTIONS;
db2 -v CONNECT RESET;
db2 -v BACKUP DATABASE $1 TO "/home/db2debt/backup/$1" WITH 2 BUFFERS BUFFER 1024 PARALLELISM 1 WITHOUT PROMPTING;
db2 -v CONNECT TO $1;
db2 -v UNQUIESCE DATABASE;
db2 -v CONNECT RESET;

BACKUPFILE=`ls /home/db2debt/backup/$1`

cp -R /home/db2debt/backup/$1/$BACKUPFILE /mnt/backup/DB/$1

rm -rf /home/db2debt/backup/$1/$BACKUPFILE

