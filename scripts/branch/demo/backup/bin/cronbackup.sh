﻿#!/bin/bash

# Now in format: YYYY-MM-DD
DATE=`date +%F`;

# Where is backup logs directory
LOG_DIR="/opt/backup/bin/logs";

# What is the name of the log
LOG="$LOG_DIR/backup-$DATE.log";

# Statistics script dir
BACKUPDIRBIN="/opt/backup/bin";

# Backup script name
BACKUPBIN="backup.sh";

# Get the schedule(weekly or monthly) from the first input parameter
SCHEDULE=$1;

# Call the script to do the backup
$BACKUPDIRBIN/$BACKUPBIN $SCHEDULE > $LOG
