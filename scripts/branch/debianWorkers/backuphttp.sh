#!/bin/bash

# Now in format: YYYY-MM
DATEYYMM=`date +"%Y-%m"`

# Now in format: DD
DATEDD=`date +%d`

# Backup dir
BACKUPDIR="/mnt/backup/$DATEYYMM/$DATEDD"

# Apache HTTP DIR
APACHE_DIR=etc/apache2

# Assuming that NAS is mounted correctly
if [ ! -d $BACKUPDIR ]; then
	mkdir -p $BACKUPDIR
fi

# We compress contents
echo "Compressing /$APACHE_DIR"
tar -jcPf $BACKUPDIR/`basename /$APACHE_DIR`.tar.bz2 -C / $APACHE_DIR
