/**
 * Created by igor on 5/7/14.
 */

import groovy.io.FileType
import groovy.json.JsonSlurper

// Structure of TEST_ANSWER_OBJECT

//        | UID          | varchar(255)  | NO   | PRI | NULL    |          |
//        | answerId     | varchar(255)  | YES  |     | NULL    |   ima    |
//        | answerValue  | varchar(2048) | YES  |     | NULL    |   ima    |
//        | answerText   | varchar(255)  | YES  |     | NULL    |   ima    |
//        | questionId   | varchar(255)  | YES  |     | NULL    |   ima    |
//        | type         | varchar(255)  | YES  |     | NULL    |   ima    |
//        | correct      | bit(1)        | YES  |     | NULL    |   ima    |
//        | TEST_UID     | varchar(255)  | YES  | MUL | NULL    |   primary kluc od TEST_ANSWERS    |
//        | questionText | varchar(1024) | YES  |     | NULL    |   ima    |

// Structure of TEST_ANSWERS

//        | UID          | varchar(255)  | NO   | PRI | NULL    |       |
//        | sessionId    | varchar(255)  | YES  |     | NULL    |       |
//        | definitionId | varchar(255)  | YES  |     | NULL    |       |
//        | userId       | varchar(255)  | YES  |     | NULL    |       |
//        | username     | varchar(255)  | YES  |     | NULL    |       |
//        | testStatus   | varchar(255)  | YES  |     | NULL    |       |
//        | totalPoints  | decimal(19,2) | YES  |     | NULL    |       |
//        | points       | decimal(19,2) | YES  |     | NULL    |       |
//        | percentage   | double        | YES  |     | NULL    |       |

String filePath = "/home/igor/Administration/BJN/2014-05-06/rezultati/"

def listJSONFiles = []

// Create TestAnswerList and TestAnswerObjectList
ArrayList<TestAnswers> taList = new ArrayList<TestAnswers>();
ArrayList<TestAnswerObject> taoList = new ArrayList<TestAnswerObject>()
def uniqueIds = []

def dir = new File(filePath)
dir.eachFileRecurse (FileType.FILES) { file ->
    listJSONFiles << file
}

listJSONFiles.eachWithIndex { file , idx ->
    println file.path
    println file.name


    def inputFile = new File(file.path.toString())
    def cleanJSON = inputFile.text.substring(17)
    println cleanJSON
    def inputJSON = new JsonSlurper().parseText(cleanJSON)
    ArrayList<HashMap> answers = inputJSON.answers

    // Create TestAnswers

    TestAnswers ta = new TestAnswers()
    ta.definitionId = inputJSON.definitionId
    ta.percentage = inputJSON.percentage
    ta.points = inputJSON.points
    ta.sessionId = inputJSON.sessionId
    ta.username = inputJSON.username
    if((int)inputJSON.points >= 80){
        ta.testStatus = "EXAM_PASS"
    }
    else{
        ta.testStatus = "EXAM_FELL"
    }
    ta.userId = inputJSON.userId
    ta.totalPoints = inputJSON.totalPoints
    ta.UID = "ff07052014taid" + idx.toString()

    taList.add(ta)

    // Create TestAnswerObjects

    answers.eachWithIndex {answer , idY ->

        TestAnswerObject tao = new TestAnswerObject()
        tao.answerId = answer.get('answerId')
        tao.answerText = answer.get('answerText')
        tao.answerValue = answer.get('answerValue')
        tao.correct = answer.get('correct')
        tao.questionId = answer.get('questionId')
        tao.questionText = answer.get('questionText')
        tao.TEST_UID = ta.getUID()
        tao.type = answer.get('type')
        tao.UID = "ff07052014taoid" + idx.toString() + idY.toString() + idY.toString()

        uniqueIds << tao.UID

        taoList.add(tao)
    }

    println taList.size()
    println taoList.size()

}
println taList.size()
println taoList.size()

// Add Lib dynamically to classpath and insert data in database
def defaultPathBase = new File( "." ).getCanonicalFile().getParent()
this.getClass().classLoader.rootLoader.addURL(new File(defaultPathBase+"/lib/mysql-connector-java-5.1.30-bin.jar").toURL())

def sql = groovy.sql.Sql.newInstance(
        "jdbc:mysql://10.10.30.15:3306/Class_Manager_prod?autoReconnect=true",
        "bjnexam",
        "maverick@2010u",
        "com.mysql.jdbc.Driver")

taList.each { testAnswer ->

    def params = [testAnswer.UID, testAnswer.sessionId, testAnswer.definitionId, testAnswer.userId, testAnswer.username,
                  testAnswer.testStatus, testAnswer.totalPoints, testAnswer.points, testAnswer.percentage ]
    sql.execute 'insert into TEST_ANSWERS (UID, sessionId, definitionId, userId, username,' +
                'testStatus, totalPoints, points, percentage) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', params


}

taoList.each { testAnswerObject ->
    def params = [testAnswerObject.UID, testAnswerObject.answerId, testAnswerObject.answerValue, testAnswerObject.answerText,
                  testAnswerObject.questionId,testAnswerObject.type, testAnswerObject.correct, testAnswerObject.TEST_UID,
                  testAnswerObject.questionText ]
        sql.execute 'insert into TEST_ANSWER_OBJECT (UID, answerId, answerValue, answerText, questionId,' +
                    'type, correct, TEST_UID, questionText) values (?, ?, ?, ?, ?, ?, ?, ?, ?)', params
}
