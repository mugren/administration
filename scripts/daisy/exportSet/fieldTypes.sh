#!/bin/bash

#* Filename to construct *#
FILENAME=fieldTypes.xml

### Server Setup ###
#* MySQL login user name *#
MUSER="root";

#* MySQL login PASSWORD name *#
MPASS="nomis11s";

#* MySQL login HOST name *#
MHOST="localhost";
MPORT="3306";

#* Get all names of Field Types *#
FIELDTYPES="$(mysql -u $MUSER -p$MPASS -h $MHOST -P $MPORT -Bse 'select name from daisyrepository.field_types')"

#* Begin constructing the XML *#

printf "<?xml version=\"1.0\"?>\n" > $FILENAME
printf "<exportSet>\n" >> $FILENAME

for ft in $FIELDTYPES
do
	echo "Adding $ft to Export Set..."
	printf "<fieldType>$ft" >> $FILENAME
	printf "</fieldType>\n" >> $FILENAME
done

printf "</exportSet>\n" >> $FILENAME 
