#!/bin/bash

#* Filename to construct *#
FILENAME=partTypes.xml

### Server Setup ###
#* MySQL login user name *#
MUSER="root";

#* MySQL login PASSWORD name *#
MPASS="nomis11s";

#* MySQL login HOST name *#
MHOST="localhost";
MPORT="3306";

#* Get all names of Field Types *#
PARTTYPES="$(mysql -u $MUSER -p$MPASS -h $MHOST -P $MPORT -Bse 'select name from daisyrepository.part_types')"

#* Begin constructing the XML *#

printf "<?xml version=\"1.0\"?>\n" > $FILENAME
printf "<exportSet>\n" >> $FILENAME

for ft in $PARTTYPES
do
	echo "Adding $ft to Export Set..."
	printf "<partType>$ft" >> $FILENAME
	printf "</partType>\n" >> $FILENAME
done

printf "</exportSet>\n" >> $FILENAME 
