﻿#!/bin/bash

EMAIL_ADDRESS="vlatkop@gmail.com"
MOUNT_POINT="/mnt/backup"
MAX_DURATION="28800"
MY_COMMAND="du -h /opt"

MAIL=`which mail`

SUCCESS=true

send_mail_cmd_timeout() {
$MAIL -s "Command was running too long" $EMAIL_ADDRESS <<EOM
Error!
The command was running too long.
EOM
}

send_mail_cmd_success() {
$MAIL -s "Backup was successful" $EMAIL_ADDRESS <<EOM
Success!
Backup was successful.
EOM
}

send_mail_mount_not_mounted() {
$MAIL -s "$MOUNT_POINT not mounted" $EMAIL_ADDRESS <<EOM
Error!
$MOUNT_POINT is not mounted.
EOM
}

cmd_timeout() {
   [ $# -eq 2 ] || echo "cmd_timeout takes 2 arguments"
   command=$1
   sleep_time=$2

   # run $command in background, sleep for our timeout then kill the process if it is running
   # $! has the pid of the backgrounded job
   $command &
   cmd_pid=$!

   # sleep for our timeout then kill the process if it is running
   ( sleep $sleep_time && kill $cmd_pid && echo "ERROR - killed $command due to timeout $sleep_time exceeded" ) &
   killer_pid=$!

   # 'wait' for cmd_pid to complete normally.  If it does before the timeout is reached, then
   # the status will be zero.  If the killer_pid terminates it, then it will have a non-zero
   # exit status
   wait $cmd_pid &> /dev/null
   wait_status=$?

   if [ $wait_status -ne 0 ]; then
      echo "WARNING - command, $command, unclean exit"
      SUCCESS=false
   else
      # Normal exit, detach and clean up the useless killer_pid
      disown $killer_pid
      kill $killer_pid &> /dev/null
   fi

   return $wait_status

}

# Check if backup partition is mounted
if [ -n "$(mount -l | grep $MOUNT_POINT)" ]; then
    echo "$MOUNT_POINT is mounted, backup will run..."
        cmd_timeout "$MY_COMMAND" "$MAX_DURATION"
        if $SUCCESS; then
                send_mail_cmd_success
        else
                send_mail_cmd_timeout
        fi
else
    echo "$MOUNT_POINT is not mounted, sending error message...";
        send_mail_mount_not_mounted
fi
