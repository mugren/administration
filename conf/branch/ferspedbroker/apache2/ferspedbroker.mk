<VirtualHost *:80>

	ServerAdmin admin@ferspedbroker.mk
	ServerName ferspedbroker.mk

	# Send servlet for context /ferspedbroker to worker named tomcatFersped
	JkMount		/ferspedbroker		tomcatFersped

        # Send JSPs for context /ferspedbroker/* to worker named tomcatFersped
	JkMount		/ferspedbroker/*	tomcatFersped

	RedirectMatch ^/$ /ferspedbroker

	ErrorLog ${APACHE_LOG_DIR}/ferspedbroker_error.log

	# Possible values include: debug, info, notice, warn, error, crit, alert, emerg.
	LogLevel debug

	CustomLog ${APACHE_LOG_DIR}/ferspedbroker_access.log combined 

</VirtualHost>
