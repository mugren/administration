<VirtualHost *:80>

	ServerAdmin admin@iustinian.mk
	ServerName iustinian.mk

	# Send servlet for context /iustinian to worker named tomcatIustinian
	JkMount		/iustinian		tomcatIustinian

        # Send JSPs for context /iustinian/* to worker named tomcatIustinian
	JkMount		/iustinian/*		tomcatIustinian

	RedirectMatch ^/$ /iustinian

	ErrorLog ${APACHE_LOG_DIR}/iustinian_error.log

	# Possible values include: debug, info, notice, warn, error, crit, alert, emerg.
	LogLevel debug

	CustomLog ${APACHE_LOG_DIR}/iustinian_access.log combined 

</VirtualHost>
